function prosesData(url, tipe, data, fungsiLanjutan) {
    var cdata = JSON.stringify(data);
    $.ajax({
        url: url,
        type: tipe,
        dataType: 'json',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        cache: false,
        async: false,
        timeout: 600000,
        beforeSend: function () {
            tampilLoading();
        },
        success: function (response) {
            sembunyiLoading();
            var kode = response["kode"];
            var pesan = response["pesan"];
            var returnData = response["data"];
            if (kode !== 200) {
                if (returnData != null && returnData.length > 0) {
                    tampilNotif('error', "Gagal", pesan, setTabelDataGagal(returnData));
                } else {
                    tampilNotif('error', "Gagal", pesan);
                }
            } else {
                tampilNotif('success', "Berhasil", pesan, fungsiLanjutan);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            sembunyiLoading();
            tampilNotif('error', "Gagal", textStatus + " : " + errorThrown);
        }
    });
}

function prosesUpload(url, data) {
    var returnData = {};
    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            tampilLoading();
        }
    }).done(function (response) {
        sembunyiLoading();
        if (response.kode !== 200) {
            tampilNotif('error', "Gagal", response.pesan);
        }
        returnData = response;
    }).fail(function (jqXHR, textStatus, errorThrown) {
        sembunyiLoading();
        tampilNotif('error', "Gagal", textStatus + " : " + errorThrown);
        returnData = null;
    });
    return returnData;
}

function cariDataKelas() {
    var response = ambilData("/account/user-login").data.akses;
    var data = [];
    var detail = {};
    var idx = 0;
    detail["key"] = idx;
    detail["value"] = "Semua";
    data.push(detail);
    for (var x in response) {
        if (response[x].parent === 2) {
            detail = {};
            idx++;
            detail["key"] = idx;
            detail["value"] = response[x].nama;
            data.push(detail);
        }
    }
    var mainData = {};
    mainData["kode"] = 200;
    mainData["pesan"] = "OK";
    mainData.data = data;
    return mainData;
}

function aturIkon(jenjang) {
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.type = "image/x-icon";
    link.rel = "shortcut icon";
    link.href = lokasiIkon(jenjang);
    document.getElementsByTagName('head')[0].appendChild(link);
}

function lokasiIkon(jenjang) {
    var lokasi = "";
    switch (jenjang) {
        case "SD":
            lokasi = "../../../static/assets/img/icon/sd-kecil.png";
            break;
        case "SMP":
            lokasi = "../../../static/assets/img/icon/smp-kecil.png";
            break;
        case "SMA":
            lokasi = "../../../static/assets/img/icon/sma-kecil.png";
            break;
        case "SMK":
            lokasi = "../../../static/assets/img/icon/smk-kecil.png";
            break;
        default:
            lokasi = "../../../static/assets/img/icon/yayasan-kecil.png";
            break;
    }
    return lokasi;
}

function konfirmasiProses(url, pesan, data, fungsiLanjutan) {
    Swal.fire({
        title: 'Konfirmasi',
        text: pesan,
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33'
    }).then(function (confirm) {
        if (confirm.value) {
            prosesData(url, "POST", data, fungsiLanjutan);
        }
    });
}

function sembunyiLoading() {
    $("#loading").modal("hide");
}

function tampilLoading() {
    $("#loading").modal("show");
}

function tampilNotif(tipe, judul, pesan, fungsiLanjutan) {
    Swal.fire({
        title: judul,
        html: pesan,
        icon: tipe,
        showClass: {
            popup: 'animate__animated animate__zoomIn'
        },
        hideClass: {
            popup: 'animate__animated animate__zoomOut'
        }
    }).then(function () {
        fungsiLanjutan && fungsiLanjutan();
    });
}

function reloadTampilan() {
    location.reload();
}

function ambilDataDenganParameter(url, param) {
    var data = {};
    $.ajax({
        url: url,
        type: "POST",
        async: false,
        dataType: 'json',
        data: JSON.stringify(param),
        contentType: 'application/json; charset=utf-8',
        cache: false,
        timeout: 600000,
        beforeSend: function () {
            tampilLoading();
        }
    }).done(function (response) {
        sembunyiLoading();
        data = response;
    }).fail(function (jqXHR, textStatus, errorThrown) {
        sembunyiLoading();
        tampilNotif('error', "Gagal", textStatus + " : " + errorThrown);
        data = null;
    });
    return data;
}

function ambilData(url) {
    var data = {};
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        dataType: 'json',
        beforeSend: function () {
            tampilLoading();
        }
    }).done(function (response) {
        sembunyiLoading();
        data = response;
    }).fail(function (jqXHR, textStatus, errorThrown) {
        sembunyiLoading();
        tampilNotif('error', "Gagal", textStatus + " : " + errorThrown);
        data = null;
    });
    return data;
}

function prosesFungsi(url, tipe, data, fungsiLanjutan) {
    $.ajax({
        url: url,
        type: tipe,
        dataType: 'json',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        cache: false,
        timeout: 600000,
        success: function (response) {
            var kode = response["kode"];
            var pesan = response["pesan"];
            if (kode !== 200) {
                tampilNotif('error', "Gagal", pesan, fungsiLanjutan);
            } else {
                fungsiLanjutan && fungsiLanjutan();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            tampilNotif('error', "Gagal", textStatus + " : " + errorThrown);
        }
    });
}

function appendOptionElement(select, data, param) {
    for (var x in data.data) {
        var option = document.createElement("option");
        option.setAttribute("value", data.data[x].key);
        option.innerHTML = data.data[x].value;
        if (param !== "" && param != null && param !== "-") {
            if (param === data.data[x].value) {
                option.setAttribute("selected", "selected");
            }
        }
        select.appendChild(option);
    }
}

function redirect(url) {
    window.location.href = url;
}

function cariBulanSekarang() {
    var bln = new Date().getMonth() + 1;
    var bulan = "";
    switch (bln) {
        case 1:
            bulan = "Januari";
            break;
        case 2:
            bulan = "Februari";
            break;
        case 3:
            bulan = "Maret";
            break;
        case 4:
            bulan = "April";
            break;
        case 5:
            bulan = "Mei";
            break;
        case 6:
            bulan = "Juni";
            break;
        case 7:
            bulan = "Juli";
            break;
        case 8:
            bulan = "Agustus";
            break;
        case 9:
            bulan = "September";
            break;
        case 10:
            bulan = "Oktober";
            break;
        case 11:
            bulan = "November";
            break;
        case 12:
            bulan = "Desember";
            break;
    }
    return bulan;
}

function tanggalKonversi(tgl) {
    var tanggal = tgl.toISOString().substring(0, 10);
    var bln = tanggal.substring(5, 7);
    var bulan = "-";
    switch (bln) {
        case "01":
            bulan = "Januari";
            break;
        case "02":
            bulan = "Februari";
            break;
        case "03":
            bulan = "Maret";
            break;
        case "04":
            bulan = "April";
            break;
        case "05":
            bulan = "Mei";
            break;
        case "06":
            bulan = "Juni";
            break;
        case "07":
            bulan = "Juli";
            break;
        case "08":
            bulan = "Agustus";
            break;
        case "09":
            bulan = "September";
            break;
        case "10":
            bulan = "Oktober";
            break;
        case "11":
            bulan = "November";
            break;
        case "12":
            bulan = "Desember";
            break;
    }
    return tanggal.substring(8, 10) + " " + bulan + " " + tanggal.substring(0, 4);
}

function setTabelDataGagal(data) {
    var xtable = $('#example1').DataTable();
    xtable.clear().draw();
    for (var x in data) {
        xtable.row.add([
            data[x].no,
            data[x].keterangan
        ]);
    }
    xtable.draw();
    aturTampilanTabel();
    $('#gagal').show();
}

function cariJenjang(kelas) {
    var sd = ["I", "II", "III", "IV", "V", "VI"];
    var smp = ["VII", "VIII", "IX"];
    var sma = ["X MIPA 1", "X MIPA 2", "X IPS 1", "X IPS 2", "XI MIPA 1", "IX MIPA 2", "XI IPS 1", "IX IPS 2", "XII MIPA 1", "XII MIPA 2", "XII IPS 1", "XII IPS 2"];
    var smk = ["X AKUNTANSI", "X PERKANTORAN", "X MULTIMEDIA", "XI AKUNTANSI", "XI PERKANTORAN", "XI MULTIMEDIA", "XII AKUNTANSI", "XII PERKANTORAN", "XII MULTIMEDIA"];
    if (sd.includes(kelas)) {
        return "SD";
    } else if (smp.includes(kelas)) {
        return "SMP";
    } else if (sma.includes(kelas)) {
        return "SMA";
    } else if (smk.includes(kelas)) {
        return "SMK";
    } else {
        return "-";
    }
}

function textDateFormat() {
    var dateComponent = $(".convert-date");
    var regex = /^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/gm;
    if (dateComponent) {
        dateComponent.each(function (index) {
            var valDate = $(this).text();
            if (!regex.test(valDate)) {
                var tgl = convertDate(valDate);
                $(this).html(tgl)
            }
        });
    }
}

function convertDate(val) {
    var regex = /^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/gm;
    if (!regex.test(val)) {
        var date = new Date(val.replace(val.substring(11, 23), ''));
        date.setDate(date.getDate() + 1);
        return date.toISOString().substring(0, 10);
    }
    return val;
}

function textMoneyFormat() {
    var textInputComponents = $('.money-format');
    textInputComponents.each(function (index) {
        var formatted = "";
        if ($(this).val()) {
            formatted = moneyFormat($(this).val());
            $(this).val(formatted)
        } else if ($(this).text()) {
            formatted = moneyFormat($(this).text());
            $(this).html(formatted);
        }
        $(this).keyup(function () {
            $(this).val(moneyFormat($(this).val()));
        });
    });
}

function formatMoneyDisplay(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1\,");
}

function moneyFormat(desc) {
    if (desc === "" || desc === null) {
        return "0";
    } else {
        var regex = /^[0-9-.,\b\t ]+$/;
        if (!regex.test(desc)) {
            tampilNotif('warning', "Harap Masukkan Angka [0-9]", null);
            return "0";
        } else {
            desc = removeMoneyDelimiters(desc);
            var money = parseFloat(desc);
            return money.formatMoney(0);
        }
    }
}

function removeMoneyDelimiters(num) {
    if (num) {
        return Number(num.replace(/,/g, ''));
    } else {
        return num;
    }
}

function hurufBesarPertama(x) {
    return x.charAt(0).toUpperCase() + x.slice(1);
}

Number.prototype.formatMoney = function (decPlaces, thouSeparator, decSeparator) {
    var n = this,
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSeparator = decSeparator === undefined ? "." : decSeparator,
        thouSeparator = thouSeparator === undefined ? "," : thouSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};
