<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="header" class="back-merah">
    <nav id="navheader" class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#" id="logo" onclick="menuUtama()"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarScroll" aria-controls="navbarScroll"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarScroll">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li id="judul" class="li-title"></li>
                </ul>
                <div class="menu">
                    <ul class="navbar-nav head-ul" id="ulHead">
                        <li id="pengguna" class="li-title"></li>
                        <li>
                            <div class="dropdown">
                                <div class="dropdown-toggle" id="profil" role="button" data-bs-toggle="dropdown"
                                     aria-expanded="false">
                                    <div class="photo" id="foto"></div>
                                </div>
                                <div class="dropdown-menu dropdown-fix" aria-labelledby="profil">
                                    <a class="dropdown-item" onclick="toDetail()">
                                        <em class="fa fa-user mr-2"></em>
                                        Profil
                                    </a>
                                    <a class="dropdown-item" onclick="toSetting()">
                                        <em class="fa fa-cog mr-2"></em>
                                        Setting
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" onclick="eksekusiLogout()">
                                        <em class="fa fa-sign-out mr-2"></em>
                                        Log Out
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>

<script type="text/javascript">

    function eksekusiLogout() {
        konfirmasiProses("/account/logout", "Anda Yakin Ingin Keluar Dari Sistem?", null, function () {
            redirect("/akun/login");
        });
    }

    function toDetail() {
        var dataLogin = ambilData("/account/user-login");
        redirect("/pengguna/detail?nip=" + dataLogin.data.nip);
    }

    function toSetting() {
        var dataLogin = ambilData("/account/user-login");
        redirect("/pengguna/setting?nip=" + dataLogin.data.nip);
    }

    function generateHeader(data) {
        var logo = document.getElementById("logo");
        var title = document.getElementById("judul");
        var user = document.getElementById("pengguna");
        var main = document.getElementById("base");
        var jenjang = data.jabatan.aksesJenjang;
        var judul = "MUHAMMADIYAH 18 JAKARTA";
        var img = document.createElement("img");

        img.src = "../../../static/assets/img/icon/yayasan-kecil.png";
        if (jenjang !== null || jenjang !== "") {
            if (jenjang !== "-" && jenjang !== "Semua") {
                if (jenjang === "SD") {
                    img.src = "../../../static/assets/img/icon/sd-kecil.png";
                } else if (jenjang === "SMP") {
                    img.src = "../../../static/assets/img/icon/smp-kecil.png";
                } else if (jenjang === "SMA") {
                    img.src = "../../../static/assets/img/icon/sma-kecil.png";
                } else if (jenjang === "SMK") {
                    img.src = "../../../static/assets/img/icon/smk-kecil.png";
                } else {
                    img.src = "../../../static/assets/img/icon/yayasan-kecil.png";
                }
            } else {
                jenjang = "YAYASAN";
            }
        } else {
            jenjang = "YAYASAN";
        }

        // img.src = "../../../static/assets/img/icon/sma-kecil.png";
        // img.src = "../../../static/assets/img/icon/yayasan-kecil.png";
        // if (jenjang !== null || jenjang !== "") {
        //     if (jenjang !== "-" && jenjang !== "Semua") {
        //         if (jenjang === "SD") {
        //             img.src = "../../../static/assets/img/icon/sd-kecil.png";
        //         } else if (jenjang === "SMP") {
        //             img.src = "../../../static/assets/img/icon/smp-kecil.png";
        //         } else if (jenjang === "SMA") {
        //             img.src = "../../../static/assets/img/icon/sma-kecil.png";
        //         } else if (jenjang === "SMK") {
        //             img.src = "../../../static/assets/img/icon/smk-kecil.png";
        //         } else {
        //             img.src = "../../../static/assets/img/icon/yayasan-kecil.png";
        //         }
        //     } else {
        //         jenjang = "YAYASAN";
        //     }
        // } else {
        //     jenjang = "YAYASAN";
        // }
        judul = jenjang + " " + judul;
        title.innerHTML = judul;
        user.innerHTML = data.nama;
        logo.appendChild(img);
        var navFoto = document.getElementById("foto");
        var imgNav = document.createElement("img");
        imgNav.src = "../../../static/assets/img/avatar/man.png";
        if (data.jenisKelamin === "Perempuan") {
            imgNav.src = "../../../static/assets/img/avatar/woman.png";
        }
        navFoto.appendChild(imgNav);
        if (data.username !== "admin") {
            document.getElementById("navheader").style.paddingTop = 0;
        }
        main.innerHTML = "MONITORING PEMBAYARAN UNIT " + judul;
    }

    function menuUtama() {
        redirect("/");
    }
</script>
