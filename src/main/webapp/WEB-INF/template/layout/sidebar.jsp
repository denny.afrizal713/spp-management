<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="sidebar transition">
    <div class="sidebar-items">
        <ul id="listMenu"></ul>
    </div>
</div>

<script type="text/javascript">

    function generateMenu(akses) {
        var ul = document.getElementById("listMenu");
        for (var x in akses) {
            var ikon = akses[x].ikon;
            if (ikon === "" || ikon === "-" || ikon == null) {
                ikon = "fa-folder";
            }
            ikon = "fa " + ikon;
            if (akses[x].parent === 0) {
                var li = document.createElement("li");
                li.setAttribute("id", "li" + akses[x].menuId);
                li.setAttribute("onClick", "keHalaman('" + akses[x].url + "')");
                var aMenu = document.createElement("a");
                aMenu.setAttribute("id", "a" + akses[x].menuId);
                aMenu.setAttribute("href", "#");
                aMenu.setAttribute("class", "transition");
                aMenu.setAttribute("style", "line-height: 50px;");
                var emMenu = document.createElement("em");
                emMenu.setAttribute("class", ikon);
                aMenu.appendChild(emMenu);
                var spanMenu = document.createElement("span");
                spanMenu.innerHTML = akses[x].nama
                aMenu.appendChild(spanMenu);
                li.appendChild(aMenu);
                ul.appendChild(li);
            } else {
                var liParent = document.getElementById("li" + akses[x].parent);
                liParent.removeAttribute("onClick");
                var ulParent = document.getElementById("ul" + akses[x].parent);
                if (ulParent != null) {
                    generateToggleSub(ulParent, akses[x].menuId, akses[x].nama, ikon, akses[x].url);
                } else {
                    var aParent = document.getElementById("a" + akses[x].parent);
                    aParent.setAttribute("data-bs-toggle", "collapse");
                    aParent.setAttribute("data-bs-target", "#ul" + akses[x].parent);
                    aParent.setAttribute("aria-expanded", "false");
                    var emParent = document.createElement("em");
                    emParent.setAttribute("class", "fa fa-chevron-right toggle-right");
                    aParent.appendChild(emParent);
                    var divChild = document.createElement("div");
                    divChild.setAttribute("class", "collapse btn-toggle-submenu");
                    divChild.setAttribute("id", "ul" + akses[x].parent);
                    var ulChild = document.createElement("ul");
                    ulChild.setAttribute("id", "ul" + akses[x].parent);
                    generateToggleSub(ulChild, akses[x].menuId, akses[x].nama, ikon, akses[x].url);
                    divChild.appendChild(ulChild);
                    divChild.appendChild(ulChild);
                    liParent.appendChild(divChild);
                }
            }
        }
    }

    function generateToggleSub(ul, id, nama, ikon, url) {
        var li = document.createElement("li");
        li.setAttribute("id", "lisub" + id);
        li.setAttribute("onClick", "keHalaman('" + url + "')");
        var a = document.createElement("a");
        a.setAttribute("id", "asub" + id);
        a.setAttribute("href", "#");
        var em = document.createElement("em");
        em.setAttribute("class", ikon);
        a.appendChild(em);
        var span = document.createElement("span");
        span.innerHTML = nama;
        a.appendChild(span);
        li.appendChild(a);
        ul.appendChild(li);
    }

    function keHalaman(url) {
        if (url === "" || url === "-" || url == null) {
            redirect("/");
        } else {
            redirect(url);
        }
    }
</script>
