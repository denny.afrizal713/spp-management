<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><tiles:insertAttribute name="title"/></title>
    <link href="../../../static/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap-reboot.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/sweetalert2.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/ldbtn.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/ldld.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/loading.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/data-table.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <script src="../../../static/assets/js/util.js"></script>
    <script src="../../../static/assets/js/bootstrap.min.js"></script>
    <script src="../../../static/assets/js/popper.min.js"></script>
    <script src="../../../static/assets/js/sweetalert2.js"></script>
    <script src="../../../static/assets/js/sweetalert2.min.js"></script>
    <script src="../../../static/assets/js/jquery.min.js"></script>
    <script src="../../../static/assets/js/jquery-ui.min.js"></script>
    <script src="../../../static/assets/js/jquery.data-table.min.js"></script>
    <script src="../../../static/assets/js/ldld.min.js"></script>
    <script src="../../../static/assets/js/bootstrap-datepicker.min.js"></script>
</head>
<body class="backcolor">
<div id="wrapper">
    <tiles:insertAttribute name="header"/>
    <div id="content">
        <div class="content side-content">
            <tiles:insertAttribute name="sidebar"/>
        </div>
        <div class="content main-content">
            <br>
            <div class="text-center">
                <h2 id="base" class="base-title"></h2>
            </div>
            <br>
            <tiles:insertAttribute name="body"/>
        </div>
    </div>
</div>
<div class="modal" id="loading" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header modal-header-warna">
                <h4>
                    <strong>
                        Mohon Menunggu...
                    </strong>
                </h4>
            </div>
            <div class="modal-body">
                <img src="../../../static/assets/img/animation/loading.gif" alt="Loading Dialog"
                     style="position: relative;width: 250px"/>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        var exp = document.getElementById("example");
        if (exp != null) {
            exp.style.width = "100%";
        }
        $('#example').DataTable();
        var exp1 = document.getElementById("example1");
        if (exp1 != null) {
            exp1.style.width = "100%";
        }
        $('#example1').DataTable();
        aturTampilanTabel();
        textMoneyFormat();
        var response = ambilData("/account/user-login");
        if (response != null) {
            var data = response.data;
            aturIkon(data.jabatan.aksesJenjang);
            generateHeader(data);
            generateMenu(data.akses);
        }
    });

    function aturTampilanTabel() {
        $('#example').DataTable().columns.adjust().draw();
        $('#example1').DataTable().columns.adjust().draw();
    }

    $.extend(true, $.fn.dataTable.defaults, {
        "scrollX": true,
        "scrollY": "400px",
        "language": {
            "search": "Pencarian Data : ",
            "loadingRecords": "Mohon Menunggu...",
            "processing": "Memproses...",
            "lengthMenu": "Menampilkan _MENU_ data",
            "infoEmpty": "Menampilkan 0 ke 0 dari 0 data",
            "info": "Menampilkan _START_ ke _END_ dari _TOTAL_ data",
            "emptyTable": "Tidak ada data yang ditampilkan",
            "zeroRecords": "Tidak ada pencarian yang sesuai",
            "infoFiltered": "(filter dari _MAX_ total data)",
            "paginate": {
                "first": "<i class='fa fa-angle-double-left'></i>",
                "last": "<i class='fa fa-angle-double-right'></i>",
                "next": "<i class='fa fa-angle-right'></i>",
                "previous": "<i class='fa fa-angle-left'></i>"
            }
        }
    });
</script>
</body>
</html>
