<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><tiles:insertAttribute name="title"/></title>
    <link href="../../../static/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap-grid.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap-reboot.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/ldbtn.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/ldld.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/loading.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/data-table.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/assets/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../../static/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <script src="../../../static/assets/js/util.js"></script>
    <script src="../../../static/assets/js/bootstrap.min.js"></script>
    <script src="../../../static/assets/js/popper.min.js"></script>
    <script src="../../../static/assets/js/sweetalert2.min.js"></script>
    <script src="../../../static/assets/js/jquery.min.js"></script>
    <script src="../../../static/assets/js/jquery-ui.min.js"></script>
    <script src="../../../static/assets/js/jquery.data-table.min.js"></script>
    <script src="../../../static/assets/js/ldld.min.js"></script>
    <script src="../../../static/assets/js/bootstrap-datepicker.min.js"></script>
</head>
<body class="backcolor">
<div id="wrapper">
    <div id="content">
        <div>
            <tiles:insertAttribute name="navbar"/>
        </div>
        <div>
            <tiles:insertAttribute name="body"/>
        </div>
<%--        <div>--%>
        <%--            <tiles:insertAttribute name="footer"/>--%>
        <%--        </div>--%>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        aturIkon("-");
    });
</script>
</body>
</html>
