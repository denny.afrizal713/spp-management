<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="notfound">
    <div class="notfound">
        <div class="notfound-err">
            <h1>Oops!</h1>
        </div>
        <h2>${judulnya}</h2>
        <p>${deskripsi}</p>
        <a href="/">Kembali ke Halaman Utama</a>
    </div>
</div>
