<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <div class="row form-group">
        <div class="col">
            <fieldset class="fieldset" id="utama">
                <legend class="legend" id="legend"></legend>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label" for="username">Username</label>
                    </div>
                    <div class="col">
                        <input id="username" name="username" placeholder="Username" class="form-control"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label" for="nama">Nama</label>
                    </div>
                    <div class="col">
                        <input id="nama" name="nama" placeholder="Nama" class="form-control"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label" for="email">Email</label>
                    </div>
                    <div class="col">
                        <input id="email" name="email" placeholder="Email" class="form-control"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label" for="jkel">Jenis Kelamin</label>
                    </div>
                    <div class="col">
                        <select id="jkel" name="jkel" class="form-select"></select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label" for="pend">Pendidikan Terakhir</label>
                    </div>
                    <div class="col">
                        <select id="pend" name="pend" class="form-select"></select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label" for="jbtn">Jabatan</label>
                    </div>
                    <div class="col">
                        <select id="jbtn" name="jbtn" class="form-select"></select>
                    </div>
                </div>
                <div class="row form-group" id="divWalas">
                    <div class="col">
                        <label class="display-label" for="walas">Wali Kelas</label>
                    </div>
                    <div class="col">
                        <select id="walas" name="walas" class="form-select"></select>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col" id="lanjutan">
            <fieldset class="fieldset">
                <legend class="legend">Pilihan Lanjutan</legend>
                <div class="row form-group" id="divGdpn">
                    <div class="col">
                        <label class="display-label" for="gdpn">Gelar Depan</label>
                    </div>
                    <div class="col">
                        <div id="gdpn"></div>
                    </div>
                </div>
                <div class="row form-group" id="divGblkgd1">
                    <div class="col">
                        <label class="display-label" for="gblkgd1">Gelar Belakang D1</label>
                    </div>
                    <div class="col">
                        <select id="gblkgd1" name="gblkgd1" class="form-select"></select>
                    </div>
                </div>
                <div class="row form-group" id="divGblkgd2">
                    <div class="col">
                        <label class="display-label" for="gblkgd2">Gelar Belakang D2</label>
                    </div>
                    <div class="col">
                        <select id="gblkgd2" name="gblkgd2" class="form-select"></select>
                    </div>
                </div>
                <div class="row form-group" id="divGblkgd3">
                    <div class="col">
                        <label class="display-label" for="gblkgd3">Gelar Belakang D3</label>
                    </div>
                    <div class="col">
                        <select id="gblkgd3" name="gblkgd3" class="form-select"></select>
                    </div>
                </div>
                <div class="row form-group" id="divGblkgs1">
                    <div class="col">
                        <label class="display-label" for="gblkgs1">Gelar Belakang S1</label>
                    </div>
                    <div class="col">
                        <select id="gblkgs1" name="gblkgs1" class="form-select"></select>
                    </div>
                </div>
                <div class="row form-group" id="divGblkgs2">
                    <div class="col">
                        <label class="display-label" for="gblkgs2">Gelar Belakang S2</label>
                    </div>
                    <div class="col">
                        <select id="gblkgs2" name="gblkgs2" class="form-select"></select>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <br>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-primary btn-sm btn-fix" type="button" onclick="simpanDataPengguna()">
            <em class="fa fa-save"></em>
            Simpan
        </button>
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="reloadTampilan();">
            <em class="fa fa-times"></em>
            Batal
        </button>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <td>No</td>
                    <td>Menu Akses</td>
                    <td>Lihat Data</td>
                    <td>Tambah Data</td>
                    <td>Ubah Data</td>
                    <td>Hapus Data</td>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <input type="hidden" id="scope" name="scope" value="${scope}"/>
    <input type="hidden" id="nip" name="nip" value="${nip}"/>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        $('#divWalas').hide();
        var scope = $("#scope").val();
        var legend = document.getElementById("legend");
        if (scope === "ubah") {
            legend.innerHTML = "Ubah Pengguna";
            var dataUser = ambilDataDenganParameter("/user/detail", dataRequestPengguna());
            var data = dataUser.data;
            if (data != null) {
                $("#username").val(data.username);
                $("#nama").val(data.nama);
                appendOptionElement(document.getElementById("jkel"), ambilData("/user/option?key=jkel"), data.jenisKelamin);
                appendOptionElement(document.getElementById("pend"), ambilData("/user/option?key=pend"), data.pendidikanTerakhir);
                appendOptionElement(document.getElementById("jbtn"), ambilData("/user/option?key=jbtn"), data.jabatan.nama);
                if (data.jabatan.nama.startsWith("Wali")) {
                    appendElementWalas(data.idJabatan, data.waliKelas);
                }
                var jkel = "1";
                if (data.jenisKelamin === "Perempuan") {
                    jkel = "2";
                }
                tampilGelarDepan(jkel, data.pendidikanTerakhir, data.gelarDepan);
                tampilGelarBelakang(data.pendidikanTerakhir, data.gelarBelakang);
                isiTabelAkses(ambilDataDenganParameter("/jabatan/cari", dataFilterAkses()));
            }
        } else {
            document.getElementById("lanjutan").style.display = "none";
            var utama = document.getElementById("utama");
            utama.setAttribute("class", "fieldset half-fieldset");
            appendOptionElement(document.getElementById("jkel"), ambilData("/user/option?key=jkel"), "");
            appendOptionElement(document.getElementById("pend"), ambilData("/user/option?key=pend"), "");
            appendOptionElement(document.getElementById("jbtn"), ambilData("/user/option?key=jbtn"), "");
            legend.innerHTML = "Pengguna Baru";
        }
    });

    function dataRequestPengguna() {
        var data = {};
        data["nip"] = $("#nip").val();
        data["username"] = "-";
        data["password"] = "-";
        return data;
    }

    function simpanDataPengguna() {
        var data = dataRequest();
        if (data.username === "" || data.username == null) {
            tampilNotif('warning', "Peringatan", "Harap Isi Username");
        } else if (data.nama === "" || data.nama == null) {
            tampilNotif('warning', "Peringatan", "Harap Isi Nama Pengguna");
        } else if (data.email === "" || data.email == null) {
            tampilNotif('warning', "Peringatan", "Harap Isi Email Pengguna");
        } else if (data.jenisKelamin === "Semua") {
            tampilNotif('warning', "Peringatan", "Harap Pilih Jenis Kelamin");
        } else if (data.pendidikanTerakhir === "Semua") {
            tampilNotif('warning', "Peringatan", "Harap Pilih Pendidikan Terakhir");
        } else if (data.idJabatan === "0") {
            tampilNotif('warning', "Peringatan", "Harap Pilih Jabatan Pengguna");
        } else if ((data.idJabatan === "7" || data.idJabatan === "8" || data.idJabatan === "9" || data.idJabatan === "10") && data.waliKelas === "0") {
            tampilNotif('warning', "Peringatan", "Harap Pilih Kelas Untuk Wali Kelas");
        } else if ((data.pendidikanTerakhir === "D1" || data.pendidikanTerakhir === "D2" ||
            data.pendidikanTerakhir === "D3" || data.pendidikanTerakhir === "S1" ||
            data.pendidikanTerakhir === "S2") && data.gelarBelakang === "-") {
            tampilNotif('warning', "Peringatan", "Harap Isi Gelar Belakang Dengan Lengkap");
        } else if (data.pendidikanTerakhir === "S3" && (data.gelarDepan === "-" || data.gelarDepan === "" || data.gelarDepan == null)) {
            tampilNotif('warning', "Peringatan", "Harap Pilih Gelar Depan");
        } else {
            konfirmasiProses("/user", "Anda Yakin Ingin Simpan Data Pengguna?", data, function () {
                redirect("/pengguna");
            });
        }
    }

    function dataGelarDepan() {
        var gelarDepan = "";
        var jkel = $("#jkel option:selected").val();
        switch (jkel) {
            case "1":
                var drs = document.getElementById("chkDrs");
                var h = document.getElementById("chkH");
                var kh = document.getElementById("chkKh");
                if (drs != null && drs.checked === true) {
                    gelarDepan = gelarDepan + "Drs. ";
                }
                if (kh != null && kh.checked === true) {
                    gelarDepan = gelarDepan + "KH. ";
                }
                if (h != null && h.checked === true) {
                    gelarDepan = gelarDepan + "H. ";
                }
                break;
            case "2":
                var dra = document.getElementById("chkDra");
                var hj = document.getElementById("chkHj");
                if (dra != null && dra.checked === true) {
                    gelarDepan = gelarDepan + "Dra. ";
                }
                if (hj != null && hj.checked === true) {
                    gelarDepan = gelarDepan + "Hj. ";
                }
                break;
        }
        return gelarDepan;
    }

    function dataGelarBelakang() {
        var d1 = "";
        var d2 = "";
        var d3 = "";
        var s1 = "";
        var s2 = "";
        var gelarBelakang = "-";
        var pend = $("#pend option:selected").val();
        switch (pend) {
            case "4":
                d1 = $("#gblkgd1 option:selected").text();
                if (d1 !== "Semua") {
                    gelarBelakang = d1;
                }
                break;
            case "5":
                d2 = $("#gblkgd2 option:selected").text();
                if (d2 !== "Semua") {
                    gelarBelakang = d2;
                }
                break;
            case "6":
                d3 = $("#gblkgd3 option:selected").text();
                if (d3 !== "Semua") {
                    gelarBelakang = d3;
                }
                break;
            case "7":
                d3 = $("#gblkgd3 option:selected").text();
                s1 = $("#gblkgs1 option:selected").text();
                if (d3 !== "Semua" && s1 !== "Semua") {
                    gelarBelakang = d3 + ". " + s1;
                }
                break;
            case "8":
                d3 = $("#gblkgd3 option:selected").text();
                s1 = $("#gblkgs1 option:selected").text();
                s2 = $("#gblkgs2 option:selected").text();
                if (d3 !== "Semua" && s1 !== "Semua" && s2 !== "Semua") {
                    gelarBelakang = d3 + ". " + s1 + ". " + s2;
                }
                break;
        }
        return gelarBelakang;
    }

    function dataRequest() {
        var walas = $("#walas option:selected").text();
        var nip = $("#nip").val();
        var data = {};
        data["nip"] = nip;
        if (nip === "" || nip == null) {
            data["nip"] = "-";
        }
        data["username"] = $("#username").val();
        data["nama"] = $("#nama").val();
        data["email"] = $("#email").val();
        data["jenisKelamin"] = $("#jkel option:selected").text();
        data["pendidikanTerakhir"] = $("#pend option:selected").text();
        data["idJabatan"] = $("#jbtn option:selected").val();
        data["waliKelas"] = "-";
        if (walas != null && walas !== "") {
            data["waliKelas"] = walas;
        }
        data["gelarDepan"] = "-";
        var gelarDepan = dataGelarDepan();
        if (gelarDepan !== "" && gelarDepan != null) {
            data["gelarDepan"] = gelarDepan;
        }
        data["gelarBelakang"] = dataGelarBelakang();
        data["totalGagal"] = 0;
        return data;
    }

    function tampilGelarDepan(jkel, pend, param) {
        $('#divGdpn').show();
        var gelar = [];
        switch (jkel) {
            case "1":
                if (pend === "9") {
                    gelar = ["Drs", "KH", "H"];
                } else {
                    gelar = ["KH", "H"];
                }
                break;
            case "2":
                if (pend === "9") {
                    gelar = ["Dra", "Hj"];
                } else {
                    gelar = ["Hj"];
                }
                break;
        }
        var gdpn = document.getElementById("gdpn");
        while (gdpn.hasChildNodes()) {
            gdpn.removeChild(gdpn.firstChild);
        }
        var gelarParam = [];
        param = param.replace(".", "");
        if (param !== "" || param !== "-" || param != null) {
            gelarParam = param.split(" ");
        }
        for (var x in gelar) {
            var div = document.createElement("div");
            div.setAttribute("class", "container-sign");
            var label = document.createElement("label");
            label.setAttribute("class", "checkbox");
            label.innerHTML = gelar[x];
            var input = document.createElement("input");
            input.setAttribute("type", "checkbox");
            input.setAttribute("id", "chk" + gelar[x]);
            input.setAttribute("name", "chk" + gelar[x]);
            if (gelarParam.includes(gelar[x])) {
                input.setAttribute("checked", true);
            }
            label.appendChild(input);
            var span = document.createElement("span");
            span.setAttribute("class", "check");
            label.appendChild(span);
            div.appendChild(label);
            gdpn.appendChild(div);
        }
    }

    function tampilLanjutan() {
        sembunyiPilihanGelar();
        var jkel = $("#jkel option:selected").val();
        var pend = $("#pend option:selected").val();
        if (jkel !== "0" && pend !== "0") {
            var utama = document.getElementById("utama");
            utama.setAttribute("class", "fieldset");
            tampilGelarDepan(jkel, pend, "");
            tampilGelarBelakang(pend, "");
            document.getElementById("lanjutan").style.display = "block";
        }
    }

    function sembunyiPilihanGelar() {
        $('#divGblkgd1').hide();
        $('#divGblkgd2').hide();
        $('#divGblkgd3').hide();
        $('#divGblkgs1').hide();
        $('#divGblkgs2').hide();
    }

    function sembunyiGelarBelakang() {
        $('#divGblkgd2').hide();
        $('#divGblkgd3').hide();
        $('#divGblkgs1').hide();
        $('#divGblkgs2').hide();
    }

    function tampilGelarBelakang(pend, param) {
        $('#divGblkgd1').hide();
        sembunyiGelarBelakang();
        var paramGelar = [];
        if (param !== "") {
            paramGelar = param.split(" ");
        }
        switch (pend) {
            case "D1":
            case "4":
            case 4:
                $('#gblkgd1').empty();
                appendOptionElement(document.getElementById("gblkgd1"), ambilData("/user/option?key=gblkgd1"), paramGelar[0]);
                $('#divGblkgd1').show();
                break;
            case "D2":
            case "5":
            case 5:
                $('#gblkgd2').empty();
                appendOptionElement(document.getElementById("gblkgd2"), ambilData("/user/option?key=gblkgd2"), paramGelar[0]);
                $('#divGblkgd2').show();
                break;
            case "D3":
            case "6":
            case 6:
                $('#gblkgd3').empty();
                appendOptionElement(document.getElementById("gblkgd3"), ambilData("/user/option?key=gblkgd3"), paramGelar[0]);
                $('#divGblkgd3').show();
                break;
            case "S1":
            case "7":
            case 7:
                $('#gblkgd3').empty();
                $('#gblkgs1').empty();
                appendOptionElement(document.getElementById("gblkgd3"), ambilData("/user/option?key=gblkgd3"), paramGelar[0]);
                $('#divGblkgd3').show();
                appendOptionElement(document.getElementById("gblkgs1"), ambilData("/user/option?key=gblkgs1"), paramGelar[1]);
                $('#divGblkgs1').show();
                break;
            case "S2":
            case "8":
            case 8:
            case "S3":
            case "9":
            case 9:
                $('#gblkgd3').empty();
                $('#gblkgs1').empty();
                $('#gblkgs2').empty();
                appendOptionElement(document.getElementById("gblkgd3"), ambilData("/user/option?key=gblkgd3"), paramGelar[0]);
                $('#divGblkgd3').show();
                appendOptionElement(document.getElementById("gblkgs1"), ambilData("/user/option?key=gblkgs1"), paramGelar[1]);
                $('#divGblkgs1').show();
                appendOptionElement(document.getElementById("gblkgs2"), ambilData("/user/option?key=gblkgs2"), paramGelar[2]);
                $('#divGblkgs2').show();
                break;
            default:
                $('#divGblkgd1').empty();
                $('#divGblkgd2').empty();
                $('#divGblkgd3').empty();
                $('#divGblkgs1').empty();
                $('#divGblkgs2').empty();
                sembunyiGelarBelakang();
                break;
        }
    }

    function appendElementWalas(jabatan, param) {
        switch (jabatan) {
            case "7":
            case 7:
                $('#divWalas').show();
                appendOptionElement(document.getElementById("walas"), ambilData("/user/option?key=walassd"), param);
                break;
            case "8":
            case 8:
                $('#divWalas').show();
                appendOptionElement(document.getElementById("walas"), ambilData("/user/option?key=walassmp"), param);
                break;
            case "9":
            case 9:
                $('#divWalas').show();
                appendOptionElement(document.getElementById("walas"), ambilData("/user/option?key=walassma"), param);
                break;
            case "10":
            case 10:
                $('#divWalas').show();
                appendOptionElement(document.getElementById("walas"), ambilData("/user/option?key=walassmk"), param);
                break;
            default:
                $('#divWalas').hide();
                $('#walas').empty();
                break;
        }
    }

    $('#jbtn').change(function () {
        $('#divWalas').hide();
        $('#walas').empty();
        var jabatan = $("#jbtn option:selected").val();
        appendElementWalas(jabatan, "");
    });

    $('#jkel').change(function () {
        tampilLanjutan();
    });

    $('#pend').change(function () {
        tampilLanjutan();
    });

    function isiTabelAkses(data) {
        var xtable = $('#example').DataTable();
        xtable.clear().draw();
        for (var x in data.data) {
            xtable.row.add([
                data.data[x].no,
                data.data[x].menu,
                data.data[x].lihat,
                data.data[x].tambah,
                data.data[x].ubah,
                data.data[x].hapus
            ]);
        }
        xtable.draw();
    }

    function dataFilterAkses() {
        var filter = {};
        var jabatan = $("#jbtn option:selected").text();
        var kelas = $("#walas option:selected").text();
        if (kelas === "" || kelas == null || kelas === "Semua") {
            kelas = "-";
        }
        filter["jabatan"] = jabatan;
        filter["kelas"] = kelas;
        return filter;
    }

    $('#jbtn').change(function () {
        var jabatan = $("#jbtn option:selected").text();
        if (jabatan !== "Wali Kelas SD" && jabatan !== "Wali Kelas SMP" && jabatan !== "Wali Kelas SMA" && jabatan !== "Wali Kelas SMK") {
            isiTabelAkses(ambilDataDenganParameter("/jabatan/cari", dataFilterAkses()));
        } else {
            var xtable = $('#table').DataTable();
            xtable.clear().draw();
        }
    });

    $('#walas').change(function () {
        isiTabelAkses(ambilDataDenganParameter("/jabatan/cari", dataFilterAkses()));
    });
</script>
