<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <div class="row form-group">
        <div class="col">
            <fieldset class="fieldset">
                <legend class="legend">Detail Pengguna</legend>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">NIP</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="nip" name="nip"></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">Username</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="username" name="username"></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">Nama</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="nama" name="nama"></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">Email</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="email" name="email"></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">Jenis Kelamin</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="jkel" name="jkel"></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">Pendidikan Terakhir</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="pend" name="pend"></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">Jabatan</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="jbtn" name="jbtn"></label>
                    </div>
                </div>
                <div class="row form-group" id="divWalas">
                    <div class="col">
                        <label class="display-label">Wali Kelas</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="walas" name="walas"></label>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Menu Akses</th>
                    <th>Lihat Data</th>
                    <th>Tambah Data</th>
                    <th>Ubah Data</th>
                    <th>Hapus Data</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <input type="hidden" id="xnip" name="xnip" value="${nip}"/>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        var nip = $("#xnip").val();
        $('#divWalas').hide();
        document.getElementById("nip").innerHTML = nip;
        var dataUser = ambilDataDenganParameter("/user/detail", requestDataUser());
        var data = dataUser.data;
        if (data != null) {
            document.getElementById("username").innerHTML = data.username;
            var nama = data.nama;
            var gelarDepan = data.gelarDepan;
            if (gelarDepan.startsWith("KH. H.")) {
                gelarDepan.replace("KH. H. ", "KH.");
            }
            if (gelarDepan === "-" || gelarDepan == null) {
                gelarDepan = "";
            } else {
                gelarDepan = gelarDepan + " ";
            }
            var gelarBelakang = data.gelarBelakang;
            if (gelarBelakang === "-" || gelarBelakang == null) {
                gelarBelakang = "";
            }
            document.getElementById("nama").innerHTML = gelarDepan + nama + " " + gelarBelakang;
            document.getElementById("jkel").innerHTML = data.jenisKelamin;
            document.getElementById("pend").innerHTML = data.pendidikanTerakhir;
            document.getElementById("email").innerHTML = data.email;
            var jabatan = data.jabatan.nama;
            if (jabatan.startsWith("Wali")) {
                $('#divWalas').show();
                document.getElementById("walas").innerHTML = data.waliKelas;
            }
            document.getElementById("jbtn").innerHTML = jabatan;
            isiTabelAkses(ambilDataDenganParameter("/jabatan/cari", aksesFilter(jabatan, data.waliKelas)));
        }
    });

    function aksesFilter(jabatan, kelas) {
        var filter = {};
        if (kelas === "" || kelas == null || kelas === "Semua") {
            kelas = "-";
        }
        filter["jabatan"] = jabatan;
        filter["kelas"] = kelas;
        return filter;
    }

    function requestDataUser() {
        var data = {};
        data["nip"] = $("#xnip").val();
        data["username"] = "-";
        data["password"] = "-";
        return data;
    }

    function isiTabelAkses(data) {
        var xtable = $('#example').DataTable();
        xtable.clear().draw();
        for (var x in data.data) {
            xtable.row.add([
                data.data[x].no,
                data.data[x].menu,
                data.data[x].lihat,
                data.data[x].tambah,
                data.data[x].ubah,
                data.data[x].hapus
            ]);
        }
        xtable.draw();
    }
</script>
