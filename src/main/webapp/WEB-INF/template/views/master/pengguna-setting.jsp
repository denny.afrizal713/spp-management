<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <div class="row form-group">
        <div class="col">
            <fieldset class="fieldset">
                <legend class="legend">Ubah Password</legend>
                <div class="row form-group">
                    <div class="col">
                        <label class="display-label">Nama</label>
                    </div>
                    <div class="col">
                        <label class="display-label" id="nama" name="nama"></label>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="oldpass">Password Lama</label>
                        </div>
                        <div class="col">
                            <input id="oldpass" name="oldpass" placeholder="Password Lama" class="form-control"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="newpass">Password Baru</label>
                        </div>
                        <div class="col">
                            <input id="newpass" name="newpass" placeholder="Password Baru" class="form-control"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="confirm">Konfirmasi</label>
                        </div>
                        <div class="col">
                            <input id="confirm" name="confirm" placeholder="Konfirmasi" class="form-control"/>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <br>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-primary btn-sm btn-fix" type="button" onclick="simpanPerubahan()">
            <em class="fa fa-save"></em>
            Simpan
        </button>
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="reloadTampilan();">
            <em class="fa fa-times"></em>
            Batal
        </button>
    </div>
    <input type="hidden" id="xnip" name="xnip" value="${nip}"/>
    <input type="hidden" id="user" name="user"/>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        var dataUser = ambilDataDenganParameter("/user/detail", requestDataUser());
        var xnama = document.getElementById("nama");
        var nama = dataUser.data.nama;
        var gelarDepan = dataUser.data.gelarDepan;
        if (gelarDepan.startsWith("KH. H.")) {
            gelarDepan.replace("KH. H. ", "KH.");
        }
        if (gelarDepan === "-" || gelarDepan == null) {
            gelarDepan = "";
        } else {
            gelarDepan = gelarDepan + " ";
        }
        var gelarBelakang = dataUser.data.gelarBelakang;
        if (gelarBelakang === "-" || gelarBelakang == null) {
            gelarBelakang = "";
        }
        xnama.innerHTML = gelarDepan + nama + " " + gelarBelakang;
        $("#user").val(dataUser.data.username);
    });

    function requestDataUser() {
        var data = {};
        data["nip"] = $("#xnip").val();
        data["username"] = "-";
        data["password"] = "-";
        return data;
    }

    function simpanPerubahan() {
        var oldPass = $("#oldpass").val();
        var newPass = $("#newpass").val();
        var confirm = $("#confirm").val();
        if (oldPass === "" || oldPass == null) {
            tampilNotif('info', "Peringatan", "Harap Isi Password Saat Ini");
        } else if (newPass === "" || newPass == null) {
            tampilNotif('info', "Peringatan", "Harap Isi Password Baru");
        } else if (confirm === "" || confirm == null) {
            tampilNotif('info', "Peringatan", "Harap Konfirmasi Password Baru");
        } else if (newPass !== confirm) {
            tampilNotif('info', "Peringatan", "Password Baru Tidak Sesuai dengan Konfirmasi");
        } else {
            konfirmasiProses("/user/ubah-password", "Anda Yakin Ingin Simpan Perubahan?", siapkanDataRequest(), function () {
                redirect("/pengguna/setting?nip=" + $("#xnip").val());
            });
        }
    }

    function siapkanDataRequest() {
        var data = {};
        data["nip"] = $("#xnip").val();
        data["username"] = $("#user").val();
        data["password"] = $("#newpass").val();
        data["oldPassword"] = $("#oldpass").val();
        return data;
    }
</script>
