<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <div class="row form-group">
        <fieldset class="fieldset">
            <legend class="legend">Pulihkan Pengguna</legend>
            <table>
                <tr>
                    <td class="col-sm-1">
                        <label class="display-label" for="nip">NIP</label>
                    </td>
                    <td class="col-sm-3">
                        <input id="nip" name="nip" placeholder="NIP" class="form-control"/>
                    </td>
                    <td class="col-sm-1">
                        <label class="display-label" for="username">Username</label>
                    </td>
                    <td class="col-sm-3">
                        <input id="username" name="username" placeholder="Username" class="form-control"/>
                    </td>
                    <td class="col-sm-1">
                        <label class="display-label" for="nama">Nama</label>
                    </td>
                    <td class="col-sm-3">
                        <input id="nama" name="nama" placeholder="Nama" class="form-control"/>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <br>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="cariDataUntukRecover();return false;">
            <em class="fa fa-search"></em>
            Cari
        </button>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <th>NIP</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Pendidikan</th>
                    <th>Jabatan</th>
                    <th>Wali Kelas</th>
                    <th>Akses Jenjang</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        cariDataUntukRecover();
    });

    function dataRequestCari() {
        var data = {};
        data["nip"] = $("#nip").val();
        data["username"] = $("#username").val();
        data["nama"] = $("#nama").val();
        return data;
    }

    function cariDataUntukRecover() {
        cariData(ambilDataDenganParameter("/user/cari-akun", dataRequestCari()));
    }

    function recover(x) {
        konfirmasiProses("/user/pulih-akun", "Anda Yakin Ingin Pulihkan Akun " + x + "?", siapkanDataReset(x), function () {
            redirect("/pengguna/pulihkan-akun");
        });
    }

    function siapkanDataReset(x) {
        var data = {};
        data["nip"] = x;
        data["username"] = "-";
        data["password"] = "-";
        data["oldPassword"] = "-";
        return data;
    }

    function cariData(data) {
        var xtable = $('#example').DataTable();
        xtable.clear().draw();
        for (var x in data.data) {
            var gelarDepan = data.data[x].gelarDepan;
            if (gelarDepan.startsWith("KH. H.")) {
                gelarDepan.replace("KH. H. ", "KH.");
            }
            if (gelarDepan === "-" || gelarDepan == null) {
                gelarDepan = "";
            } else {
                gelarDepan = gelarDepan + " ";
            }
            var gelarBelakang = data.data[x].gelarBelakang;
            if (gelarBelakang === "-" || gelarBelakang == null) {
                gelarBelakang = "";
            }
            var nama = gelarDepan + " " + data.data[x].nama + " " + gelarBelakang;
            xtable.row.add([
                data.data[x].nip,
                data.data[x].username,
                nama,
                data.data[x].jenisKelamin,
                data.data[x].pendidikanTerakhir,
                data.data[x].jabatan.nama,
                data.data[x].waliKelas,
                data.data[x].jabatan.aksesJenjang,
                "<button id='" + data.data[x].nip + "' class='btn btn-danger btn-sm btn-fix' type='button' onclick='recover(this.id)'><em class='fa fa-cog'></em>&nbsp;Recover</button>"
            ]);
        }
        xtable.draw();
    }
</script>
