<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Filter Data</legend>
        <table>
            <tr>
                <td class="col-sm-3">
                    <label for="nip" class="display-label">NIP</label>
                </td>
                <td class="col-sm-3">
                    <input id="nip" name="nip" placeholder="NIP" class="form-control" maxlength="10"/>
                </td>
                <td class="col-sm-3">
                    <label for="username" class="display-label">Username</label>
                </td>
                <td class="col-sm-3">
                    <input id="username" name="username" placeholder="Username" class="form-control"/>
                </td>
            </tr>
            <tr>
                <td class="col-sm-3">
                    <label for="nama" class="display-label">Nama</label>
                </td>
                <td class="col-sm-3">
                    <input id="nama" name="nama" placeholder="Nama" class="form-control"/>
                </td>
                <td class="col-sm-3">
                    <label for="jenisKelamin" class="display-label">Jenis Kelamin</label>
                </td>
                <td class="col-sm-3">
                    <select id="jenisKelamin" name="jenisKelamin" class="form-select"></select>
                </td>
            </tr>
            <tr>
                <td class="col-sm-3">
                    <label for="pendidikan" class="display-label">Pendidikan Terakhir</label>
                </td>
                <td class="col-sm-3">
                    <select id="pendidikan" name="pendidikan" class="form-select"></select>
                </td>
                <td class="col-sm-3">
                    <label for="walas" class="display-label">Wali Kelas</label>
                </td>
                <td class="col-sm-3">
                    <select id="walas" name="walas" class="form-select"></select>
                </td>
            </tr>
            <tr>
                <td class="col-sm-3">
                    <label for="gelarDepan" class="display-label">Gelar Depan</label>
                </td>
                <td class="col-sm-3">
                    <select id="gelarDepan" name="gelarDepan" class="form-select"></select>
                </td>
                <td class="col-sm-3">
                    <label for="gelarBelakang" class="display-label">Gelar Belakang</label>
                </td>
                <td class="col-sm-3">
                    <select id="gelarBelakang" name="gelarBelakang" class="form-select"></select>
                </td>
            </tr>
            <tr>
                <td class="col-sm-3">
                    <label for="jabatan" class="display-label">Jabatan</label>
                </td>
                <td class="col-sm-3">
                    <select id="jabatan" name="jabatan" class="form-select"></select>
                </td>
                <td class="col-sm-3">
                    <label for="akses" class="display-label">Akses Jenjang</label>
                </td>
                <td class="col-sm-3">
                    <select id="akses" name="akses" class="form-select"></select>
                </td>
            </tr>
        </table>
        <br>
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <button id="baru" class="btn btn-primary btn-sm btn-fix" type="button" onclick="tambahPengguna()">
                <em class="fa fa-pencil"></em>
                Tambah
            </button>
            <button class="btn btn-info btn-sm btn-fix" type="button" onclick="reloadTampilan()">
                <em class="fa fa-refresh"></em>
                Reset
            </button>
            <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="cariData();return false;">
                <em class="fa fa-search"></em>
                Cari
            </button>
        </div>
    </fieldset>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <th>NIP</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Pendidikan</th>
                    <th>Jabatan</th>
                    <th>Wali Kelas</th>
                    <th>Akses Jenjang</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        $("#nip").val("");
        $("#username").val("");
        $("#nama").val("");
        appendOptionElement(document.getElementById("jenisKelamin"), ambilData("/user/option?key=jkel"), "");
        appendOptionElement(document.getElementById("pendidikan"), ambilData("/user/option?key=pend"), "");
        appendOptionElement(document.getElementById("walas"), ambilData("/user/option?key=walas"), "");
        appendOptionElement(document.getElementById("gelarDepan"), ambilData("/user/option?key=gdpn"), "");
        appendOptionElement(document.getElementById("gelarBelakang"), ambilData("/user/option?key=gblkg"), "");
        appendOptionElement(document.getElementById("jabatan"), ambilData("/user/option?key=jbtn"), "");
        appendOptionElement(document.getElementById("akses"), ambilData("/user/option?key=akses"), "");
        generateTableValue(ambilDataDenganParameter("/user/list", dataFilterAwal()));
    });

    function dataFilterAwal() {
        var data = {};
        data["nip"] = "";
        data["username"] = "";
        data["nama"] = "";
        data["gelarDepan"] = "";
        data["gelarBelakang"] = "";
        data["pendidikanTerakhir"] = "";
        data["jenisKelamin"] = "";
        data["waliKelas"] = "";
        data["jabatan"] = "";
        data["aksesJenjang"] = "";
        data["size"] = 10;
        data["page"] = 0;
        return data;
    }

    function generateTableValue(data) {
        var xtable = $('#example').DataTable();
        xtable.clear().draw();
        for (var x in data.data) {
            var btnUbah = "<button id='baru' class='btn btn-primary btn-sm btn-fix' type='button' onclick='ubah(" + data.data[x].nip + ")'><em class='fa fa-pencil'></em>&nbsp;Ubah</button>";
            var btnHapus = "<button id='baru' class='btn btn-danger btn-sm btn-fix' type='button' onclick='hapus(" + data.data[x].nip + ")'><em class='fa fa-times'></em>&nbsp;Hapus</button>";
            xtable.row.add([
                data.data[x].nip,
                data.data[x].username,
                data.data[x].nama,
                data.data[x].jenisKelamin,
                data.data[x].pendidikanTerakhir,
                data.data[x].jabatan,
                data.data[x].waliKelas,
                data.data[x].aksesJenjang,
                btnUbah + " " + btnHapus
            ]);
        }
        xtable.draw();
    }

    function cariData() {
        generateTableValue(ambilDataDenganParameter("/user/list", dataFilter()));
    }

    function tambahPengguna() {
        redirect("/pengguna/tambah");
    }

    function dataFilter() {
        var table = $("#dataTable").DataTable();
        var info = table.page.info();
        var data = {};
        data["nip"] = $("#nip").val();
        data["username"] = $("#username").val();
        data["nama"] = $("#nama").val();
        var gelarDepan = $("#gelarDepan option:selected").text();
        data["gelarDepan"] = gelarDepan;
        if (gelarDepan === "Semua") {
            data["gelarDepan"] = "";
        }
        var gelarBelakang = $("#gelarBelakang option:selected").text();
        data["gelarBelakang"] = gelarBelakang;
        if (gelarBelakang === "Semua") {
            data["gelarBelakang"] = "";
        }
        var pendidikanTerakhir = $("#pendidikan option:selected").text();
        data["pendidikanTerakhir"] = pendidikanTerakhir;
        if (pendidikanTerakhir === "Semua") {
            data["pendidikanTerakhir"] = "";
        }
        var jenisKelamin = $("#jenisKelamin option:selected").text();
        data["jenisKelamin"] = jenisKelamin;
        if (jenisKelamin === "Semua") {
            data["jenisKelamin"] = "";
        }
        var waliKelas = $("#walas option:selected").text();
        data["waliKelas"] = waliKelas;
        if (waliKelas === "Semua") {
            data["waliKelas"] = "";
        }
        var jabatan = $("#jabatan option:selected").text();
        data["jabatan"] = jabatan;
        if (jabatan === "Semua") {
            data["jabatan"] = "";
        }
        var aksesJenjang = $("#akses option:selected").text();
        data["aksesJenjang"] = aksesJenjang;
        if (aksesJenjang === "Semua") {
            data["aksesJenjang"] = "";
        }
        data["size"] = info.length;
        data["page"] = info.page;
        return data;
    }

    function ubah(x) {
        redirect("/pengguna/ubah?nip=" + x);
    }

    function hapus(x) {
        var data = {};
        data["nip"] = x;
        data["username"] = "";
        konfirmasiProses("/user/hapus", "Anda Yakin Ingin Hapus Data Pengguna?", data, function () {
            redirect("/pengguna");
        });
    }
</script>
