<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Rekapan Tunggakan Pembayaran</legend>
        <div class="col">
            <div class="row form-group">
                <div class="col">
                    <label class="display-label" for="bln">Bulan</label>
                </div>
                <div class="col"></div>
            </div>
            <div class="row form-group">
                <div class="col">
                    <select id="bln" name="bln" class="form-select"></select>
                </div>
                <div class="col">
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="cariTunggakan()">
                        <em class="fa fa-search"></em>
                        Cari
                    </button>
                    <a href="#" id="cetak" class="btn btn-danger btn-sm btn-fix" type="button">
                        <em class="fa fa-print"></em>
                        Cetak
                    </a>
                </div>
            </div>
        </div>
    </fieldset>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Keterangan</th>
                    <th>Tunggakan Uang SPP</th>
                    <th>Tunggakan Uang Pangkal</th>
                    <th>Total Tunggakan</th>
                    <th>Wali Kelas</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="row form-group">
        <div class="col col-sm-4">
            <label class="display-label" id="keterangan">Total Tunggakan</label>
        </div>
        <div class="col col-sm-2">
            <label class="display-label" id="spp"></label>
        </div>
        <div class="col col-sm-2">
            <label class="display-label" id="up"></label>
        </div>
        <div class="col col-sm-4">
            <label class="display-label" id="total"></label>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        isiBulan();
        document.getElementById("cetak").style.color = "#ffffff";
        $('#cetak').hide();
    });

    function isiBulan() {
        var list = listBulan();
        var select = document.getElementById("bln");
        var iniOpt = document.createElement("option");
        var idx = 0;
        iniOpt.setAttribute("value", idx);
        iniOpt.innerHTML = "Pilih Bulan";
        select.appendChild(iniOpt);
        for (var x in list) {
            idx++;
            var opt = document.createElement("option");
            opt.setAttribute("value", idx);
            opt.innerHTML = list[x];
            select.appendChild(opt);
        }
    }

    function listBulan() {
        var bulan = [];
        bulan.push("Juli");
        bulan.push("Agustus");
        bulan.push("September");
        bulan.push("Oktober");
        bulan.push("November");
        bulan.push("Desember");
        bulan.push("Januari");
        bulan.push("Februari");
        bulan.push("Maret");
        bulan.push("April");
        bulan.push("Mei");
        bulan.push("Juni");
        return bulan;
    }

    function cariTunggakan() {
        $('#cetak').hide();
        var bulan = $("#bln option:selected").text();
        if (bulan === "Pilih Bulan" || bulan == null) {
            tampilNotif('warning', "Peringatan", "Harap Pilih Bulan");
        } else {
            var totalSpp = 0;
            var totalUp = 0;
            var grandTotal = 0;
            var data = ambilData("/laporan/tunggakan/" + bulan).data;
            var xtable = $('#example').DataTable();
            xtable.clear().draw();
            for (var x in data) {
                xtable.row.add([
                    data[x].no,
                    data[x].keterangan,
                    formatMoneyDisplay(data[x].spp),
                    formatMoneyDisplay(data[x].uangPangkal),
                    formatMoneyDisplay(data[x].total),
                    data[x].walas
                ]);
                totalSpp += parseFloat(data[x].spp);
                totalUp += parseFloat(data[x].uangPangkal);
                grandTotal += parseFloat(data[x].total);
            }
            xtable.draw();
            document.getElementById("spp").innerHTML = formatMoneyDisplay(totalSpp);
            document.getElementById("up").innerHTML = formatMoneyDisplay(totalUp);
            document.getElementById("total").innerHTML = formatMoneyDisplay(grandTotal);
            document.getElementById("cetak").href = "/laporan/tunggakan/cetak/" + bulan;
            $('#cetak').show();
        }
    }
</script>
