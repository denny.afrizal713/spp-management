<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Laporan Transaksi Harian</legend>
        <div class="col">
            <div class="row form-group">
                <div class="col">
                    <label class="display-label" for="tgl">Tanggal</label>
                </div>
                <div class="col">
                    <label class="display-label" for="jenis">Jenis Pembayaran</label>
                </div>
                <div class="col"></div>
            </div>
            <div class="row form-group">
                <div class="col">
                    <div class="input-group date">
                        <input type="text" class="form-control tanggal" id="tgl" name="tgl"/>
                        <span class="input-group-append">
                            <span class="input-group-text bg-light d-block">
                                <em class="fa fa-calendar"></em>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col">
                    <select id="jenis" name="jenis" class="form-select"></select>
                </div>
                <div class="col">
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="pencarian()">
                        <em class="fa fa-search"></em>
                        Cari
                    </button>
                    <a href="#" class="btn btn-danger btn-sm btn-fix" id="cetak" type="button">
                        <em class="fa fa-print"></em>
                        Cetak
                    </a>
                </div>
            </div>
        </div>
    </fieldset>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <th>No</th>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Jenis Pembayaran</th>
                    <th>Nominal</th>
                    <th>Keterangan</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="row form-group">
        <div class="col">
            <label class="display-label" id="keterangan"></label>
        </div>
        <div class="col col-sm-4">
            <label class="display-label" id="nominal"></label>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        $('.tanggal').datepicker({
            format: 'dd-mm-yyyy',
            orientation: 'bottom',
            autoclose: true
        });
        aturJenis();
        document.getElementById("cetak").style.color = "#ffffff";
        $('#cetak').hide();
    });

    function aturJenis() {
        var select = document.getElementById("jenis");
        for (var x = 0; x <= 4; x++) {
            var option = document.createElement("option");
            option.setAttribute("value", x.toString());
            switch (x) {
                case 1:
                    option.innerHTML = "Uang Pangkal";
                    break;
                case 2:
                    option.innerHTML = "Daftar Ulang";
                    break;
                case 3:
                    option.innerHTML = "SPP";
                    break;
                case 4:
                    option.innerHTML = "SPP Tahun Lalu";
                    break;
                default:
                    option.innerHTML = "Pilih Data";
                    break;
            }
            select.appendChild(option);
        }
    }

    function convertTanggal(tgl) {
        var hari = tgl.substring(0, 2);
        var bulan = tgl.substring(3, 5);
        var tahun = tgl.substring(6, 10);
        return bulan + "/" + hari + "/" + tahun;
    }

    function pencarian() {
        $('#cetak').hide();
        var total = 0;
        var dataTrans = dataTransaksi();
        if (dataTrans.tanggal == null) {
            tampilNotif('warning', "Peringatan", "Harap Pilih Tanggal");
        } else {
            var data = ambilDataDenganParameter("/laporan/pembayaran", dataTrans).data;
            var xtable = $('#example').DataTable();
            xtable.clear().draw();
            for (var x in data) {
                xtable.row.add([
                    data[x].no,
                    data[x].nis,
                    data[x].nama,
                    data[x].kelas,
                    data[x].jenis,
                    formatMoneyDisplay(data[x].nominal),
                    data[x].keterangan
                ]);
                total += parseFloat(data[x].nominal);
            }
            xtable.draw();
            var tgl = tanggalKonversi(new Date(data[0].tanggal));
            document.getElementById("keterangan").innerHTML = "Total Transaksi Pembayaran " + tgl;
            document.getElementById("nominal").innerHTML = formatMoneyDisplay(total);
            var tanggal = dataTrans.tanggal;
            tanggal.setDate(tanggal.getDate() + 1);
            document.getElementById("cetak").href = "/laporan/pembayaran/cetak/" + dataTrans.jenis + "/" + tanggal.toISOString().substring(0, 10);
            $('#cetak').show();
        }
    }

    function dataTransaksi() {
        var data = {};
        var tgl = $("#tgl").val();
        if (tgl === "" || tgl == null) {
            data["tanggal"] = null;
        } else {
            data["tanggal"] = new Date(convertTanggal(tgl));
        }
        var jenis = $("#jenis option:selected").text();
        if (jenis === "Pilih Data" || jenis == null) {
            data["jenis"] = null;
        } else if (jenis === "SPP") {
            data["jenis"] = "tb";
        } else if (jenis === "SPP Tahun Lalu") {
            data["jenis"] = "tl";
        } else if (jenis === "Uang Pangkal") {
            data["jenis"] = "up";
        } else if (jenis === "Daftar Ulang") {
            data["jenis"] = "du";
        } else {
            data["jenis"] = null;
        }
        return data;
    }
</script>
