<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <div class="text-center">
        <h2 id="titleBase" class="base-title"></h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Keterangan</th>
                    <th>Tunggakan Uang SPP</th>
                    <th>Tunggakan Uang Pangkal</th>
                    <th>Total Tunggakan</th>
                    <th>Wali Kelas</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="row form-group">
        <div class="col col-sm-4">
            <label class="display-label" id="keterangan">Total Tunggakan</label>
        </div>
        <div class="col col-sm-2">
            <label class="display-label" id="spp"></label>
        </div>
        <div class="col col-sm-2">
            <label class="display-label" id="up"></label>
        </div>
        <div class="col col-sm-4">
            <label class="display-label" id="total"></label>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        cariTunggakan();
    });

    function cariTunggakan() {
        var bulan = cariBulanSekarang();
        var totalSpp = 0;
        var totalUp = 0;
        var grandTotal = 0;
        var data = ambilData("/laporan/tunggakan/" + bulan).data;
        var xtable = $('#example').DataTable();
        xtable.clear().draw();
        for (var x in data) {
            xtable.row.add([
                data[x].no,
                data[x].keterangan,
                formatMoneyDisplay(data[x].spp),
                formatMoneyDisplay(data[x].uangPangkal),
                formatMoneyDisplay(data[x].total),
                data[x].walas
            ]);
            totalSpp += parseFloat(data[x].spp);
            totalUp += parseFloat(data[x].uangPangkal);
            grandTotal += parseFloat(data[x].total);
        }
        xtable.draw();
        document.getElementById("spp").innerHTML = formatMoneyDisplay(totalSpp);
        document.getElementById("up").innerHTML = formatMoneyDisplay(totalUp);
        document.getElementById("total").innerHTML = formatMoneyDisplay(grandTotal);
        document.getElementById("cetak").href = "/laporan/tunggakan/cetak/" + bulan;
    }
</script>