<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="limiter">
    <div class="container-sign container-log-in">
        <div class="wrap-log-in">
            <form class="log-in-form validate-form">
                <div class="wrap-input-sign validate-input wrap-input-username"
                     data-validate="Masukkan Username atau NIP">
                    <input class="input-sign" type="text" id="username" name="username" maxlength="10"/>
                    <span class="focus-input-sign" data-placeholder="Username / NIP"></span>
                </div>
                <div class="wrap-input-sign validate-input wrap-input-pass" data-validate="Masukkan Password">
                    <input class="input-sign" type="password" id="password" name="password"/>
                    <span class="focus-input-sign" data-placeholder="Password"></span>
                </div>
                <div class="container-sign wrap-input-desc">
                    <label class="checkbox">Show Password
                        <input type="checkbox" id="showpass" name="showpass" onclick="showOrHide()"/>
                        <span class="check"></span>
                    </label>
                    <span class="ml-auto"><a href="#" onclick="notif()" class="txt-link">Forgot Password</a></span>
                </div>
                <div class="container-sign container-sign-btn">
                    <button class="btn-sign ld-ext-left" id="sign" onclick="eksekusiLogin();return false;">
                        Login
                        <span class="ld ld-ring ld-spin"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('.input-sign').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() !== "") {
                $(this).addClass('has-val');
            } else {
                $(this).removeClass('has-val');
            }
        });
    });

    $('.validate-form .input-sign').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    function validate(input) {
        if ($(input).attr('type') === 'email' || $(input).attr('name') === 'email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        } else {
            if ($(input).val().trim() === '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }

    function cek() {
        var input = $('.validate-input .input-sign');
        var cek = true;
        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) === false) {
                showValidate(input[i]);
                cek = false;
            }
        }
        if (cek === true) {
            prosesFungsi("/account/login", "POST", siapkanDataLogin(), function () {
                redirect("/");
            });
        }
    }

    function eksekusiLogin() {
        var signbtn = document.getElementById("sign");
        var loader = new ldLoader({root: signbtn});
        signbtn.classList.add("disabled");
        loader.on();
        cek();
    }

    function showOrHide() {
        var pass = document.getElementById("password");
        if (pass.type === "password") {
            pass.type = "text";
        } else {
            pass.type = "password";
        }
    }

    function siapkanDataLogin() {
        var data = {};
        data["username"] = $("#username").val();
        data["password"] = $("#password").val();
        return data
    }

    function notif() {
        tampilNotif('info', "Informasi", "Harap Hubungi Administrator Untuk Reset Password Anda");
    }
</script>
