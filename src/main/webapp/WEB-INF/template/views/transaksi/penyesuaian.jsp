<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Biodata&nbsp;Siswa</legend>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">NIS&nbsp;Siswa</label>
            </div>
            <div class="col col-sm-6">
                <label id="nis" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Nama&nbsp;Siswa</label>
            </div>
            <div class="col col-sm-6">
                <label id="nama" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Kelas</label>
            </div>
            <div class="col col-sm-6">
                <label id="kelas" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Tahun&nbsp;Ajaran</label>
            </div>
            <div class="col col-sm-6">
                <label id="tahun" class="display-label"></label>
            </div>
        </div>
    </fieldset>
    <br>
    <fieldset class="fieldset">
        <legend class="legend">Penyesuaian&nbsp;Pembayaran</legend>
        <br>
        <div class="row form-group">
            <div class="col">
                <fieldset class="fieldset">
                    <legend class="legend">Uang&nbsp;Pangkal</legend>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="tup" class="display-label">Tunggakan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="tup" name="tup" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="lup" class="display-label">Kelebihan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="lup" name="lup" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group float-end">
                        <div class="col col-sm-8">
                            <button class="btn btn-danger btn-sm btn-fix-plus" type="button" onclick="sesuai('up')">
                                <em class="fa fa-exchange"></em>
                                Penyesuaian
                            </button>
                        </div>
                    </div>
                </fieldset>
                <br>
                <fieldset class="fieldset">
                    <legend class="legend">Daftar&nbsp;Ulang</legend>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="tdu" class="display-label">Tunggakan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="tdu" name="tdu" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="ldu" class="display-label">Kelebihan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="ldu" name="ldu" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group float-end">
                        <div class="col col-sm-8">
                            <button class="btn btn-danger btn-sm btn-fix-plus" type="button" onclick="sesuai('du')">
                                <em class="fa fa-exchange"></em>
                                Penyesuaian
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col">
                <fieldset class="fieldset">
                    <legend class="legend">SPP&nbsp;Tahun&nbsp;Berjalan</legend>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="ttb" class="display-label">Tunggakan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="ttb" name="ttb" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="ltb" class="display-label">Kelebihan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="ltb" name="ltb" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group float-end">
                        <div class="col col-sm-8">
                            <button class="btn btn-danger btn-sm btn-fix-plus" type="button" onclick="sesuai('tb')">
                                <em class="fa fa-exchange"></em>
                                Penyesuaian
                            </button>
                        </div>
                    </div>
                </fieldset>
                <br>
                <fieldset class="fieldset">
                    <legend class="legend">SPP&nbsp;Tahun&nbsp;Lalu</legend>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="ttl" class="display-label">Tunggakan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="ttl" name="ttl" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-6">
                            <label for="ltl" class="display-label">Kelebihan&nbsp;Pembayaran</label>
                        </div>
                        <div class="col col-sm-6">
                            <input id="ltl" name="ltl" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group float-end">
                        <div class="col col-sm-8">
                            <button class="btn btn-danger btn-sm btn-fix-plus" type="button" onclick="sesuai('tl')">
                                <em class="fa fa-exchange"></em>
                                Penyesuaian
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </fieldset>
    <br>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="balik()">
            <em class="fa fa-undo"></em>
            Kembali
        </button>
    </div>
    <div class="modal" id="adjust" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modal-header-warna text-center">
                    <h4>
                        <strong id="modalTitle"></strong>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="sumber">Sumber</label>
                        </div>
                        <div class="col col-sm-8">
                            <select id="sumber" name="sumber" onchange="hitungPenyesuaian()"
                                    class="form-select"></select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="kurang">Kekurangan</label>
                        </div>
                        <div class="col col-sm-8">
                            <input id="kurang" name="kurang" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="bayar">Penyesuaian</label>
                        </div>
                        <div class="col col-sm-8">
                            <input id="bayar" name="bayar" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="sisa">Sisa</label>
                        </div>
                        <div class="col col-sm-8">
                            <input id="sisa" name="sisa" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="adjustment()">
                        <em class="fa fa-save"></em>
                        Simpan
                    </button>
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="tutup()">
                        <em class="fa fa-times"></em>
                        Batal
                    </button>
                </div>
                <input type="hidden" id="dari" name="dari"/>
                <input type="hidden" id="ke" name="ke"/>
            </div>
        </div>
    </div>
    <input type="hidden" id="xnis" name="xnis" value="${nis}"/>
    <input type="hidden" id="xkelas" name="xkelas" value="${kelas}"/>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        tutup();
        detailDataSet();
    });

    function tutup() {
        $("#adjust").modal("hide");
    }

    function detailDataSet() {
        var response = ambilData("/siswa/detail/" + $("#xnis").val() + "/" + cariJenjang($("#xkelas").val()));
        var data = response.data;
        if (data != null) {
            document.getElementById("nis").innerHTML = data.nis;
            document.getElementById("nama").innerHTML = data.nama;
            document.getElementById("kelas").innerHTML = data.kelas;
            document.getElementById("tahun").innerHTML = data.tahunAjaran;
            $("#tup").val(data.transaksi.upSisa);
            $("#lup").val(data.transaksi.upLebih);
            $("#tdu").val(data.transaksi.duSisa);
            $("#ldu").val(data.transaksi.duLebih);
            $("#ttb").val(cekTunggakanSppTb(data.sppTahunBerjalan));
            $("#ltb").val(data.transaksi.sppTbLebih);
            $("#ttl").val(data.transaksi.sppTlSisa);
            $("#ltl").val(data.transaksi.sppTlLebih);
        }
    }

    function cekTunggakanSppTb(spp) {
        var bln = new Date().getMonth();
        var total = 0;
        var jul = parseFloat(spp.sppJuli);
        var aug = parseFloat(spp.sppAgustus);
        var sep = parseFloat(spp.sppSeptember);
        var oct = parseFloat(spp.sppOktober);
        var nov = parseFloat(spp.sppNovember);
        var dec = parseFloat(spp.sppDesember);
        var jan = parseFloat(spp.sppJanuari);
        var feb = parseFloat(spp.sppFebruari);
        var mar = parseFloat(spp.sppMaret);
        var apr = parseFloat(spp.sppApril);
        var may = parseFloat(spp.sppMei);
        var jun = parseFloat(spp.sppJuni);
        switch (bln) {
            case 6:
                total = jul;
                break;
            case 7:
                total = jul + aug;
                break;
            case 8:
                total = jul + aug + sep;
                break;
            case 9:
                total = jul + aug + sep + oct;
                break;
            case 10:
                total = jul + aug + sep + oct + nov;
                break;
            case 11:
                total = jul + aug + sep + oct + nov + dec;
                break;
            case 0:
                total = jul + aug + sep + oct + nov + dec + jan;
                break;
            case 1:
                total = jul + aug + sep + oct + nov + dec + jan + feb;
                break;
            case 2:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar;
                break;
            case 3:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar + apr;
                break;
            case 4:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar + apr + may;
                break;
            case 5:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar + apr + may + jun;
                break;
        }
        return total;
    }

    function sesuai(type) {
        $('#sumber').empty();
        var modalJudul = "";
        var tunggak = 0;
        switch (type) {
            case "up":
                modalJudul = "Uang Pangkal";
                tunggak = $("#tup").val();
                break;
            case "du":
                modalJudul = "Daftar Ulang";
                tunggak = $("#tdu").val();
                break;
            case "tb":
                modalJudul = "SPP Tahun Berjalan";
                tunggak = $("#ttb").val();
                break;
            case "tl":
                modalJudul = "SPP Tahun Lalu";
                tunggak = $("#ttl").val();
                break;
            default:
                modalJudul = "-";
                break;
        }
        if (tunggak <= 0) {
            tampilNotif('warning', "Peringatan", "Tidak Bisa Melakukan Penyesuaian Untuk Data Yang Sudah Lunas");
        } else {
            $("#ke").val(modalJudul);
            document.getElementById("modalTitle").innerHTML = "Penyesuaian Pembayaran " + modalJudul;
            var select = document.getElementById("sumber");
            tambahOpsi(select, "all", "Pilih Pembayaran");
            var cek = false;
            var upLebih = parseFloat(removeMoneyDelimiters($("#lup").val()));
            if (upLebih > 0) {
                tambahOpsi(select, "up", "Uang Pangkal");
                cek = true;
            }
            var duLebih = parseFloat(removeMoneyDelimiters($("#ldu").val()));
            if (duLebih > 0) {
                tambahOpsi(select, "du", "Daftar Ulang");
                cek = true;
            }
            var tbLebih = parseFloat(removeMoneyDelimiters($("#ltb").val()));
            if (tbLebih > 0) {
                tambahOpsi(select, "tb", "SPP Tahun Berjalan");
                cek = true;
            }
            var tlLebih = parseFloat(removeMoneyDelimiters($("#ltl").val()));
            if (tlLebih > 0) {
                tambahOpsi(select, "tl", "SPP Tahun Lalu");
                cek = true;
            }
            if (cek === false) {
                tampilNotif('warning', "Peringatan", "Tidak Ada Kelebihan Pembayaran Untuk Dilakukan Penyesuaian");
            } else {
                $("#kurang").val(tunggak);
                $("#bayar").val(0);
                $("#sisa").val(tunggak);
                $("#adjust").modal("show");
            }
        }
    }

    function balik() {
        redirect("/siswa/transaksi/bayar?nis=" + $("#xnis").val() + "&kelas=" + $("#xkelas").val());
    }

    function tambahOpsi(select, id, value) {
        var opt = document.createElement("option");
        opt.setAttribute("value", id);
        opt.innerHTML = value;
        select.appendChild(opt);
    }

    function hitungPenyesuaian() {
        var sumber = $("#sumber option:selected").val();
        var bayar = 0;
        var desc = "";
        switch (sumber) {
            case "up":
                bayar = parseFloat(removeMoneyDelimiters($("#lup").val()));
                desc = "Uang Pangkal";
                break;
            case "du":
                bayar = parseFloat(removeMoneyDelimiters($("#ldu").val()));
                desc = "Daftar Ulang";
                break;
            case "tb":
                bayar = parseFloat(removeMoneyDelimiters($("#ltb").val()));
                desc = "SPP Tahun Berjalan";
                break;
            case "tl":
                bayar = parseFloat(removeMoneyDelimiters($("#ltl").val()));
                desc = "SPP Tahun Lalu";
                break;
            default:
                bayar = 0;
                desc = "-";
                break;
        }
        $("#dari").val(desc);
        var kurang = parseFloat(removeMoneyDelimiters($("#kurang").val()));
        var sisa = kurang - bayar
        $("#bayar").val(formatMoneyDisplay(bayar));
        $("#sisa").val(formatMoneyDisplay(sisa));
    }

    function prepareData() {
        var data = {};
        data["nis"] = $("#xnis").val();
        data["dari"] = $("#dari").val();
        data["ke"] = $("#ke").val();
        data["bayar"] = parseFloat(removeMoneyDelimiters($("#bayar").val()));
        return data;
    }

    function adjustment() {
        var bayar = parseFloat(removeMoneyDelimiters($("#bayar").val()));
        if (bayar === 0) {
            tampilNotif('warning', "Peringatan", "Harap Pilih Sumber Pembayaran");
        } else {
            tutup();
            konfirmasiProses("/transaksi/penyesuaian", "Anda Yakin Ingin Menyimpan Data Penyesuaian Pembayaran ?", prepareData(), function () {
                reloadTampilan();
            });
        }
    }
</script>
