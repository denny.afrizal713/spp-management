<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Pencarian Data Siswa</legend>
        <div class="col">
            <div class="row form-group">
                <div class="col">
                    <label class="display-label" for="nis">Nomor Induk Siswa</label>
                </div>
                <div class="col">
                    <label class="display-label" for="nama">Nama Siswa</label>
                </div>
                <div class="col">
                    <label class="display-label" for="kelas">Kelas</label>
                </div>
                <div class="col">
                    <label class="display-label" for="tahun">Tahun Ajaran</label>
                </div>
                <div class="col"></div>
            </div>
            <div class="row form-group">
                <div class="col">
                    <input id="nis" name="nis" placeholder="Nomor Induk Siswa" class="form-control"/>
                </div>
                <div class="col">
                    <input id="nama" name="nama" placeholder="Nama Siswa" class="form-control"/>
                </div>
                <div class="col">
                    <select id="kelas" name="kelas" class="form-select"></select>
                </div>
                <div class="col">
                    <select id="tahun" name="tahun" class="form-select"></select>
                </div>
                <div class="col">
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="cariData()">
                        <em class="fa fa-search"></em>
                        Cari
                    </button>
                </div>
            </div>
        </div>
    </fieldset>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered display nowrap">
                <thead>
                <tr>
                    <th rowspan="2" class="align-middle">No</th>
                    <th rowspan="2" class="align-middle">NIS</th>
                    <th rowspan="2" class="align-middle">Nama</th>
                    <th rowspan="2" class="align-middle">Kelas</th>
                    <th rowspan="2" class="align-middle">Uang PSB</th>
                    <th colspan="14" scope="colgroup" class="align-middle header-tengah">SPP Tahun Berjalan</th>
                    <th rowspan="2" class="align-middle">Jumlah</th>
                    <th rowspan="2" class="align-middle">Action</th>
                </tr>
                <tr>
                    <th scope="col">SPP TL</th>
                    <th scope="col">D. Ulang</th>
                    <th scope="col">Juli</th>
                    <th scope="col">Agustus</th>
                    <th scope="col">September</th>
                    <th scope="col">Oktober</th>
                    <th scope="col">November</th>
                    <th scope="col">Desember</th>
                    <th scope="col">Januari</th>
                    <th scope="col">Februari</th>
                    <th scope="col">Maret</th>
                    <th scope="col">April</th>
                    <th scope="col">Mei</th>
                    <th scope="col">Juni</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        appendOptionElement(document.getElementById("kelas"), cariDataKelas(), "");
        appendOptionElement(document.getElementById("tahun"), ambilData("/siswa/tahun-ajaran"), "");
        cariData();
    });

    function parameterCariData(page, size) {
        var nis = $("#nis").val();
        if (nis == null) {
            nis = "";
        }
        var nama = $("#nama").val();
        if (nama == null) {
            nama = "";
        }
        var kelas = $("#kelas option:selected").text();
        if (kelas == null || kelas === "Semua") {
            kelas = "";
        }
        var tahun = $("#tahun option:selected").text();
        if (tahun == null || tahun === "Semua") {
            tahun = "";
        }
        var data = {};
        data["nis"] = nis;
        data["nama"] = nama;
        data["kelas"] = kelas;
        data["tahunAjaran"] = tahun;
        data["page"] = page
        if (page == null) {
            data["page"] = 0
        }
        data["size"] = size;
        if (size == null) {
            data["size"] = 10
        }
        return data;
    }

    function ubah(no) {
        var xtable = $('#example').DataTable();
        var data = xtable.row(no - 1).data();
        var nis = data[1];
        var kelas = data[3];
        redirect("/siswa/detail?nis=" + nis + "&kelas=" + kelas);
    }

    function cariData() {
        var data = ambilDataDenganParameter("/siswa/list", parameterCariData());
        var xtable = $('#example').DataTable();
        xtable.clear().draw();
        for (var x in data.data) {
            var btnUbah = "<button class='btn btn-danger btn-sm btn-fix' type='button' onclick='ubah(" + data.data[x].no + ")'><em class='fa fa-pencil'></em>&nbsp;Ubah</button>";
            xtable.row.add([
                data.data[x].no,
                data.data[x].nis,
                data.data[x].nama,
                data.data[x].kelas,
                formatMoneyDisplay(data.data[x].uangPangkal),
                formatMoneyDisplay(data.data[x].sppTl),
                formatMoneyDisplay(data.data[x].daftarUlang),
                formatMoneyDisplay(data.data[x].juli),
                formatMoneyDisplay(data.data[x].agustus),
                formatMoneyDisplay(data.data[x].september),
                formatMoneyDisplay(data.data[x].oktober),
                formatMoneyDisplay(data.data[x].november),
                formatMoneyDisplay(data.data[x].desember),
                formatMoneyDisplay(data.data[x].januari),
                formatMoneyDisplay(data.data[x].februari),
                formatMoneyDisplay(data.data[x].maret),
                formatMoneyDisplay(data.data[x].april),
                formatMoneyDisplay(data.data[x].mei),
                formatMoneyDisplay(data.data[x].juni),
                formatMoneyDisplay(data.data[x].jumlah),
                btnUbah
            ]);
        }
        xtable.draw();
    }
</script>
