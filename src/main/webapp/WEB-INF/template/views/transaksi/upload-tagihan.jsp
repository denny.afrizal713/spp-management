<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row form-group">
    <div class="col">
        <fieldset class="fieldset">
            <legend class="legend">Upload File Rincian Pembayaran</legend>
            <form method="POST" enctype="multipart/form-data">
                <div class="row form-group">
                    <div class="col-sm-3">
                        <input type="file" id="file" name="file" class="form-control form-control-sm"/>
                    </div>
                    <div class="col">
                        <div class="d-grid gap-2 d-md-flex">
                            <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="uploadData()">
                                <em class="fa fa-upload"></em>
                                Upload
                            </button>
                            <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="reloadTampilan()">
                                <em class="fa fa-times"></em>
                                Batal
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <div class="row form-group">
                <div class="col">
                        <span>
                            <a href="/file/unduh" class="txt-link">
                                <em class="fa fa-download"></em>
                                Download Template
                            </a>
                        </span>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12" id="data">
        <table id="example" class="table table-striped table-bordered display nowrap">
            <thead>
            <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Kelas</th>
                <th>Uang Pangkal</th>
                <th>Daftar Ulang</th>
                <th>SPP TL</th>
                <th>Juli</th>
                <th>Agustus</th>
                <th>September</th>
                <th>Oktober</th>
                <th>November</th>
                <th>Desember</th>
                <th>Januari</th>
                <th>Februari</th>
                <th>Maret</th>
                <th>April</th>
                <th>Mei</th>
                <th>Juni</th>
                <th>Jumlah</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <br>
    <div class="col-md-12" id="gagal">
        <table id="example1" class="table table-striped table-bordered display nowrap">
            <thead>
            <tr>
                <th>No</th>
                <th>Keterangan</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    var dataRequest = [];
    var dataGagal = [];

    $(document).ready(function () {
        $('#data').hide();
        $('#gagal').hide();
    });

    function bacaFile() {
        $('#data').hide();
        $('#gagal').hide();
        dataRequest = [];
        dataGagal = [];
        var file = document.getElementById("file").value;
        var ext = cekExtensi(file)[0];
        if (ext !== "xls" && ext !== "xlsx") {
            tampilNotif('warning', "Peringatan", "Harap Upload File dengan Extensi .xls atau .xlsx");
        } else {
            var data = prosesUpload("/file/unggah", new FormData(document.forms[0])).data;
            setTabelData(data);
            if (data.listGagal.length > 0) {
                tampilNotif('warning', "Peringatan", "Terdapat Beberapa Data Tidak Sesuai. Lihat Tabel Dibawah Untuk Detailnya");
            }
        }
    }

    function setTabelData(data) {
        setTabelDataUpload(data.listData);
        $('#data').show();
        if (data.listGagal.length > 0) {
            setTabelDataGagal(data.listGagal);
            dataGagal = data.listGagal;
        } else {
            dataRequest = data.listData;
        }
        aturTampilanTabel();
    }

    function setTabelDataUpload(data) {
        var xtable = $('#example').DataTable();
        xtable.clear().draw();
        for (var x in data) {
            xtable.row.add([
                data[x].no,
                data[x].nis.replace(".0", ""),
                data[x].nama,
                data[x].jenisKelamin,
                data[x].kelas,
                data[x].uangPangkal,
                data[x].daftarUlang,
                data[x].sppTl,
                data[x].juli,
                data[x].agustus,
                data[x].september,
                data[x].oktober,
                data[x].november,
                data[x].desember,
                data[x].januari,
                data[x].februari,
                data[x].maret,
                data[x].april,
                data[x].mei,
                data[x].juni,
                data[x].jumlah
            ])
        }
        xtable.draw();
        aturTampilanTabel();
    }

    function cekExtensi(filename) {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
    }

    document.getElementById("file").addEventListener('change', () => {
        bacaFile();
    });

    function uploadData() {
        if (dataRequest.length === 0 && dataGagal.length === 0) {
            tampilNotif('warning', "Peringatan", "Belum Ada File Yang Diupload");
        } else if (dataGagal.length > 0) {
            tampilNotif('warning', "Peringatan", "Terdapat Beberapa Data Tidak Sesuai. Lihat Tabel Dibawah Untuk Detailnya");
        } else {
            if (cekKelas(dataRequest) === false) {
                tampilNotif('warning', "Peringatan", "Terdapat Beberapa Kelas Yang Berbeda, Harap Diseragamkan Untuk Pemilihan Kelasnya");
            } else {
                konfirmasiProses("/file/unggah/simpan", "Anda Yakin Ingin Simpan Data?", dataRequest, function () {
                    redirect("/upload/tagihan");
                });
            }
        }
    }

    function cekKelas(data) {
        var hasil = true;
        var kelas = data[0].kelas;
        for (var x in data) {
            if (kelas !== data[x].kelas) {
                hasil = false;
                break;
            }
        }
        return hasil;
    }
</script>
