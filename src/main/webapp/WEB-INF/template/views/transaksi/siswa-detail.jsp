<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Biodata Siswa</legend>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">NIS Siswa</label>
            </div>
            <div class="col col-sm-6">
                <label id="nis" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Nama Siswa</label>
            </div>
            <div class="col col-sm-6">
                <label id="nama" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Email</label>
            </div>
            <div class="col col-sm-3">
                <label id="email" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Kelas</label>
            </div>
            <div class="col col-sm-6">
                <label id="kelas" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Tahun Ajaran</label>
            </div>
            <div class="col col-sm-6">
                <label id="tahun" class="display-label"></label>
            </div>
        </div>
    </fieldset>
    <br>
    <fieldset class="fieldset">
        <legend class="legend">Tunggakan Pembayaran</legend>
        <fieldset class="fieldset">
            <div class="row form-group">
                <div class="col">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="up">Uang&nbsp;Pangkal</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="up" name="up" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="xup" name="xup"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('up','xup')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="up">Buku</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="bk" name="bk" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="xbk" name="xbk"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('bk','xbk')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="du">Daftar&nbsp;Ulang</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="du" name="du" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="xdu" name="xdu"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('du','xdu')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="up">Seragam</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="sr" name="sr" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="xsr" name="xsr"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('sr','xsr')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="tl">SPP&nbsp;TL</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="tl" name="tl" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="xtl" name="xtl"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('tl','xtl')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <br>
        <div class="text-center">
            <h2 class="base-title">Daftar Pembayaran SPP Tahun Berjalan</h2>
        </div>
        <br>
        <div class="row form-group">
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="juli">1.&nbsp;Juli</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="juli" name="juli" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="jul" name="jul"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('juli','jul')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="agustus">2.&nbsp;Agustus</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="agustus" name="agustus" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="aug" name="aug"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="edit('agustus','aug')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="september">3.&nbsp;September</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="september" name="september" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="sep" name="sep"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="edit('september','sep')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="oktober">4.&nbsp;Oktober</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="oktober" name="oktober" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="oct" name="oct"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="edit('oktober','oct')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="november">5.&nbsp;November</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="november" name="november" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="nov" name="nov"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="edit('november','nov')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="desember">6.&nbsp;Desember</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="desember" name="desember" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="dec" name="dec"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="edit('desember','dec')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="januari">7.&nbsp;Januari</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="januari" name="januari" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="jan" name="jan"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="edit('januari','jan')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="februari">8.&nbsp;Februari</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="februari" name="februari" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="feb" name="feb"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="edit('februari','feb')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="maret">9.&nbsp;Maret</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="maret" name="maret" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="mar" name="mar"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('maret','mar')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="april">10.&nbsp;April</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="april" name="april" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="apr" name="apr"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('april','apr')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="mei">11.&nbsp;Mei</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="mei" name="mei" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="may" name="may"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('mei','may')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="juni">12.&nbsp;Juni</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="juni" name="juni" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="jun" name="jun"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('juni','jun')">
                                <em class="fa fa-pencil"></em>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <br>
        <div class="row form-group">
            <div class="col">
                <label class="display-label" for="tb">Total Tunggakan SPP Tahun Berjalan</label>
            </div>
            <div class="col col-sm-3">
                <input id="tb" name="tb" value="0" disabled="disabled" class="form-control kolom-uang money-format"/>
            </div>
        </div>
    </fieldset>
    <br>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="hapusDataSiswa()">
            <em class="fa fa-eraser"></em>
            Hapus
        </button>
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="reloadTampilan();">
            <em class="fa fa-times"></em>
            Batal
        </button>
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="back()">
            <em class="fa fa-undo"></em>
            Kembali
        </button>
    </div>
    <div class="modal" id="update" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modal-header-warna text-center">
                    <h4>
                        <strong>
                            Ubah Nominal Pembayaran
                        </strong>
                    </h4>
                </div>
                <div class="modal-body">
                    <input id="action" name="action" value="0" class="form-control kolom-uang money-format"/>
                    <input type="hidden" id="idx" name="idx"/>
                    <input type="hidden" id="idt" name="idt"/>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="perbarui()">
                        <em class="fa fa-save"></em>
                        Simpan
                    </button>
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="batal()">
                        <em class="fa fa-times"></em>
                        Batal
                    </button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="xnis" name="xnis" value="${nis}"/>
    <input type="hidden" id="xnama" name="xnama"/>
    <input type="hidden" id="xjkel" name="xjkel"/>
    <input type="hidden" id="xkelas" name="xkelas" value="${kelas}"/>
    <input type="hidden" id="xtahun" name="xtahun"/>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        $("#update").modal("hide");
        aturData();
    });

    function batal() {
        $("#update").modal("hide");
    }

    function aturData() {
        var response = ambilData("/siswa/detail/" + $("#xnis").val() + "/" + cariJenjang($("#xkelas").val()));
        var data = response.data;
        if (data != null) {
            document.getElementById("nis").innerHTML = data.nis;
            document.getElementById("nama").innerHTML = data.nama;
            document.getElementById("email").innerHTML = data.email;
            document.getElementById("kelas").innerHTML = data.kelas;
            document.getElementById("tahun").innerHTML = data.tahunAjaran;
            $("#xnama").val(data.nama);
            $("#xjkel").val(data.jenisKelamin);
            $("#xtahun").val(data.tahunAjaran);
            $("#up").val(data.transaksi.upSisa);
            $("#du").val(data.transaksi.duSisa);
            $("#tl").val(data.transaksi.sppTlSisa);
            $("#juli").val(data.sppTahunBerjalan.sppJuli);
            $("#agustus").val(data.sppTahunBerjalan.sppAgustus);
            $("#september").val(data.sppTahunBerjalan.sppSeptember);
            $("#oktober").val(data.sppTahunBerjalan.sppOktober);
            $("#november").val(data.sppTahunBerjalan.sppNovember);
            $("#desember").val(data.sppTahunBerjalan.sppDesember);
            $("#januari").val(data.sppTahunBerjalan.sppJanuari);
            $("#februari").val(data.sppTahunBerjalan.sppFebruari);
            $("#maret").val(data.sppTahunBerjalan.sppMaret);
            $("#april").val(data.sppTahunBerjalan.sppApril);
            $("#mei").val(data.sppTahunBerjalan.sppMei);
            $("#juni").val(data.sppTahunBerjalan.sppJuni);
            totalSpp();
            $("#xup").val(data.transaksi.upSisa);
            $("#xdu").val(data.transaksi.duSisa);
            $("#xtl").val(data.transaksi.sppTlSisa);
            $("#jul").val(data.sppTahunBerjalan.sppJuli);
            $("#aug").val(data.sppTahunBerjalan.sppAgustus);
            $("#sep").val(data.sppTahunBerjalan.sppSeptember);
            $("#oct").val(data.sppTahunBerjalan.sppOktober);
            $("#nov").val(data.sppTahunBerjalan.sppNovember);
            $("#dec").val(data.sppTahunBerjalan.sppDesember);
            $("#jan").val(data.sppTahunBerjalan.sppJanuari);
            $("#feb").val(data.sppTahunBerjalan.sppFebruari);
            $("#mar").val(data.sppTahunBerjalan.sppMaret);
            $("#apr").val(data.sppTahunBerjalan.sppApril);
            $("#may").val(data.sppTahunBerjalan.sppMei);
            $("#jun").val(data.sppTahunBerjalan.sppJuni);
        }
    }

    function back() {
        redirect("/siswa");
    }

    function totalSpp() {
        var juli = parseFloat(removeMoneyDelimiters($("#juli").val()));
        var agustus = parseFloat(removeMoneyDelimiters($("#agustus").val()));
        var september = parseFloat(removeMoneyDelimiters($("#september").val()));
        var oktober = parseFloat(removeMoneyDelimiters($("#oktober").val()));
        var november = parseFloat(removeMoneyDelimiters($("#november").val()));
        var desember = parseFloat(removeMoneyDelimiters($("#desember").val()));
        var januari = parseFloat(removeMoneyDelimiters($("#januari").val()));
        var februari = parseFloat(removeMoneyDelimiters($("#februari").val()));
        var maret = parseFloat(removeMoneyDelimiters($("#maret").val()));
        var april = parseFloat(removeMoneyDelimiters($("#april").val()));
        var mei = parseFloat(removeMoneyDelimiters($("#mei").val()));
        var juni = parseFloat(removeMoneyDelimiters($("#juni").val()));
        var total = juli + agustus + september + oktober + november + desember + januari + februari + maret + april + mei + juni;
        $("#tb").val(formatMoneyDisplay(total));
    }

    function edit(x, z) {
        var elem = document.getElementById(x);
        var elemx = document.getElementById(z);
        var nilai = elem.value;
        var value = elemx.value;
        if (nilai === "0" && value === "0") {
            tampilNotif('warning', "Peringatan", "Tidak Bisa Merubah Nilai SPP / Uang Pangkal / Daftar Ulang Yang Sudah Dilunasi");
        } else {
            $("#idx").val(x);
            $("#idt").val(z);
            $("#update").modal("show");
        }
    }

    function hitungTunggakanSpp() {
        var jul = parseFloat($("#jul").val());
        var aug = parseFloat($("#aug").val());
        var sep = parseFloat($("#sep").val());
        var oct = parseFloat($("#oct").val());
        var nov = parseFloat($("#nov").val());
        var dec = parseFloat($("#dec").val());
        var jan = parseFloat($("#jan").val());
        var feb = parseFloat($("#feb").val());
        var mar = parseFloat($("#mar").val());
        var apr = parseFloat($("#apr").val());
        var may = parseFloat($("#may").val());
        var jun = parseFloat($("#jun").val());
        var total = 0;
        var tgl = new Date().getMonth();
        switch (tgl) {
            case 6:
                total = jul;
                break;
            case 7:
                total = jul + aug;
                break;
            case 8:
                total = jul + aug + sep;
                break;
            case 9:
                total = jul + aug + sep + oct;
                break;
            case 10:
                total = jul + aug + sep + oct + nov;
                break;
            case 11:
                total = jul + aug + sep + oct + nov + dec;
                break;
            case 0:
                total = jul + aug + sep + oct + nov + dec + jan;
                break;
            case 1:
                total = jul + aug + sep + oct + nov + dec + jan + feb;
                break;
            case 2:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar;
                break;
            case 3:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar + apr;
                break;
            case 4:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar + apr + may;
                break;
            case 5:
                total = jul + aug + sep + oct + nov + dec + jan + feb + mar + apr + may + jun;
                break;
        }
        return total;
    }

    function cekTunggakan() {
        var dataTunggakan = {};
        dataTunggakan["uangPangkal"] = parseFloat($("#xup").val());
        dataTunggakan["daftarUlang"] = parseFloat($("#xdu").val());
        dataTunggakan["sppTl"] = parseFloat($("#xtl").val());
        dataTunggakan["sppTb"] = hitungTunggakanSpp();
        return dataTunggakan;
    }

    function hapusDataSiswa() {
        var nis = $("#xnis").val();
        var nama = $("#xnama").val();
        var tunggakan = cekTunggakan();
        var notif = "Anda Yakin Ingin Menghapus Data Tunggakan " + nama + "?";
        if (tunggakan.uangPangkal > 0) {
            notif = notif + "\r\n";
            notif = notif + "Tunggakan Uang Pangkal Sebesar Rp. " + formatMoneyDisplay(tunggakan.uangPangkal);
        }
        if (tunggakan.daftarUlang > 0) {
            notif = notif + "\r\n";
            notif = notif + "Tunggakan Daftar Ulang Sebesar Rp. " + formatMoneyDisplay(tunggakan.daftarUlang);
        }
        if (tunggakan.sppTl > 0) {
            notif = notif + "\r\n";
            notif = notif + "Tunggakan SPP Tahun Lalu Sebesar Rp. " + formatMoneyDisplay(tunggakan.sppTl);
        }
        if (tunggakan.sppTb > 0) {
            notif = notif + "\r\n";
            notif = notif + "Tunggakan SPP Tahun Berjalan Sebesar Rp. " + formatMoneyDisplay(tunggakan.sppTb);
        }
        konfirmasiProses("/siswa/hapus/" + nis + "/" + cariJenjang($("#xkelas").val()), notif, null, function () {
            redirect("/siswa");
        });
    }

    function requestUbahData() {
        var data = {};
        data["nis"] = $("#xnis").val();
        data["nama"] = $("#xnama").val();
        data["kelas"] = $("#xkelas").val();
        data["jenisKelamin"] = $("#xjkel").val();
        data["tahunAjaran"] = $("#xtahun").val();
        data["uangPangkal"] = parseFloat(removeMoneyDelimiters($("#up").val()));
        data["daftarUlang"] = parseFloat(removeMoneyDelimiters($("#du").val()));
        data["sppTl"] = parseFloat(removeMoneyDelimiters($("#tl").val()));
        data["juli"] = parseFloat(removeMoneyDelimiters($("#juli").val()));
        data["agustus"] = parseFloat(removeMoneyDelimiters($("#agustus").val()));
        data["september"] = parseFloat(removeMoneyDelimiters($("#september").val()));
        data["oktober"] = parseFloat(removeMoneyDelimiters($("#oktober").val()));
        data["november"] = parseFloat(removeMoneyDelimiters($("#november").val()));
        data["desember"] = parseFloat(removeMoneyDelimiters($("#desember").val()));
        data["januari"] = parseFloat(removeMoneyDelimiters($("#januari").val()));
        data["februari"] = parseFloat(removeMoneyDelimiters($("#februari").val()));
        data["maret"] = parseFloat(removeMoneyDelimiters($("#maret").val()));
        data["april"] = parseFloat(removeMoneyDelimiters($("#april").val()));
        data["mei"] = parseFloat(removeMoneyDelimiters($("#mei").val()));
        data["juni"] = parseFloat(removeMoneyDelimiters($("#juni").val()));
        data["total"] = parseFloat(removeMoneyDelimiters($("#tb").val()));
        return data;
    }

    function perbarui() {
        var act = $("#action").val();
        var idx = $("#idx").val();
        var idt = $("#idt").val();
        var data = $("#" + idt).val();
        var desc = "";
        if (idx === "up") {
            desc = "Uang Pangkal";
        } else if (idx === "tl") {
            desc = "SPP Tahun Lalu";
        } else {
            desc = "SPP Bulan " + hurufBesarPertama(idx);
        }
        $("#" + idx).val(act);
        $("#update").modal("hide");
        totalSpp();
        konfirmasiProses("/siswa/ubah", "Anda Yakin Ingin Ubah Data Pembayaran Siswa?", requestUbahData(), function () {
            redirect("/siswa/detail?nis=" + $("#xnis").val() + "&kelas=" + $("#xkelas").val());
        });
    }
</script>
