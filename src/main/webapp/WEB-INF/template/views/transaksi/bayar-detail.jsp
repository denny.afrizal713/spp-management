<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Biodata Siswa</legend>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">NIS Siswa</label>
            </div>
            <div class="col col-sm-6">
                <label id="nis" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Nama Siswa</label>
            </div>
            <div class="col col-sm-6">
                <label id="nama" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Kelas</label>
            </div>
            <div class="col col-sm-6">
                <label id="kelas" class="display-label"></label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label">Tahun Ajaran</label>
            </div>
            <div class="col col-sm-6">
                <label id="tahun" class="display-label"></label>
            </div>
        </div>
    </fieldset>
    <br>
    <fieldset class="fieldset">
        <legend class="legend">Tunggakan Pembayaran</legend>
        <fieldset class="fieldset">
            <div class="row form-group">
                <div class="col">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="up">Uang&nbsp;Pangkal</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="up" name="up" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('up')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="up">Buku</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="bk" name="bk" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="xbk" name="xbk"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('bk','xbk')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="du">Daftar&nbsp;Ulang</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="du" name="du" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('du')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="up">Seragam</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="sr" name="sr" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                            <input type="hidden" id="xsr" name="xsr"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="edit('sr','xsr')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="tl">SPP&nbsp;TL</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="tl" name="tl" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('tl')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <br>
        <div class="text-center">
            <h2 class="base-title">Daftar Pembayaran SPP Tahun Berjalan</h2>
        </div>
        <br>
        <div class="row form-group">
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="juli">1.&nbsp;Juli</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="juli" name="juli" value="0" disabled="disabled" onchange="perhitunganTotalSpp()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('juli')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="agustus">2.&nbsp;Agustus</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="agustus" name="agustus" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()" class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="bayar('agustus')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="september">3.&nbsp;September</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="september" name="september" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()" class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="bayar('september')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="oktober">4.&nbsp;Oktober</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="oktober" name="oktober" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()" class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="bayar('oktober')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="november">5.&nbsp;November</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="november" name="november" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()" class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="bayar('november')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="desember">6.&nbsp;Desember</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="desember" name="desember" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()" class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="bayar('desember')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="januari">7.&nbsp;Januari</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="januari" name="januari" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()" class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="bayar('januari')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="februari">8.&nbsp;Februari</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="februari" name="februari" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()" class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button"
                                    onclick="bayar('februari')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="maret">9.&nbsp;Maret</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="maret" name="maret" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('maret')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="april">10.&nbsp;April</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="april" name="april" value="0" disabled="disabled"
                                   onchange="perhitunganTotalSpp()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('april')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="mei">11.&nbsp;Mei</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="mei" name="mei" value="0" disabled="disabled" onchange="perhitunganTotalSpp()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('mei')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="juni">12.&nbsp;Juni</label>
                        </div>
                        <div class="col col-sm-5">
                            <input id="juni" name="juni" value="0" disabled="disabled" onchange="perhitunganTotalSpp()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                        <div class="col col-sm-3">
                            <button class="btn btn-danger btn-sm btn-half" type="button" onclick="bayar('juni')">
                                <em class="fa fa-money"></em>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <br>
        <div class="row form-group">
            <div class="col">
                <label class="display-label" for="tb">Total Tunggakan SPP Tahun Berjalan</label>
            </div>
            <div class="col col-sm-3">
                <input id="tb" name="tb" value="0" disabled="disabled" class="form-control kolom-uang money-format"/>
            </div>
        </div>
    </fieldset>
    <br>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-danger btn-sm btn-fix-plus" type="button" onclick="sesuai()">
            <em class="fa fa-exchange"></em>
            Penyesuaian
        </button>
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="kembali()">
            <em class="fa fa-undo"></em>
            Kembali
        </button>
    </div>
    <div class="modal" id="pay" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modal-header-warna text-center">
                    <h4>
                        <strong id="perjudulan"></strong>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="total">Total</label>
                        </div>
                        <div class="col col-sm-8">
                            <input id="total" name="total" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="dibayar">Dibayar</label>
                        </div>
                        <div class="col col-sm-8">
                            <input id="dibayar" name="dibayar" value="0" onchange="perhitungan()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-4">
                            <label class="display-label" for="kurang">Kurang</label>
                        </div>
                        <div class="col col-sm-8">
                            <input id="kurang" name="kurang" value="0" disabled="disabled"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <input type="hidden" id="idx" name="idx"/>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="pembayaran()">
                        <em class="fa fa-save"></em>
                        Simpan
                    </button>
                    <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="gagal()">
                        <em class="fa fa-times"></em>
                        Batal
                    </button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="xnis" name="xnis" value="${nis}"/>
    <input type="hidden" id="xnama" name="xnama"/>
    <input type="hidden" id="xjkel" name="xjkel"/>
    <input type="hidden" id="xkelas" name="xkelas" value="${kelas}"/>
    <input type="hidden" id="xtahun" name="xtahun"/>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        $("#pay").modal("hide");
        dapatkanData();
    });

    function kembali() {
        var ckelas = $("#xkelas").val().split(" ");
        var dataMenu = ambilDataDenganParameter("/transaksi/menu/desc", ckelas).data;
        if (dataMenu == null) {
            redirect("/");
        } else {
            redirect("/siswa/transaksi?id=" + dataMenu.idMenu);
        }
    }

    function sesuai() {
        redirect("/siswa/transaksi/penyesuaian?nis=" + $("#xnis").val() + "&kelas=" + $("#xkelas").val());
    }

    function pembayaran() {
        var jenis = $("#idx").val();
        var url = "";
        var desc = "";
        if (jenis === "up") {
            desc = "Uang Pangkal";
            url = "/transaksi/pembayaran/uang-pangkal";
        } else if (jenis === "du") {
            desc = "Daftar Ulang";
            url = "/transaksi/pembayaran/daftar-ulang";
        } else if (jenis === "tl") {
            desc = "SPP Tahun Lalu";
            url = "/transaksi/pembayaran/spp-tl";
        } else {
            desc = "SPP";
            url = "/transaksi/pembayaran/spp";
        }
        $("#pay").modal("hide");
        konfirmasiProses(url, "Anda Yakin Ingin Menyimpan Data Pembayaran " + desc + "?", requestDataBayar(), function () {
            reloadTampilan();
        });
    }

    function requestDataBayar() {
        var type = $("#idx").val();
        if (type === "up" || type === "du" || type === "tl") {
            type = "-";
        }
        var data = {};
        data["nis"] = $("#xnis").val();
        data["bulan"] = hurufBesarPertama(type);
        data["jenjang"] = cariJenjang($("#xkelas").val());
        data["jumlahBayar"] = parseFloat(removeMoneyDelimiters($("#dibayar").val()));
        return data;
    }

    function perhitungan() {
        var total = parseFloat(removeMoneyDelimiters($("#total").val()));
        var bayar = parseFloat(removeMoneyDelimiters($("#dibayar").val()));
        var kurang = total - bayar;
        $("#kurang").val(formatMoneyDisplay(kurang));
    }

    function dapatkanData() {
        var response = ambilData("/siswa/detail/" + $("#xnis").val() + "/" + cariJenjang($("#xkelas").val()));
        var data = response.data;
        if (data != null) {
            document.getElementById("nis").innerHTML = data.nis;
            document.getElementById("nama").innerHTML = data.nama;
            document.getElementById("kelas").innerHTML = data.kelas;
            document.getElementById("tahun").innerHTML = data.tahunAjaran;
            $("#xnama").val(data.nama);
            $("#xjkel").val(data.jenisKelamin);
            $("#xkelas").val(data.kelas);
            $("#xtahun").val(data.tahunAjaran);
            $("#up").val(data.transaksi.upSisa);
            $("#du").val(data.transaksi.duSisa);
            $("#tl").val(data.transaksi.sppTlSisa);
            aturDataSpp(data.sppTahunBerjalan);
        }
    }

    function aturDataSpp(data) {
        var jul = 0;
        var aug = 0;
        var sep = 0;
        var oct = 0;
        var nov = 0;
        var dec = 0;
        var jan = 0;
        var feb = 0;
        var mar = 0;
        var apr = 0;
        var may = 0;
        var jun = 0;
        var tgl = new Date().getMonth();
        switch (tgl) {
            case 6:
                jul = data.sppJuli;
                break;
            case 7:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                break;
            case 8:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                break;
            case 9:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                break;
            case 10:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                break;
            case 11:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                dec = data.sppDesember;
                break;
            case 0:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                dec = data.sppDesember;
                jan = data.sppJanuari;
                break;
            case 1:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                dec = data.sppDesember;
                jan = data.sppJanuari;
                feb = data.sppFebruari;
                break;
            case 2:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                dec = data.sppDesember;
                jan = data.sppJanuari;
                feb = data.sppFebruari;
                mar = data.sppMaret;
                break;
            case 3:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                dec = data.sppDesember;
                jan = data.sppJanuari;
                feb = data.sppFebruari;
                mar = data.sppMaret;
                apr = data.sppApril;
                break;
            case 4:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                dec = data.sppDesember;
                jan = data.sppJanuari;
                feb = data.sppFebruari;
                mar = data.sppMaret;
                apr = data.sppApril;
                may = data.sppMei;
                break;
            case 5:
                jul = data.sppJuli;
                aug = data.sppAgustus;
                sep = data.sppSeptember;
                oct = data.sppOktober;
                nov = data.sppNovember;
                dec = data.sppDesember;
                jan = data.sppJanuari;
                feb = data.sppFebruari;
                mar = data.sppMaret;
                apr = data.sppApril;
                may = data.sppMei;
                jun = data.sppJuni;
                break;
        }
        $("#juli").val(formatMoneyDisplay(jul));
        $("#agustus").val(formatMoneyDisplay(aug));
        $("#september").val(formatMoneyDisplay(sep));
        $("#oktober").val(formatMoneyDisplay(oct));
        $("#november").val(formatMoneyDisplay(nov));
        $("#desember").val(formatMoneyDisplay(dec));
        $("#januari").val(formatMoneyDisplay(jan));
        $("#februari").val(formatMoneyDisplay(feb));
        $("#maret").val(formatMoneyDisplay(mar));
        $("#april").val(formatMoneyDisplay(apr));
        $("#mei").val(formatMoneyDisplay(may));
        $("#juni").val(formatMoneyDisplay(jun));
        perhitunganTotalSpp();
    }

    function perhitunganTotalSpp() {
        var juli = parseFloat(removeMoneyDelimiters($("#juli").val()));
        var agustus = parseFloat(removeMoneyDelimiters($("#agustus").val()));
        var september = parseFloat(removeMoneyDelimiters($("#september").val()));
        var oktober = parseFloat(removeMoneyDelimiters($("#oktober").val()));
        var november = parseFloat(removeMoneyDelimiters($("#november").val()));
        var desember = parseFloat(removeMoneyDelimiters($("#desember").val()));
        var januari = parseFloat(removeMoneyDelimiters($("#januari").val()));
        var februari = parseFloat(removeMoneyDelimiters($("#februari").val()));
        var maret = parseFloat(removeMoneyDelimiters($("#maret").val()));
        var april = parseFloat(removeMoneyDelimiters($("#april").val()));
        var mei = parseFloat(removeMoneyDelimiters($("#mei").val()));
        var juni = parseFloat(removeMoneyDelimiters($("#juni").val()));
        var total = juli + agustus + september + oktober + november + desember + januari + februari + maret + april + mei + juni;
        $("#tb").val(formatMoneyDisplay(total));
    }

    function bayar(x) {
        var value = $("#" + x).val();
        if (parseFloat(value) === 0) {
            tampilNotif('warning', "Peringatan", "Tidak Bisa Melakukan Pembayaran Uang Pangkal / Daftar Ulang / SPP yang telah Lunas Atau Belum Waktunya Pembayaran");
        } else {
            $("#total").val(value);
            $("#kurang").val(value);
            $("#dibayar").val("0");
            var desc = "";
            if (x === "tl") {
                desc = "Pembayaran SPP Tahun Lalu";
            } else if (x === "up") {
                desc = "Pembayaran Uang Pangkal";
            } else if (x === "du") {
                desc = "Pembayaran Daftar Ulang";
            } else {
                desc = "Pembayaran SPP Bulan " + hurufBesarPertama(x);
            }
            $("#idx").val(x);
            var judul = document.getElementById("perjudulan");
            judul.innerHTML = desc;
            $("#pay").modal("show");
        }
    }

    function gagal() {
        $("#pay").modal("hide");
    }
</script>
