<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form>
    <fieldset class="fieldset">
        <legend class="legend">Tambahkan Data Siswa Baru</legend>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label" for="nis">NIS Siswa</label>
            </div>
            <div class="col col-sm-2">
                <input id="nis" name="nis" placeholder="NIS Siswa" class="form-control"/>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label" for="nama">Nama Siswa</label>
            </div>
            <div class="col col-sm-3">
                <input id="nama" name="nama" placeholder="Nama Siswa" class="form-control"/>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label" for="email">Email</label>
            </div>
            <div class="col col-sm-3">
                <input id="email" name="email" placeholder="Email" class="form-control"/>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label" for="jkel">Jenis Kelamin</label>
            </div>
            <div class="col col-sm-2">
                <select id="jkel" name="jkel" class="form-select"></select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label" for="kelas">Kelas</label>
            </div>
            <div class="col col-sm-3">
                <select id="kelas" name="kelas" class="form-select"></select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-sm-2">
                <label class="display-label" for="tahun">Tahun Ajaran</label>
            </div>
            <div class="col col-sm-2">
                <select id="tahun" name="tahun" class="form-select"></select>
            </div>
        </div>
    </fieldset>
    <br>
    <fieldset class="fieldset">
        <legend class="legend">Tambahkan Jumlah Pembayaran</legend>
        <fieldset class="fieldset">
            <div class="row form-group">
                <div class="col">
                    <label class="display-label" for="up">Uang Pangkal</label>
                </div>
                <div class="col">
                    <input id="up" name="up" value="0" class="form-control kolom-uang money-format"/>
                </div>
                <div class="col">
                    <label class="display-label" for="tl">SPP Tahun Lalu</label>
                </div>
                <div class="col">
                    <input id="tl" name="tl" value="0" class="form-control kolom-uang money-format"/>
                </div>
            </div>
            <div class="row form-group">
                <div class="col">
                    <label class="display-label" for="up">Buku</label>
                </div>
                <div class="col">
                    <input id="bk" name="bk" value="0" class="form-control kolom-uang money-format"/>
                </div>
                <div class="col">
                    <label class="display-label" for="tl">Seragam</label>
                </div>
                <div class="col">
                    <input id="sr" name="sr" value="0" class="form-control kolom-uang money-format"/>
                </div>
            </div>
        </fieldset>
        <br>
        <div class="text-center">
            <h2 class="base-title">Daftar Pembayaran SPP Tahun Berjalan</h2>
        </div>
        <br>
        <div class="row form-group">
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="juli">1. Juli</label>
                        </div>
                        <div class="col">
                            <input id="juli" name="juli" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="agustus">2. Agustus</label>
                        </div>
                        <div class="col">
                            <input id="agustus" name="agustus" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="september">3. September</label>
                        </div>
                        <div class="col">
                            <input id="september" name="september" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="oktober">4. Oktober</label>
                        </div>
                        <div class="col">
                            <input id="oktober" name="oktober" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="november">5. November</label>
                        </div>
                        <div class="col">
                            <input id="november" name="november" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="desember">6. Desember</label>
                        </div>
                        <div class="col">
                            <input id="desember" name="desember" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="januari">7. Januari</label>
                        </div>
                        <div class="col">
                            <input id="januari" name="januari" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="februari">8. Februari</label>
                        </div>
                        <div class="col">
                            <input id="februari" name="februari" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col">
                <fieldset class="fieldset">
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="maret">9. Maret</label>
                        </div>
                        <div class="col">
                            <input id="maret" name="maret" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="april">10. April</label>
                        </div>
                        <div class="col">
                            <input id="april" name="april" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="mei">11. Mei</label>
                        </div>
                        <div class="col">
                            <input id="mei" name="mei" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col">
                            <label class="display-label" for="juni">12. Juni</label>
                        </div>
                        <div class="col">
                            <input id="juni" name="juni" value="0" onchange="hitungSppTotal()"
                                   class="form-control kolom-uang money-format"/>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <br>
        <div class="row form-group">
            <div class="col">
                <label class="display-label" for="tb">Total Tunggakan SPP Tahun Berjalan</label>
            </div>
            <div class="col col-sm-3">
                <input id="tb" name="tb" value="0" disabled="disabled" class="form-control kolom-uang money-format"/>
            </div>
        </div>
    </fieldset>
    <br>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="simpanDataSiswa()">
            <em class="fa fa-save"></em>
            Simpan
        </button>
        <button class="btn btn-danger btn-sm btn-fix" type="button" onclick="reloadTampilan();">
            <em class="fa fa-times"></em>
            Batal
        </button>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {
        appendOptionElement(document.getElementById("kelas"), cariDataKelas(), "");
        appendOptionElement(document.getElementById("jkel"), ambilData("/user/option?key=jkel"), "");
        appendOptionElement(document.getElementById("tahun"), ambilData("/siswa/tahun-ajaran"), "");
    });

    function hitungSppTotal() {
        var juli = parseFloat(removeMoneyDelimiters($("#juli").val()));
        var agustus = parseFloat(removeMoneyDelimiters($("#agustus").val()));
        var september = parseFloat(removeMoneyDelimiters($("#september").val()));
        var oktober = parseFloat(removeMoneyDelimiters($("#oktober").val()));
        var november = parseFloat(removeMoneyDelimiters($("#november").val()));
        var desember = parseFloat(removeMoneyDelimiters($("#desember").val()));
        var januari = parseFloat(removeMoneyDelimiters($("#januari").val()));
        var februari = parseFloat(removeMoneyDelimiters($("#februari").val()));
        var maret = parseFloat(removeMoneyDelimiters($("#maret").val()));
        var april = parseFloat(removeMoneyDelimiters($("#april").val()));
        var mei = parseFloat(removeMoneyDelimiters($("#mei").val()));
        var juni = parseFloat(removeMoneyDelimiters($("#juni").val()));
        var total = juli + agustus + september + oktober + november + desember + januari + februari + maret + april + mei + juni;
        $("#tb").val(formatMoneyDisplay(total));
    }

    function siapkanDataSiswa() {
        var data = {};
        var nis = $("#nis").val();
        var nama = $("#nama").val();
        var email = $("#email").val();
        var jkel = $("#jkel option:selected").text();
        var kelas = $("#kelas option:selected").text();
        var tahun = $("#tahun option:selected").text();
        var uangPangkal = parseFloat(removeMoneyDelimiters($("#up").val()));
        var sppTl = parseFloat(removeMoneyDelimiters($("#tl").val()));
        var juli = parseFloat(removeMoneyDelimiters($("#juli").val()));
        var agustus = parseFloat(removeMoneyDelimiters($("#agustus").val()));
        var september = parseFloat(removeMoneyDelimiters($("#september").val()));
        var oktober = parseFloat(removeMoneyDelimiters($("#oktober").val()));
        var november = parseFloat(removeMoneyDelimiters($("#november").val()));
        var desember = parseFloat(removeMoneyDelimiters($("#desember").val()));
        var januari = parseFloat(removeMoneyDelimiters($("#januari").val()));
        var februari = parseFloat(removeMoneyDelimiters($("#februari").val()));
        var maret = parseFloat(removeMoneyDelimiters($("#maret").val()));
        var april = parseFloat(removeMoneyDelimiters($("#april").val()));
        var mei = parseFloat(removeMoneyDelimiters($("#mei").val()));
        var juni = parseFloat(removeMoneyDelimiters($("#juni").val()));
        var jumlah = parseFloat(removeMoneyDelimiters($("#tb").val()));
        if (nis === "" || nis == null) {
            nis = "-";
        }
        if (nama === "" || nama == null) {
            nama = "-";
        }
        data["nis"] = nis;
        data["nama"] = nama;
        data["kelas"] = kelas;
        data["email"] = email;
        data["jenisKelamin"] = jkel;
        data["tahunAjaran"] = tahun;
        data["uangPangkal"] = uangPangkal;
        data["sppTl"] = sppTl;
        data["juli"] = juli;
        data["agustus"] = agustus;
        data["september"] = september;
        data["oktober"] = oktober;
        data["november"] = november;
        data["desember"] = desember;
        data["januari"] = januari;
        data["februari"] = februari;
        data["maret"] = maret;
        data["april"] = april;
        data["mei"] = mei;
        data["juni"] = juni;
        data["total"] = jumlah;
        return data;
    }

    function simpanDataSiswa() {
        var data = siapkanDataSiswa();
        if (data.nama === "-") {
            tampilNotif('warning', "Peringatan", "Harap Isi Nama Siswa");
        } else if (data.kelas === "Semua") {
            tampilNotif('warning', "Peringatan", "Harap Pilih Kelas Siswa");
        } else if (data.email === "" || data.email == null) {
            tampilNotif('warning', "Peringatan", "Harap Isi Email Siswa");
        } else if (data.jenisKelamin === "Semua") {
            tampilNotif('warning', "Peringatan", "Harap Pilih Jenis Kelamin Siswa");
        } else if (data.tahunAjaran === "Semua") {
            tampilNotif('warning', "Peringatan", "Harap Pilih Tahun Ajaran");
        } else if (data.juli === 0) {
            tampilNotif('warning', "Peringatan", "SPP Juli Tidak Boleh 0");
        } else if (data.agustus === 0) {
            tampilNotif('warning', "Peringatan", "SPP Agustus Tidak Boleh 0");
        } else if (data.september === 0) {
            tampilNotif('warning', "Peringatan", "SPP September Tidak Boleh 0");
        } else if (data.oktober === 0) {
            tampilNotif('warning', "Peringatan", "SPP Oktober Tidak Boleh 0");
        } else if (data.november === 0) {
            tampilNotif('warning', "Peringatan", "SPP November Tidak Boleh 0");
        } else if (data.desember === 0) {
            tampilNotif('warning', "Peringatan", "SPP Desember Tidak Boleh 0");
        } else if (data.januari === 0) {
            tampilNotif('warning', "Peringatan", "SPP Januari Tidak Boleh 0");
        } else if (data.februari === 0) {
            tampilNotif('warning', "Peringatan", "SPP Februari Tidak Boleh 0");
        } else if (data.maret === 0) {
            tampilNotif('warning', "Peringatan", "SPP Maret Tidak Boleh 0");
        } else if (data.april === 0) {
            tampilNotif('warning', "Peringatan", "SPP April Tidak Boleh 0");
        } else if (data.mei === 0) {
            tampilNotif('warning', "Peringatan", "SPP Mei Tidak Boleh 0");
        } else if (data.juni === 0) {
            tampilNotif('warning', "Peringatan", "SPP Juni Tidak Boleh 0");
        } else {
            konfirmasiProses("/siswa/simpan", "Anda Yakin Ingin Simpan Data Siswa?", data, function () {
                redirect("/siswa");
            });
        }
    }
</script>
