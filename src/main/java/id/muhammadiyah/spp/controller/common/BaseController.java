package id.muhammadiyah.spp.controller.common;

import com.google.gson.Gson;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserAkses;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import id.muhammadiyah.spp.service.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class BaseController {

    @Autowired
    protected PenggunaService penggunaService;

    @Autowired
    protected JabatanService jabatanService;

    @Autowired
    protected UploadService uploadService;

    @Autowired
    protected MenuService menuService;

    @Autowired
    protected SiswaService siswaService;

    @Autowired
    protected TransaksiService transaksiService;

    @Autowired
    protected LaporanService laporanService;

    protected static final String LOGIN = "userLogin";
    protected static final String HOME = "redirect:/";

    protected Response resp = new Response();

    protected String toHome() {
        return HOME;
    }

    protected void clearCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] kukis = request.getCookies();
        if (kukis != null) {
            for (Cookie kuki : kukis) {
                kuki.setValue("");
                kuki.setPath("");
                response.addCookie(kuki);
            }
            Cookie cookie = new Cookie("JSESSIONID", "");
            cookie.setPath("");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
    }

    protected Boolean setCookie(HttpServletRequest request) {
        Cookie[] kukis = request.getCookies();
        if (kukis != null) {
            for (Cookie kuki : kukis) {
                if (kuki.getName().equals(LOGIN)) {
                    UserLogin login = new Gson().fromJson(kuki.getValue(), UserLogin.class);
                    request.getSession().setAttribute(LOGIN, login);
                    request.getSession().setMaxInactiveInterval(1);
                }
            }
            return true;
        }
        return false;
    }

    public UserLogin getUserLogin(HttpServletRequest request) {
        try {
            return (UserLogin) request.getSession().getAttribute(LOGIN);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean cekLogin(HttpServletRequest request) {
        try {
            return getUserLogin(request) != null;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean cekAkses(HttpServletRequest request, String menu) {
        try {
            List<UserAkses> aksesList = getUserLogin(request).getAkses();
            UserAkses akses = aksesList.stream().filter(x -> x.getNama().equals(menu)).findFirst().orElse(null);
            if (akses != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
