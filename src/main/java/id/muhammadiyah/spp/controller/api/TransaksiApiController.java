package id.muhammadiyah.spp.controller.api;

import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.model.request.Penyesuaian;
import id.muhammadiyah.spp.data.model.request.TransBayar;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import id.muhammadiyah.spp.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/transaksi")
public class TransaksiApiController extends BaseController {

    @GetMapping("/menu/{id}")
    @ResponseBody
    public Response getMenuById(@PathVariable("id") String id) {
        try {
            return ResponseUtil.setResponse(
                    HttpStatus.OK.value(),
                    "",
                    menuService.cariBerdasarkanId(Long.parseLong(id))
            );
        } catch (Exception e) {
            return ResponseUtil.setResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    e.getMessage(),
                    null
            );
        }
    }

    @PostMapping("/menu/desc")
    @ResponseBody
    public Response getMenuByNama(@Valid @RequestBody List<String> desc) {
        try {
            StringBuilder sb = new StringBuilder();
            desc.forEach(x -> sb.append(x).append(" "));
            return ResponseUtil.setResponse(
                    HttpStatus.OK.value(),
                    "",
                    menuService.cariBerdasarkanNama(sb.toString().substring(0, sb.toString().length() - 1))
            );
        } catch (Exception e) {
            return ResponseUtil.setResponse(
                    HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    e.getMessage(),
                    null
            );
        }
    }

    @PostMapping("/pembayaran/uang-pangkal")
    @ResponseBody
    public Response pembayaranUangPangkal(@Valid @RequestBody TransBayar bayar, HttpServletRequest request) {
        return transaksiService.bayarUangPangkal(setDateUser(request, bayar));
    }

    @PostMapping("/pembayaran/spp")
    @ResponseBody
    public Response pembayaranSppTb(@Valid @RequestBody TransBayar bayar, HttpServletRequest request) {
        return transaksiService.bayarSpp(setDateUser(request, bayar));
    }

    @PostMapping("/pembayaran/spp-tl")
    @ResponseBody
    public Response pembayaranSppTl(@Valid @RequestBody TransBayar bayar, HttpServletRequest request) {
        return transaksiService.bayarSppTl(setDateUser(request, bayar));
    }

    @PostMapping("/pembayaran/daftar-ulang")
    @ResponseBody
    public Response pembayaranDaftarUlang(@Valid @RequestBody TransBayar bayar, HttpServletRequest request) {
        return transaksiService.bayarDaftarUlang(setDateUser(request, bayar));
    }

    @PostMapping("/penyesuaian")
    @ResponseBody
    public Response penyesuaianPembayaran(@Valid @RequestBody Penyesuaian bayar, HttpServletRequest request) {
        Date sekarang = new Date();
        UserLogin login = getUserLogin(request);
        bayar.setUserCreate(login.getUsername());
        bayar.setUserUpdate(login.getUsername());
        bayar.setDateCreate(sekarang);
        bayar.setDateUpdate(sekarang);
        return transaksiService.penyesuaian(bayar);
    }

    public TransBayar setDateUser(HttpServletRequest request, TransBayar trans) {
        Date sekarang = new Date();
        UserLogin login = getUserLogin(request);
        trans.setUserCreate(login.getUsername());
        trans.setUserUpdate(login.getUsername());
        trans.setDateCreate(sekarang);
        trans.setDateUpdate(sekarang);
        return trans;
    }
}
