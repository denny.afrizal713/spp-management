package id.muhammadiyah.spp.controller.api;

import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.entity.core.Pengguna;
import id.muhammadiyah.spp.data.model.request.CariAkunData;
import id.muhammadiyah.spp.data.model.request.CariPengguna;
import id.muhammadiyah.spp.data.model.request.InputPengguna;
import id.muhammadiyah.spp.data.model.request.DetailAkunRequest;
import id.muhammadiyah.spp.data.model.response.DaftarPengguna;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import id.muhammadiyah.spp.util.EncryptUtil;
import id.muhammadiyah.spp.util.NumberUtil;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/user")
public class PenggunaApiController extends BaseController {

    @PostMapping("")
    @ResponseBody
    public Response simpanPengguna(@Valid @RequestBody InputPengguna input, HttpServletRequest request) {
        UserLogin login = this.getUserLogin(request);
        input.setUserCreate(login.getUsername());
        input.setUserUpdate(login.getUsername());
        input.setDateCreate(new Date());
        input.setDateUpdate(new Date());
        return penggunaService.simpanPengguna(input);
    }

    @PostMapping("/list")
    @ResponseBody
    public Response daftarPengguna(@Valid @RequestBody CariPengguna pengguna) {
        List<DaftarPengguna> listPengguna = penggunaService.cariPengguna(pengguna);
        if (listPengguna.isEmpty()) {
            ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
        }
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", listPengguna);
    }

    @PostMapping("/detail")
    @ResponseBody
    public Response detailPengguna(@Valid @RequestBody DetailAkunRequest detail, HttpServletRequest request) {
        Pengguna user;
        if (detail.getUsername().equals("") || detail.getUsername().equals("-") || detail.getUsername() == null) {
            user = penggunaService.cariDenganNip(detail.getNip());
        } else {
            user = penggunaService.getByNipAndUsername(detail.getNip(), detail.getUsername());
        }
        if (user == null) {
            return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
        }
        String userName = this.getUserLogin(request).getUsername();
        user.setDiubahOleh(userName);
        user.setTanggalUbah(new Date());
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", user);
    }

    @GetMapping("/option")
    @ResponseBody
    public Response getOptions(HttpServletRequest request) {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", penggunaService.getOption(request.getParameter("key")));
    }

    @PostMapping("/ubah-password")
    @ResponseBody
    public Response ubahPassword(@Valid @RequestBody DetailAkunRequest ubahPass, HttpServletRequest request) throws NoSuchAlgorithmException {
        Pengguna user = penggunaService.getByNipAndUsername(ubahPass.getNip(), ubahPass.getUsername());
        if (user == null) {
            return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
        }
        if (EncryptUtil.encrypt(ubahPass.getOldPassword()).equals(user.getPassword())) {
            user.setDiubahOleh(this.getUserLogin(request).getUsername());
            user.setTanggalUbah(new Date());
            return penggunaService.ubahPassword(user, ubahPass.getPassword());
        }
        return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(), "Password Tidak Sesuai", null);
    }

    @PostMapping("/reset-password")
    @ResponseBody
    public Response resetPassword(@Valid @RequestBody DetailAkunRequest resetPass, HttpServletRequest request) {
        Pengguna user = penggunaService.cariDenganNip(resetPass.getNip());
        if (user == null) {
            return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
        }
        user.setPassword(NumberUtil.randomPassword());
        String userName = this.getUserLogin(request).getUsername();
        user.setDiubahOleh(userName);
        user.setTanggalUbah(new Date());
        return penggunaService.ubahPassword(user, user.getPassword());
    }

    @PostMapping("/pulih-akun")
    @ResponseBody
    public Response pulihkanAkun(@Valid @RequestBody DetailAkunRequest akun, HttpServletRequest request) {
        Pengguna user = penggunaService.cariDenganNip(akun.getNip());
        if (user == null) {
            return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
        }
        if (user.getTotalGagal() < 3) {
            int kesempatan = 3 - user.getTotalGagal();
            return ResponseUtil.setResponse(HttpStatus.FOUND.value(), "Pengguna masih memiliki " + kesempatan + " Kesempatan untuk Login", null);
        }
        String userName = this.getUserLogin(request).getUsername();
        user.setDiubahOleh(userName);
        user.setTanggalUbah(new Date());
        return penggunaService.pulihkanAkun(user);
    }

    @PostMapping("/hapus")
    @ResponseBody
    public Response hapusAkun(@Valid @RequestBody DetailAkunRequest akun, HttpServletRequest request) {
        String id = "0000000000" + akun.getNip();
        int idx = id.length() - 10;
        String nip = id.substring(idx);
        Pengguna user = penggunaService.cariDenganNip(nip);
        if (user == null) {
            return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
        }
        String userName = this.getUserLogin(request).getUsername();
        user.setDiubahOleh(userName);
        user.setTanggalUbah(new Date());
        return penggunaService.hapusPengguna(user);
    }

    @PostMapping("/cari-akun")
    @ResponseBody
    public Response cariAkun(@Valid @RequestBody CariAkunData akun) {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", penggunaService.cariAkunContains(akun));
    }

    @GetMapping("/wali-kelas/{kelas}")
    @ResponseBody
    public Response cariBerdasarkanWalas(@PathVariable("kelas") String kelas) {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", penggunaService.cariWalas(kelas));
    }
}
