package id.muhammadiyah.spp.controller.api;

import com.itextpdf.text.DocumentException;
import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.model.request.CariPembayaran;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.util.DateUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping("/laporan")
public class LaporanApiController extends BaseController {

    @PostMapping("/pembayaran")
    @ResponseBody
    public Response cariDaftarPembayaran(@Valid @RequestBody CariPembayaran trans, HttpServletRequest request) throws ParseException {
        return laporanService.cariTransaksi(trans, getUserLogin(request));
    }

    @GetMapping("/pembayaran/cetak/{jenis}/{tanggal}")
    public void cetakTransaksi(@PathVariable("jenis") String jenis, @PathVariable("tanggal") String tanggal, HttpServletRequest request, HttpServletResponse response) throws ParseException, DocumentException, IOException {
        laporanService.cetakTransaksi(new CariPembayaran(DateUtil.strToDate(tanggal, "yyyy-MM-dd"), jenis), getUserLogin(request), response);
    }

    @GetMapping("/tunggakan/{bulan}")
    @ResponseBody
    public Response cariDaftarTunggakan(@PathVariable("bulan") String bulan, HttpServletRequest request) {
        return laporanService.cariTunggakan(bulan, getUserLogin(request));
    }

    @GetMapping("/tunggakan/cetak/{bulan}")
    public void cetakTunggakan(@PathVariable("bulan") String bulan, HttpServletRequest request, HttpServletResponse response) throws DocumentException, IOException {
        laporanService.cetakTunggakan(bulan, getUserLogin(request), response);
    }
}
