package id.muhammadiyah.spp.controller.api;

import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.entity.core.Menu;
import id.muhammadiyah.spp.data.entity.core.Pengguna;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UploadData;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/file")
public class UploadApiController extends BaseController {

    @GetMapping("/unduh")
    public void unduhContohFile(HttpServletResponse response) {
        uploadService.unduhKonsep(response);
    }

    @PostMapping("/unggah")
    @ResponseBody
    public Response unggahFile(@RequestParam("file") MultipartFile file) {
        try {
            if (Objects.requireNonNull(file.getOriginalFilename()).endsWith(".xls")) {
                return uploadService.readXlsFile(file);
            }
            return uploadService.readXlsxFile(file);
        } catch (Exception e) {
            return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(), "File Tidak Valid", null);
        }
    }

    @PostMapping("/unggah/simpan")
    @ResponseBody
    public Response simpanDataUnggah(@Valid @RequestBody List<UploadData> listUpload, HttpServletRequest request) {
        try {
            Menu menu = menuService.cariBerdasarkanNama(listUpload.get(0).getKelas());
            if (menu == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            Pengguna pengguna = penggunaService.getByUsername(this.getUserLogin(request).getUsername());
            if (pengguna == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            if (menu.getJenjang().equals(pengguna.getJabatan().getAksesJenjang()) || pengguna.getJabatan().getAksesJenjang().equals("Semua")) {
                List<String> listNis = new ArrayList<>();
                listUpload.forEach(x -> listNis.add(x.getNis().replace(".0", "")));
                return uploadService.simpanUploadData(listUpload, pengguna.getUsername(), listNis);
            }
            return ResponseUtil.setResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "Anda Tidak Memiliki Akses Untuk Menyimpan Data Ini",
                    null
            );
        } catch (Exception e) {
            return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage(), null);
        }
    }
}
