package id.muhammadiyah.spp.controller.api;

import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.model.request.CariHakAkses;
import id.muhammadiyah.spp.data.model.response.HakAksesMenu;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/jabatan")
public class JabatanApiController extends BaseController {

    @PostMapping("/cari")
    @ResponseBody
    public Response cariAkses(@Valid @RequestBody CariHakAkses cariHakAkses) {
        List<HakAksesMenu> daftarHakAkses = jabatanService.ambilHakAksesList(cariHakAkses.getJabatan(), cariHakAkses.getKelas());
        if (daftarHakAkses.isEmpty()) {
            return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
        }
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", daftarHakAkses);
    }
}
