package id.muhammadiyah.spp.controller.api;

import com.google.gson.Gson;
import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.entity.core.Pengguna;
import id.muhammadiyah.spp.data.model.request.InputPengguna;
import id.muhammadiyah.spp.data.model.request.Login;
import id.muhammadiyah.spp.data.model.response.JabatanInfo;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserAkses;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import id.muhammadiyah.spp.util.EncryptUtil;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.util.StringUtil;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountApiController extends BaseController {

    public InputPengguna inputPenggunaUntukGagalLogin(Pengguna user, String username) {
        Integer gagal = user.getTotalGagal();
        gagal++;
        return new InputPengguna(
                user.getNip(),
                user.getUsername(),
                user.getNama(),
                user.getJenisKelamin(),
                user.getEmail(),
                user.getGelarDepan(),
                user.getGelarBelakang(),
                user.getPendidikanTerakhir(),
                user.getWaliKelas(),
                gagal,
                user.getIdJabatan(),
                user.getDibuatOleh(),
                username,
                user.getTanggalBuat(),
                new Date()
        );
    }

    @PostMapping("/logout")
    @ResponseBody
    public Response doLogout(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        session.removeAttribute(LOGIN);
        if (request.isRequestedSessionIdValid()) {
            session.invalidate();
            clearCookie(request, response);
        }
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "Berhasil Logout", null);
    }

    @PostMapping("/login")
    @ResponseBody
    public Response doLogin(@Valid @RequestBody Login login, HttpServletResponse res, HttpSession sess) throws NoSuchAlgorithmException {
        try {
            Pengguna user = penggunaService.getByUsername(login.getUsername());
            if (user == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            if (user.getTotalGagal() >= 3) {
                return ResponseUtil.setResponse(HttpStatus.FORBIDDEN.value(), "Anda Sudah 3 kali Gagal Login, Mohon Hubungi Administrator Untuk Memperbaikinya", null);
            }
            if (!user.getPassword().equals(EncryptUtil.encrypt(login.getPassword()))) {
                try {
                    resp = ResponseUtil.setResponse(HttpStatus.OK.value(), "-", penggunaService.simpanPengguna(inputPenggunaUntukGagalLogin(user, login.getUsername())));
                } catch (Exception e) {
                    resp = ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
                }
                return ResponseUtil.setResponse(HttpStatus.FORBIDDEN.value(), "Username Atau Password Tidak Sesuai", null);
            }
            JabatanInfo jabatan = new JabatanInfo(user.getIdJabatan(), user.getJabatan().getNama(), user.getJabatan().getAksesJenjang());
            List<UserAkses> userAkses = new ArrayList<>();
            if (jabatan.getIdJabatan() == 7 || jabatan.getIdJabatan() == 8 || jabatan.getIdJabatan() == 9 || jabatan.getIdJabatan() == 10) {
                String kelas = user.getWaliKelas();
                user.getJabatan().getAkses().forEach(x -> {
                    if (x.getMenu().getNama().equals(kelas) || x.getMenu().getNama().equals("Pilih Kelas")) {
                        userAkses.add(new UserAkses(
                                x.getMenu().getIdMenu(),
                                x.getMenu().getParentId(),
                                x.getMenu().getNama(),
                                x.getMenu().getUrl(),
                                x.getMenu().getIkon(),
                                x.getBaca(),
                                x.getTulis()
                        ));
                    }
                });
            } else {
                user.getJabatan().getAkses().forEach(x -> userAkses.add(new UserAkses(
                        x.getMenu().getIdMenu(),
                        x.getMenu().getParentId(),
                        x.getMenu().getNama(),
                        x.getMenu().getUrl(),
                        x.getMenu().getIkon(),
                        x.getBaca(),
                        x.getTulis()
                )));
            }
            UserLogin userLogin = new UserLogin(user.getNip(), user.getUsername(), StringUtil.namaDanGelarPengguna(user), user.getJenisKelamin(), user.getWaliKelas(), jabatan, userAkses);
            sess.setMaxInactiveInterval(-1);
            sess.setAttribute(LOGIN, userLogin);
            Cookie usLog = new Cookie(LOGIN, new Gson().toJson(userLogin));
            usLog.setPath("/");
            usLog.setMaxAge(-1);
            res.addCookie(usLog);
            return ResponseUtil.setResponse(HttpStatus.OK.value(), "", userLogin);
        } catch (Exception e) {
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        }
    }

    @GetMapping("/user-login")
    @ResponseBody
    public Response cariUserLogin(HttpServletRequest request) {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", this.getUserLogin(request));
    }
}
