package id.muhammadiyah.spp.controller.api;

import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.model.request.CariSiswa;
import id.muhammadiyah.spp.data.model.request.InputSiswa;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import id.muhammadiyah.spp.util.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping("/siswa")
public class SiswaApiController extends BaseController {

    @PostMapping("/list")
    @ResponseBody
    public Response cariSiswa(@Valid @RequestBody CariSiswa cariSiswa, HttpServletRequest request) {
        return ResponseUtil.setResponse(
                HttpStatus.OK.value(),
                "",
                siswaService.daftarSiswa(cariSiswa, this.getUserLogin(request).getAkses())
        );
    }

    @GetMapping("/detail/{nis}/{jenjang}")
    @ResponseBody
    public Response cariDetailSiswa(@PathVariable("nis") String nis, @PathVariable("jenjang") String jenjang) {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", siswaService.detailSiswa(nis, jenjang));
    }

    @GetMapping("/tahun-ajaran")
    @ResponseBody
    public Response cariTahunAjaranList() {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", siswaService.cariDaftarTahunAjaran());
    }

    @PostMapping("/simpan")
    @ResponseBody
    public Response simpanDataSiswa(@Valid @RequestBody InputSiswa siswa, HttpServletRequest request) {
        UserLogin login = this.getUserLogin(request);
        siswa.setUserCreate(login.getUsername());
        siswa.setUserUpdate(login.getUsername());
        siswa.setDateCreate(new Date());
        siswa.setDateUpdate(new Date());
        return siswaService.simpanDataSiswa(siswa);
    }

    @PostMapping("/ubah")
    @ResponseBody
    public Response ubahDataSiswa(@Valid @RequestBody InputSiswa siswa, HttpServletRequest request) {
        UserLogin login = this.getUserLogin(request);
        siswa.setUserUpdate(login.getUsername());
        siswa.setDateUpdate(new Date());
        return siswaService.ubahDataSiswa(siswa);
    }

    @PostMapping("/hapus/{nis}/{jenjang}")
    @ResponseBody
    public Response hapusDataSiswa(@PathVariable("nis") String nis, @PathVariable("jenjang") String jenjang, HttpServletRequest request) {
        return siswaService.hapusDataSiswa(nis, jenjang, this.getUserLogin(request).getUsername());
    }
}
