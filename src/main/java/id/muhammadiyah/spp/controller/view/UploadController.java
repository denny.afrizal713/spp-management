package id.muhammadiyah.spp.controller.view;

import id.muhammadiyah.spp.controller.common.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/upload")
public class UploadController extends BaseController {

    @RequestMapping("/tagihan")
    public String uploadTagihan(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "uploadTagihan";
        }
        return toHome();
    }
}
