package id.muhammadiyah.spp.controller.view;

import id.muhammadiyah.spp.controller.common.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/siswa")
public class SiswaController extends BaseController {

    @RequestMapping({"", "/"})
    public String daftarSiswa(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "listSiswa";
        }
        return toHome();
    }

    @RequestMapping("/detail")
    public String detailSiswa(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("nis", request.getParameter("nis"));
            model.addAttribute("kelas", request.getParameter("kelas"));
            return "detailSiswa";
        }
        return toHome();
    }

    @RequestMapping("/tambah")
    public String tambahSiswa(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("tahunAjaran", "");
            return "tambahSiswa";
        }
        return toHome();
    }

    @RequestMapping("/transaksi")
    public String transaksiPembayaran(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("id", request.getParameter("id"));
            return "pembayaran";
        }
        return toHome();
    }

    @RequestMapping("/transaksi/bayar")
    public String pembayaranDetail(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("nis", request.getParameter("nis"));
            model.addAttribute("kelas", request.getParameter("kelas"));
            return "pembayaranDetail";
        }
        return toHome();
    }

    @RequestMapping("/transaksi/penyesuaian")
    public String penyesuaian(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("nis", request.getParameter("nis"));
            model.addAttribute("kelas", request.getParameter("kelas"));
            return "penyesuaian";
        }
        return toHome();
    }
}
