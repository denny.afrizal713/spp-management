package id.muhammadiyah.spp.controller.view;

import id.muhammadiyah.spp.controller.common.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/laporan")
public class LaporanController extends BaseController {

    @RequestMapping("/transaksi")
    public String laporanTransaksi(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "lapTrans";
        }
        return toHome();
    }

    @RequestMapping("/tunggakan")
    public String laporanTunggakan(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "lapTunggak";
        }
        return toHome();
    }
}
