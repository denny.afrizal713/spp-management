package id.muhammadiyah.spp.controller.view;

import id.muhammadiyah.spp.controller.common.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/pengguna")
public class PenggunaController extends BaseController {

    private static final String USER = "";

    @RequestMapping({"", "/"})
    public String daftarPengguna(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "pengguna";
        }
        return toHome();
    }

    @RequestMapping("/tambah")
    public String tambahPengguna(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("scope", "-");
            return "penggunaBaru";
        }
        return toHome();
    }

    @RequestMapping("/detail")
    public String detailPengguna(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("nip", fixNip(request.getParameter("nip")));
            return "penggunaDetail";
        }
        return toHome();
    }

    @RequestMapping("/ubah")
    public String ubahPengguna(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("nip", fixNip(request.getParameter("nip")));
            model.addAttribute("scope", "ubah");
            return "penggunaBaru";
        }
        return toHome();
    }

    @RequestMapping("/setting")
    public String settingPengguna(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            model.addAttribute("nip", fixNip(request.getParameter("nip")));
            return "penggunaSetting";
        }
        return toHome();
    }

    @RequestMapping("/reset-password")
    public String resetPassPengguna(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "penggunaReset";
        }
        return toHome();
    }

    @RequestMapping("/pulihkan-akun")
    public String recoverPengguna(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "penggunaRecover";
        }
        return toHome();
    }

    public String fixNip(String nip) {
        String id = "0000000000" + nip;
        int idx = id.length() - 10;
        return id.substring(idx);
    }
}
