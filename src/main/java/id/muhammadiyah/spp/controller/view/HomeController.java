package id.muhammadiyah.spp.controller.view;

import id.muhammadiyah.spp.controller.common.BaseController;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController extends BaseController implements ErrorController {

    private static String DESC = "deskripsi";
    private static String ERR = "error";
    private static String HDR = "judulnya";

    @RequestMapping({"", "/"})
    public String halamanAwal(HttpServletRequest request, Model model) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            UserLogin userLogin = this.getUserLogin(request);
            model.addAttribute("user", userLogin);
            return "lapTunggakanSemua";
//            return "halamanUtama";
        }
        return "redirect:/akun/login";
    }

    @RequestMapping("/error/forbidden")
    public String forbidden(Model model) {
        model.addAttribute(HDR, "403 - Tidak Diizinkan");
        model.addAttribute(DESC, "Anda Tidak Diizinkan Untuk Mengunjungi Halaman Ini");
        return ERR;
    }

    @RequestMapping("/error/not-found")
    public String notFound(Model model) {
        model.addAttribute(HDR, "404 - Tidak Ditemukan");
        model.addAttribute(DESC, "Halaman Yang Anda Kunjungi Tidak Ditemukan");
        return ERR;
    }

    @RequestMapping("/error/internal-server")
    public String internalServer(Model model) {
        model.addAttribute(HDR, "500 - Terjadi Kesalahan");
        model.addAttribute(DESC, "Terjadi Kesalahan Ketika Memuat Halaman Yang Anda Kunjungi");
        return ERR;
    }
}
