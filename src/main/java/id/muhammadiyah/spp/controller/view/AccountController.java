package id.muhammadiyah.spp.controller.view;

import id.muhammadiyah.spp.controller.common.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/akun")
public class AccountController extends BaseController {

    @RequestMapping("/login")
    public String halamanLogin(HttpServletRequest request) {
        if (Boolean.TRUE.equals(cekLogin(request))) {
            return "redirect:/";
        }
        return "login";
    }
}
