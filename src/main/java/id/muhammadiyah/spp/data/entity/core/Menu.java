package id.muhammadiyah.spp.data.entity.core;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "menu")
public class Menu {

    @Id
    @Column(name = "id_menu")
    private Long idMenu;

    @Column(name = "parent_id")
    private Long parentId;

    @Column(name = "nama")
    private String nama;

    @Column(name = "jenjang")
    private String jenjang;

    @Column(name = "url")
    private String url;

    @Column(name = "ikon")
    private String ikon;
}
