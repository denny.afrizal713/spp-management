package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.BaseEntity;
import id.muhammadiyah.spp.data.entity.embed.DetailSppTlId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "detail_spp_tl")
public class DetailSppTl extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DetailSppTlId id;

    @Column(name = "tahun_ajaran")
    private String tahunAjaran;

    @Column(name = "spp_juli")
    private Double sppJuli;

    @Column(name = "spp_agustus")
    private Double sppAgustus;

    @Column(name = "spp_september")
    private Double sppSeptember;

    @Column(name = "spp_oktober")
    private Double sppOktober;

    @Column(name = "spp_november")
    private Double sppNovember;

    @Column(name = "spp_desember")
    private Double sppDesember;

    @Column(name = "spp_januari")
    private Double sppJanuari;

    @Column(name = "spp_februari")
    private Double sppFebruari;

    @Column(name = "spp_maret")
    private Double sppMaret;

    @Column(name = "spp_april")
    private Double sppApril;

    @Column(name = "spp_mei")
    private Double sppMei;

    @Column(name = "spp_juni")
    private Double sppJuni;
}
