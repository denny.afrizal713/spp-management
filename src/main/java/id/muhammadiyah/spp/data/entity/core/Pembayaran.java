package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.BaseEntity;
import id.muhammadiyah.spp.data.entity.embed.PembayaranId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "pembayaran")
public class Pembayaran extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PembayaranId id;

    @Column(name = "no_bayar")
    private String noBayar;

    @Column(name = "jenis_pembayaran")
    private String jenisPembayaran;

    @Column(name = "bulan")
    private String bulan;

    @Column(name = "tipe_bayar")
    private String tipeBayar;

    @Column(name = "jumlah_bayar")
    private Double jumlahBayar;
}
