package id.muhammadiyah.spp.data.entity.embed;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class SiswaHistoryId implements Serializable {

    private static final long serialVersionUID = 1L;

    public SiswaHistoryId() {
        super();
    }

    public SiswaHistoryId(Long idHistory, String nis) {
        this.idHistory = idHistory;
        this.nis = nis;
    }

    @Column(name = "id_history")
    private Long idHistory;

    @Column(name = "nis")
    private String nis;
}
