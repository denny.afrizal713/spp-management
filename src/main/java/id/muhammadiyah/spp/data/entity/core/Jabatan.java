package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.DescEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "jabatan")
public class Jabatan extends DescEntity {

    @Id
    @Column(name = "id_jabatan")
    private Long idJabatan;

    @Column(name = "akses_jenjang")
    private String aksesJenjang;

    @OneToMany(mappedBy = "id.idJabatan", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @OrderBy("id.idMenu ASC")
    private List<Akses> akses;
}
