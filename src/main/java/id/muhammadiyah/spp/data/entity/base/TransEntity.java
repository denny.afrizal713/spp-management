package id.muhammadiyah.spp.data.entity.base;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class TransEntity extends BaseEntity {

    @Column(name = "nis")
    protected String nis;
}
