package id.muhammadiyah.spp.data.entity.embed;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class DetailSppTlId implements Serializable {

    private static final long serialVersionUID = 1L;

    public DetailSppTlId() {
        super();
    }

    public DetailSppTlId(Long idSppTl, String nis) {
        this.idSppTl = idSppTl;
        this.nis = nis;
    }

    @Column(name = "id_spp_tl")
    private Long idSppTl;

    @Column(name = "nis")
    private String nis;
}
