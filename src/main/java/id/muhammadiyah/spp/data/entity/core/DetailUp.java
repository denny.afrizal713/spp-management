package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.TransEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "detail_up")
public class DetailUp extends TransEntity {

    @Id
    @Column(name = "id_up")
    private Long idUp;

    @Column(name = "jumlah_up")
    private Double jumlahUp;
}
