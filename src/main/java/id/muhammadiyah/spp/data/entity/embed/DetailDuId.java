package id.muhammadiyah.spp.data.entity.embed;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class DetailDuId implements Serializable {

    private static final long serialVersionUID = 1L;

    public DetailDuId() {
        super();
    }

    public DetailDuId(Long idDu, String nis) {
        this.idDu = idDu;
        this.nis = nis;
    }

    @Column(name = "id_du")
    private Long idDu;

    @Column(name = "nis")
    private String nis;
}
