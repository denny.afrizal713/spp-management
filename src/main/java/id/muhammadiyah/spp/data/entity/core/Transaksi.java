package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.TransEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "transaksi")
public class Transaksi extends TransEntity {

    @Id
    @Column(name = "id_transaksi")
    private Long idTransaksi;

    @Column(name = "spp_tb_total")
    private Double sppTbTotal;

    @Column(name = "spp_tb_bayar")
    private Double sppTbBayar;

    @Column(name = "spp_tb_sisa")
    private Double sppTbSisa;

    @Column(name = "spp_tb_lebih")
    private Double sppTbLebih;

    @Column(name = "spp_tl_total")
    private Double sppTlTotal;

    @Column(name = "spp_tl_bayar")
    private Double sppTlBayar;

    @Column(name = "spp_tl_sisa")
    private Double sppTlSisa;

    @Column(name = "spp_tl_lebih")
    private Double sppTlLebih;

    @Column(name = "up_total")
    private Double upTotal;

    @Column(name = "up_bayar")
    private Double upBayar;

    @Column(name = "up_sisa")
    private Double upSisa;

    @Column(name = "up_lebih")
    private Double upLebih;

    @Column(name = "du_total")
    private Double duTotal;

    @Column(name = "du_bayar")
    private Double duBayar;

    @Column(name = "du_sisa")
    private Double duSisa;

    @Column(name = "du_lebih")
    private Double duLebih;
}
