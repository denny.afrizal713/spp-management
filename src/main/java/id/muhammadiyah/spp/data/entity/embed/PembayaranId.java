package id.muhammadiyah.spp.data.entity.embed;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class PembayaranId implements Serializable {

    private static final long serialVersionUID = 1L;

    public PembayaranId() {
        super();
    }

    public PembayaranId(Long idBayar, String nis) {
        this.idBayar = idBayar;
        this.nis = nis;
    }

    @Column(name = "id_bayar")
    private Long idBayar;

    @Column(name = "nis")
    private String nis;
}
