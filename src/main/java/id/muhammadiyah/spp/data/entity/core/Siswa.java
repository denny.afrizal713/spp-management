package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.DescEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "siswa")
public class Siswa extends DescEntity {

    @Id
    @Column(name = "id_siswa")
    private Long idSiswa;

    @Column(name = "nis")
    private String nis;

    @Column(name = "jenis_kelamin")
    private String jenisKelamin;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "tanggal_lahir")
    private Date tanggalLahir;

    @Column(name = "tahun_masuk")
    private String tahunMasuk;

    @Column(name = "tahun_ajaran")
    private String tahunAjaran;

    @Column(name = "kelas")
    private String kelas;

    @Column(name = "email")
    private String email;

    @Column(name = "jenjang")
    private String jenjang;

    @Column(name = "id_trans")
    private Long idTrans;

    @Column(name = "id_up")
    private Long idUp;

    @Column(name = "id_spp")
    private Long idSpp;

    @OneToMany(mappedBy = "id.nis", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    private List<SiswaHistory> historySiswa;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_trans", insertable = false, updatable = false, referencedColumnName = "id_transaksi")
    private Transaksi transaksi;

    @OneToMany(mappedBy = "id.nis", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Pembayaran> pembayaran;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_up", insertable = false, updatable = false, referencedColumnName = "id_up")
    private DetailUp uangPangkal;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_spp", insertable = false, updatable = false, referencedColumnName = "id_spp_tb")
    private DetailSppTb sppTahunBerjalan;

    @OneToMany(mappedBy = "id.nis", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    private List<DetailSppTl> sppTahunLalu;

    @OneToMany(mappedBy = "id.nis", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @OrderBy("id.idDu DESC")
    private List<DetailDu> daftarUlang;
}
