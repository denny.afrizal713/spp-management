package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.BaseEntity;
import id.muhammadiyah.spp.data.entity.embed.SiswaHistoryId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "siswa_history")
public class SiswaHistory extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private SiswaHistoryId id;

    @Column(name = "tahun_ajaran")
    private String tahunAjaran;

    @Column(name = "kelas")
    private String kelas;
}
