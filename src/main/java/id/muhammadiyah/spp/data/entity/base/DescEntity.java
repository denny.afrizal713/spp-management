package id.muhammadiyah.spp.data.entity.base;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public abstract class DescEntity extends BaseEntity {

    @Column(name = "nama")
    protected String nama;

    @Column(name = "status")
    protected String status;
}
