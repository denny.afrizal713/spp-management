package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.BaseEntity;
import id.muhammadiyah.spp.data.entity.embed.DetailDuId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "detail_du")
public class DetailDu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DetailDuId id;

    @Column(name = "jumlah_du")
    private Double jumlahDu;

    @Column(name = "tahun_ajaran")
    private String tahunAjaran;
}
