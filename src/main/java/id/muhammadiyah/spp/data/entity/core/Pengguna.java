package id.muhammadiyah.spp.data.entity.core;

import id.muhammadiyah.spp.data.entity.base.DescEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "pengguna")
public class Pengguna extends DescEntity {

    @Id
    @Column(name = "nip")
    private String nip;

    @Column(name = "jenis_kelamin")
    private String jenisKelamin;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "total_gagal")
    private Integer totalGagal;

    @Column(name = "gelar_depan")
    private String gelarDepan;

    @Column(name = "gelar_belakang")
    private String gelarBelakang;

    @Column(name = "pendidikan_terakhir")
    private String pendidikanTerakhir;

    @Column(name = "wali_kelas")
    private String waliKelas;

    @Column(name = "id_jabatan")
    private Long idJabatan;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_jabatan", insertable = false, updatable = false, referencedColumnName = "id_jabatan")
    private Jabatan jabatan;
}
