package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.Jabatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface JabatanRepository extends JpaRepository<Jabatan, Long>, JpaSpecificationExecutor<Jabatan> {

    public Jabatan findFirstByNamaContainsOrderByIdJabatanDesc(String nama);
}
