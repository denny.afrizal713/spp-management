package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.Pengguna;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PenggunaRepository extends JpaRepository<Pengguna, String>, JpaSpecificationExecutor<Pengguna> {

    Pengguna findFirstByOrderByNipDesc();

    Pengguna findFirstByWaliKelasOrderByTanggalBuatDesc(String kelas);
}
