package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long>, JpaSpecificationExecutor<Menu> {

    public List<Menu> findByParentId(Long parentId);

    public List<Menu> findByParentIdAndJenjang(Long parentId, String jenjang);

    public Menu findFirstByNama(String nama);

    public Menu findByIdMenu(Long id);
}
