package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.Siswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SiswaRepository extends JpaRepository<Siswa, String>, JpaSpecificationExecutor<Siswa> {

    public Siswa findFirstByOrderByIdSiswaDesc();

    public Siswa findFirstByJenjangOrderByNisDesc(String jenjang);
}
