package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.DetailSppTb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailSppTbRepository extends JpaRepository<DetailSppTb, Long>, JpaSpecificationExecutor<DetailSppTb> {

    public DetailSppTb findFirstByOrderByIdSppTbDesc();
}
