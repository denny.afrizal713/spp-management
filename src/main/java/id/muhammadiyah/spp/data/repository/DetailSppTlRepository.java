package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.DetailSppTl;
import id.muhammadiyah.spp.data.entity.embed.DetailSppTlId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailSppTlRepository extends JpaRepository<DetailSppTl, DetailSppTlId>, JpaSpecificationExecutor<DetailSppTl> {

    public DetailSppTl findFirstByIdNisOrderByIdIdSppTlDesc(String nis);
}
