package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.Pembayaran;
import id.muhammadiyah.spp.data.entity.embed.PembayaranId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PembayaranRepository extends JpaRepository<Pembayaran, PembayaranId>, JpaSpecificationExecutor<Pembayaran> {

    public Pembayaran findFirstByIdNisOrderByIdIdBayarDesc(String nis);

    @Query(value = "select a.nis,a.nama,a.kelas,b.jenis_pembayaran,b.bulan,b.jumlah_bayar,b.tanggal_buat, " +
            "from siswa a join pembayaran b on a.nis = b.nis " +
            "where b.tanggal_buat between :tgl1 and :tgl2 " +
            "order by b.tanggal_buat asc", nativeQuery = true)
    public List<Object[]> filterByTanggal(
            @Param("tgl1") Date tgl1,
            @Param("tgl2") Date tgl2
    );

    @Query(value = "select a.nis,a.nama,a.kelas,b.jenis_pembayaran,b.bulan,b.jumlah_bayar,b.tanggal_buat " +
            "from siswa a join pembayaran b on a.nis = b.nis " +
            "where a.kelas in (:kelas) " +
            "and b.tanggal_buat between :tgl1 and :tgl2 " +
            "order by b.tanggal_buat asc", nativeQuery = true)
    public List<Object[]> filterByKelasAndTanggal(
            @Param("kelas") List<String> kelas,
            @Param("tgl1") Date tgl1,
            @Param("tgl2") Date tgl2
    );

    @Query(value = "select a.nis,a.nama,a.kelas,b.jenis_pembayaran,b.bulan,b.jumlah_bayar,b.tanggal_buat " +
            "from siswa a join pembayaran b on a.nis = b.nis " +
            "where b.jenis_pembayaran = :jenis " +
            "and b.tanggal_buat between :tgl1 and :tgl2 " +
            "order by b.tanggal_buat asc", nativeQuery = true)
    public List<Object[]> filterByTanggalAndJenis(
            @Param("tgl1") Date tgl1,
            @Param("tgl2") Date tgl2,
            @Param("jenis") String jenis
    );

    @Query(value = "select a.nis,a.nama,a.kelas,b.jenis_pembayaran,b.bulan,b.jumlah_bayar,b.tanggal_buat " +
            "from siswa a join pembayaran b on a.nis = b.nis " +
            "where a.kelas in (:kelas) " +
            "and b.jenis_pembayaran = :jenis " +
            "and b.tanggal_buat between :tgl1 and :tgl2 " +
            "order by b.tanggal_buat asc", nativeQuery = true)
    public List<Object[]> filterByKelasAndTanggalAndJenis(
            @Param("kelas") List<String> kelas,
            @Param("tgl1") Date tgl1,
            @Param("tgl2") Date tgl2,
            @Param("jenis") String jenis
    );
}
