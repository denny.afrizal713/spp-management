package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.DetailDu;
import id.muhammadiyah.spp.data.entity.embed.DetailDuId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailDuRepository extends JpaRepository<DetailDu, DetailDuId>, JpaSpecificationExecutor<DetailDu> {

    public DetailDu findFirstByIdNisOrderByIdIdDuDesc(String nis);
}
