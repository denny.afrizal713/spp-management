package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TransaksiRepository extends JpaRepository<Transaksi, Long>, JpaSpecificationExecutor<Transaksi> {

    public Transaksi findFirstByOrderByIdTransaksiDesc();
}
