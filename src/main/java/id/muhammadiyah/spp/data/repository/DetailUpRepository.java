package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.DetailUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailUpRepository extends JpaRepository<DetailUp, Long>, JpaSpecificationExecutor<DetailUp> {

    public DetailUp findFirstByOrderByIdUpDesc();
}
