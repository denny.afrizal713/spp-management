package id.muhammadiyah.spp.data.repository;

import id.muhammadiyah.spp.data.entity.core.SiswaHistory;
import id.muhammadiyah.spp.data.entity.embed.SiswaHistoryId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SiswaHistoryRepository extends JpaRepository<SiswaHistory, SiswaHistoryId>, JpaSpecificationExecutor<SiswaHistory> {

    public SiswaHistory findFirstByIdNisOrderByIdIdHistoryDesc(String nis);
}
