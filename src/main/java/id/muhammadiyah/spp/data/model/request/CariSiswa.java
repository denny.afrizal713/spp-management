package id.muhammadiyah.spp.data.model.request;

import id.muhammadiyah.spp.data.model.common.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CariSiswa extends Page {

    public CariSiswa() {
        super();
    }

    public CariSiswa(String nis, String nama, String kelas, String tahunAjaran) {
        this.nis = nis;
        this.nama = nama;
        this.kelas = kelas;
        this.tahunAjaran = tahunAjaran;
    }

    private String nis;
    private String nama;
    private String kelas;
    private String tahunAjaran;
}
