package id.muhammadiyah.spp.data.model.request;

import lombok.Data;

@Data
public class Login {

    public Login() {
        super();
    }

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    private String username;
    private String password;
}
