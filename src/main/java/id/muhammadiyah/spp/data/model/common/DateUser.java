package id.muhammadiyah.spp.data.model.common;

import lombok.Data;

import java.util.Date;

@Data
public abstract class DateUser {

    protected String userCreate;
    protected String userUpdate;
    protected Date dateCreate;
    protected Date dateUpdate;
}
