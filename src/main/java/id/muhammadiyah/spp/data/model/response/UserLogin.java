package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

import java.util.List;

@Data
public class UserLogin {

    public UserLogin() {
        super();
    }

    public UserLogin(String nip, String username, String nama, String jenisKelamin, String waliKelas, JabatanInfo jabatan, List<UserAkses> akses) {
        this.nip = nip;
        this.username = username;
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.waliKelas = waliKelas;
        this.jabatan = jabatan;
        this.akses = akses;
    }

    private String nip;
    private String username;
    private String nama;
    private String jenisKelamin;
    private String waliKelas;
    private JabatanInfo jabatan;
    private List<UserAkses> akses;
}
