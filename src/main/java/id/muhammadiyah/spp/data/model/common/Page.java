package id.muhammadiyah.spp.data.model.common;

import lombok.Data;

@Data
public abstract class Page {

    protected Integer page;
    protected Integer size;
}
