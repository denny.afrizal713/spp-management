package id.muhammadiyah.spp.data.model.request;

import id.muhammadiyah.spp.data.model.common.DateUser;
import lombok.Data;

@Data
public class InputSiswa extends DateUser {

    public InputSiswa() {
        super();
    }

    public InputSiswa(String nis,
                      String nama,
                      String jenisKelamin,
                      String kelas,
                      String email,
                      String tahunAjaran,
                      Double uangPangkal,
                      Double daftarUlang,
                      Double sppTl,
                      Double juli,
                      Double agustus,
                      Double september,
                      Double oktober,
                      Double november,
                      Double desember,
                      Double januari,
                      Double februari,
                      Double maret,
                      Double april,
                      Double mei,
                      Double juni,
                      Double total) {
        this.nis = nis;
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.kelas = kelas;
        this.email = email;
        this.tahunAjaran = tahunAjaran;
        this.uangPangkal = uangPangkal;
        this.daftarUlang = daftarUlang;
        this.sppTl = sppTl;
        this.juli = juli;
        this.agustus = agustus;
        this.september = september;
        this.oktober = oktober;
        this.november = november;
        this.desember = desember;
        this.januari = januari;
        this.februari = februari;
        this.maret = maret;
        this.april = april;
        this.mei = mei;
        this.juni = juni;
        this.total = total;
    }

    private String nis;
    private String nama;
    private String jenisKelamin;
    private String kelas;
    private String email;
    private String tahunAjaran;
    private Double uangPangkal;
    private Double daftarUlang;
    private Double sppTl;
    private Double juli;
    private Double agustus;
    private Double september;
    private Double oktober;
    private Double november;
    private Double desember;
    private Double januari;
    private Double februari;
    private Double maret;
    private Double april;
    private Double mei;
    private Double juni;
    private Double total;
}
