package id.muhammadiyah.spp.data.model.request;

import lombok.Data;

@Data
public class DetailAkunRequest {

    public DetailAkunRequest() {
        super();
    }

    public DetailAkunRequest(String nip, String username, String password, String oldPassword) {
        this.nip = nip;
        this.username = username;
        this.password = password;
        this.oldPassword = oldPassword;
    }

    private String nip;
    private String username;
    private String password;
    private String oldPassword;
}
