package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class DaftarPengguna {

    public DaftarPengguna() {
        super();
    }

    public DaftarPengguna(String nip,
                          String username,
                          String nama,
                          String jenisKelamin,
                          String pendidikanTerakhir,
                          String jabatan,
                          String waliKelas,
                          String aksesJenjang) {
        this.nip = nip;
        this.username = username;
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.pendidikanTerakhir = pendidikanTerakhir;
        this.jabatan = jabatan;
        this.waliKelas = waliKelas;
        this.aksesJenjang = aksesJenjang;
    }

    private String nip;
    private String username;
    private String nama;
    private String jenisKelamin;
    private String pendidikanTerakhir;
    private String jabatan;
    private String waliKelas;
    private String aksesJenjang;
}
