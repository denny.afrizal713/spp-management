package id.muhammadiyah.spp.data.model.request;

import id.muhammadiyah.spp.data.model.common.DateUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class InputPengguna extends DateUser {

    public InputPengguna() {
        super();
    }

    public InputPengguna(
            String nip,
            String username,
            String nama,
            String jenisKelamin,
            String email,
            String gelarDepan,
            String gelarBelakang,
            String pendidikanTerakhir,
            String waliKelas,
            Integer totalGagal,
            Long idJabatan,
            String userCreate,
            String userUpdate,
            Date dateCreate,
            Date dateUpdate
    ) {
        this.nip = nip;
        this.username = username;
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.email = email;
        this.gelarDepan = gelarDepan;
        this.gelarBelakang = gelarBelakang;
        this.pendidikanTerakhir = pendidikanTerakhir;
        this.waliKelas = waliKelas;
        this.totalGagal = totalGagal;
        this.idJabatan = idJabatan;
        this.userCreate = userCreate;
        this.userUpdate = userUpdate;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
    }

    private String nip;
    private String username;
    private String nama;
    private String jenisKelamin;
    private String email;
    private String gelarDepan;
    private String gelarBelakang;
    private String pendidikanTerakhir;
    private String waliKelas;
    private Integer totalGagal;
    private Long idJabatan;
}
