package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class HakAksesMenu {

    public HakAksesMenu() {
        super();
    }

    public HakAksesMenu(Integer no, String menu, String lihat, String tambah, String ubah, String hapus) {
        this.no = no;
        this.menu = menu;
        this.lihat = lihat;
        this.tambah = tambah;
        this.ubah = ubah;
        this.hapus = hapus;
    }

    private Integer no;
    private String menu;
    private String lihat;
    private String tambah;
    private String ubah;
    private String hapus;
}
