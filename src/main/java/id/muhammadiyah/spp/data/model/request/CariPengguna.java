package id.muhammadiyah.spp.data.model.request;

import id.muhammadiyah.spp.data.model.common.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CariPengguna extends Page {

    public CariPengguna() {
        super();
    }

    public CariPengguna(String nip,
                        String username,
                        String nama,
                        String gelarDepan,
                        String gelarBelakang,
                        String pendidikanTerakhir,
                        String jenisKelamin,
                        String waliKelas,
                        String jabatan,
                        String aksesJenjang) {
        this.nip = nip;
        this.username = username;
        this.nama = nama;
        this.gelarDepan = gelarDepan;
        this.gelarDepan = gelarBelakang;
        this.pendidikanTerakhir = pendidikanTerakhir;
        this.jenisKelamin = jenisKelamin;
        this.waliKelas = waliKelas;
        this.jabatan = jabatan;
        this.aksesJenjang = aksesJenjang;
    }

    private String nip;
    private String username;
    private String nama;
    private String gelarDepan;
    private String gelarBelakang;
    private String pendidikanTerakhir;
    private String jenisKelamin;
    private String waliKelas;
    private String jabatan;
    private String aksesJenjang;
}
