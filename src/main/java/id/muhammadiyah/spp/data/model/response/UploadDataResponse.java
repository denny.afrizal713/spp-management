package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

import java.util.List;

@Data
public class UploadDataResponse {

    public UploadDataResponse() {
        super();
    }

    public UploadDataResponse(List<UploadData> listData, List<GagalUploadData> listGagal) {
        this.listData = listData;
        this.listGagal = listGagal;
    }

    private List<UploadData> listData;
    private List<GagalUploadData> listGagal;
}
