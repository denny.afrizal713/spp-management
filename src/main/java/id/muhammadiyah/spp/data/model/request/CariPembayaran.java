package id.muhammadiyah.spp.data.model.request;

import lombok.Data;

import java.util.Date;

@Data
public class CariPembayaran {

    public CariPembayaran() {
        super();
    }

    public CariPembayaran(Date tanggal, String jenis) {
        this.tanggal = tanggal;
        this.jenis = jenis;
    }

    Date tanggal;
    String jenis;
}
