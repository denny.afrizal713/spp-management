package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class DetailPengguna {

    public DetailPengguna() {
        super();
    }

    public DetailPengguna(Long id, String username, String nama, String password, String email, String phone, String gender) {
        this.id = id;
        this.username = username;
        this.nama = nama;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
    }

    private Long id;
    private String username;
    private String nama;
    private String password;
    private String email;
    private String phone;
    private String gender;
}
