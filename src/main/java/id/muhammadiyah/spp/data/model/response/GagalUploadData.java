package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class GagalUploadData {

    public GagalUploadData() {
        super();
    }

    public GagalUploadData(Integer no, String keterangan) {
        this.no = no;
        this.keterangan = keterangan;
    }

    private Integer no;
    private String keterangan;
}
