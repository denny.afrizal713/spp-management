package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class DaftarTunggakan {

    public DaftarTunggakan() {
        super();
    }

    public DaftarTunggakan(Integer no, String keterangan, Double spp, Double uangPangkal, Double total, String walas) {
        this.no = no;
        this.keterangan = keterangan;
        this.spp = spp;
        this.uangPangkal = uangPangkal;
        this.total = total;
        this.walas = walas;
    }

    private Integer no;
    private String keterangan;
    private Double spp;
    private Double uangPangkal;
    private Double total;
    private String walas;
}
