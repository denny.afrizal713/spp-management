package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class DetailDaftarSiswa {

    public DetailDaftarSiswa() {
        super();
    }

    public DetailDaftarSiswa(
            Integer no,
            String nis,
            String nama,
            String kelas,
            String tahunAjaran,
            Double uangPangkal,
            Double sppTl,
            Double daftarUlang,
            Double juli,
            Double agustus,
            Double september,
            Double oktober,
            Double november,
            Double desember,
            Double januari,
            Double februari,
            Double maret,
            Double april,
            Double mei,
            Double juni,
            Double jumlah
    ) {
        this.no = no;
        this.nis = nis;
        this.nama = nama;
        this.kelas = kelas;
        this.tahunAjaran = tahunAjaran;
        this.uangPangkal = uangPangkal;
        this.sppTl = sppTl;
        this.daftarUlang = daftarUlang;
        this.juli = juli;
        this.agustus = agustus;
        this.september = september;
        this.oktober = oktober;
        this.november = november;
        this.desember = desember;
        this.januari = januari;
        this.februari = februari;
        this.maret = maret;
        this.april = april;
        this.mei = mei;
        this.juni = juni;
        this.jumlah = jumlah;
    }

    private Integer no;
    private String nis;
    private String nama;
    private String kelas;
    private String tahunAjaran;
    private Double uangPangkal;
    private Double sppTl;
    private Double daftarUlang;
    private Double juli;
    private Double agustus;
    private Double september;
    private Double oktober;
    private Double november;
    private Double desember;
    private Double januari;
    private Double februari;
    private Double maret;
    private Double april;
    private Double mei;
    private Double juni;
    private Double jumlah;
}
