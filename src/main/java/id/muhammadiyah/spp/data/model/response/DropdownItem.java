package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class DropdownItem {

    public DropdownItem() {
        super();
    }

    public DropdownItem(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    private Integer key;
    private String value;
}
