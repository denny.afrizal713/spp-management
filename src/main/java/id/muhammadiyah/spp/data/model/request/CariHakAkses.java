package id.muhammadiyah.spp.data.model.request;

import lombok.Data;

@Data
public class CariHakAkses {

    public CariHakAkses() {
        super();
    }

    public CariHakAkses(String jabatan, String kelas) {
        this.jabatan = jabatan;
        this.kelas = kelas;
    }

    private String jabatan;
    private String kelas;
}
