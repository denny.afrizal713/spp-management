package id.muhammadiyah.spp.data.model.request;

import id.muhammadiyah.spp.data.model.common.DateUser;
import lombok.Data;

@Data
public class Penyesuaian extends DateUser {

    public Penyesuaian() {
        super();
    }

    public Penyesuaian(String nis, String jenjang, String dari, String ke, Double bayar) {
        this.nis = nis;
        this.jenjang = jenjang;
        this.dari = dari;
        this.ke = ke;
        this.bayar = bayar;
    }

    private String nis;
    private String jenjang;
    private String dari;
    private String ke;
    private Double bayar;
}
