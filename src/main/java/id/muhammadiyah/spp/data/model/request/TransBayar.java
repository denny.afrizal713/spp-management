package id.muhammadiyah.spp.data.model.request;

import id.muhammadiyah.spp.data.model.common.DateUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TransBayar extends DateUser {

    public TransBayar() {
        super();
    }

    public TransBayar(String nis, String jenjang, String bulan, Double jumlahBayar) {
        this.nis = nis;
        this.jenjang = jenjang;
        this.bulan = bulan;
        this.jumlahBayar = jumlahBayar;
    }

    private String nis;
    private String jenjang;
    private String bulan;
    private Double jumlahBayar;
}
