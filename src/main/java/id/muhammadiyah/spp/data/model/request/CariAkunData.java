package id.muhammadiyah.spp.data.model.request;

import lombok.Data;

@Data
public class CariAkunData {

    public CariAkunData() {
        super();
    }

    public CariAkunData(String nip, String username, String nama) {
        this.nip = nip;
        this.username = username;
        this.nama = nama;
    }

    private String nip;
    private String username;
    private String nama;
}
