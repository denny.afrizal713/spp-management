package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

import java.util.Date;

@Data
public class DaftarTransaksi {

    public DaftarTransaksi() {
        super();
    }

    public DaftarTransaksi(Integer no, String nis, String nama, String kelas, String jenis, Double nominal, String keterangan, Date tanggal) {
        this.no = no;
        this.nis = nis;
        this.nama = nama;
        this.kelas = kelas;
        this.jenis = jenis;
        this.nominal = nominal;
        this.keterangan = keterangan;
        this.tanggal = tanggal;
    }

    private Integer no;
    private String nis;
    private String nama;
    private String kelas;
    private String jenis;
    private Double nominal;
    private String keterangan;
    private Date tanggal;
}
