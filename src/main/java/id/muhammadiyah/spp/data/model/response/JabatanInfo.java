package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class JabatanInfo {

    public JabatanInfo() {
        super();
    }

    public JabatanInfo(Long idJabatan, String nama, String aksesJenjang) {
        this.idJabatan = idJabatan;
        this.nama = nama;
        this.aksesJenjang = aksesJenjang;
    }

    private Long idJabatan;
    private String nama;
    private String aksesJenjang;
}
