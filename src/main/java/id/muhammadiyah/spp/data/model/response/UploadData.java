package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class UploadData {

    public UploadData() {
        super();
    }

    public UploadData(
            Integer no,
            String nis,
            String nama,
            String jenisKelamin,
            String kelas,
            String uangPangkal,
            String daftarUlang,
            String sppTl,
            String juli,
            String agustus,
            String september,
            String oktober,
            String november,
            String desember,
            String januari,
            String februari,
            String maret,
            String april,
            String mei,
            String juni,
            String jumlah
    ) {
        this.no = no;
        this.nis = nis;
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.kelas = kelas;
        this.uangPangkal = uangPangkal;
        this.daftarUlang = daftarUlang;
        this.sppTl = sppTl;
        this.juli = juli;
        this.agustus = agustus;
        this.september = september;
        this.oktober = oktober;
        this.november = november;
        this.desember = desember;
        this.januari = januari;
        this.februari = februari;
        this.maret = maret;
        this.april = april;
        this.mei = mei;
        this.juni = juni;
        this.jumlah = jumlah;
    }

    private Integer no;
    private String nis;
    private String nama;
    private String jenisKelamin;
    private String kelas;
    private String uangPangkal;
    private String daftarUlang;
    private String sppTl;
    private String juli;
    private String agustus;
    private String september;
    private String oktober;
    private String november;
    private String desember;
    private String januari;
    private String februari;
    private String maret;
    private String april;
    private String mei;
    private String juni;
    private String jumlah;
}
