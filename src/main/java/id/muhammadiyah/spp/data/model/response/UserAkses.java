package id.muhammadiyah.spp.data.model.response;

import lombok.Data;

@Data
public class UserAkses {

    public UserAkses() {
        super();
    }

    public UserAkses(Long menuId, Long parent, String nama, String url, String ikon, Integer baca, Integer tulis) {
        this.menuId = menuId;
        this.parent = parent;
        this.nama = nama;
        this.url = url;
        this.ikon = ikon;
        this.baca = baca;
        this.tulis = tulis;
    }

    private Long menuId;
    private Long parent;
    private String nama;
    private String url;
    private String ikon;
    private Integer baca;
    private Integer tulis;
}
