package id.muhammadiyah.spp.variable.enumeration;

public enum JenisPembayaranEnum {

    UP("Uang Pangkal"),
    DU("Daftar Ulang"),
    TL("SPP Tahun Lalu"),
    TB("SPP Tahun Berjalan");
    String label;

    JenisPembayaranEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
