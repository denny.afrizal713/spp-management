package id.muhammadiyah.spp.variable.enumeration;

public enum BulanEnum {

    JAN("Januari"),
    FEB("Februari"),
    MAR("Maret"),
    APR("April"),
    MAY("Mei"),
    JUN("Juni"),
    JUL("Juli"),
    AUG("Agustus"),
    SEP("September"),
    OCT("Oktober"),
    NOV("November"),
    DEC("Desember");
    String label;

    BulanEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
