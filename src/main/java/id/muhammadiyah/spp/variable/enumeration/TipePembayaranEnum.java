package id.muhammadiyah.spp.variable.enumeration;

public enum TipePembayaranEnum {

    NORMAL("Normal"),
    ADJUST("Penyesuaian");
    String label;

    TipePembayaranEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
