package id.muhammadiyah.spp.variable.enumeration;

public enum StatusEnum {

    AKTIF("Aktif"),
    NON("Non Aktif"),
    DEL("Hapus");
    String label;

    StatusEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
