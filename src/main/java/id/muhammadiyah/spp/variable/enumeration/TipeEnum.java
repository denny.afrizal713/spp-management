package id.muhammadiyah.spp.variable.enumeration;

public enum TipeEnum {

    SISWA("Siswa"),
    TRANS("Transaksi"),
    SPPTL("SPP Tahun Lalu"),
    SPPTB("SPP Tahun Berjalan"),
    UP("Uang Pangkal"),
    DU("Daftar Ulang"),
    PAY("Pembayaran"),
    HISTORY("Siswa History");
    String label;

    TipeEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
