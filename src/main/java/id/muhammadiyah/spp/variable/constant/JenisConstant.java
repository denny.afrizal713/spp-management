package id.muhammadiyah.spp.variable.constant;

public class JenisConstant {

    private JenisConstant() {
        super();
    }

    public static final String UP = "Uang Pangkal";
    public static final String DU = "Daftar Ulang";
    public static final String TB = "SPP Tahun Berjalan";
    public static final String TL = "SPP Tahun Lalu";
}
