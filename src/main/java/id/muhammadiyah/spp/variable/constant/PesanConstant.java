package id.muhammadiyah.spp.variable.constant;

public class PesanConstant {

    private PesanConstant() {
        super();
    }

    public static final String TIDAK_DITEMUKAN = "Data Tidak Ditemukan";
    public static final String BERHASIL_SIMPAN = "Berhasil Simpan Data";
    public static final String GAGAL_SIMPAN = "Gagal Simpan Data";
    public static final String BERHASIL_UBAH = "Berhasil Ubah Data";
    public static final String GAGAL_UBAH = "Gagal Ubah Data";
    public static final String BERHASIL_HAPUS = "Berhasil Hapus Data";
    public static final String GAGAL_HAPUS = "Gagal Hapus Data";
}
