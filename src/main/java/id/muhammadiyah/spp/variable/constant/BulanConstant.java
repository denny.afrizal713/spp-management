package id.muhammadiyah.spp.variable.constant;

public class BulanConstant {

    private BulanConstant() {
        super();
    }

    public static final String JUL = "Juli";
    public static final String AUG = "Agustus";
    public static final String SEP = "September";
    public static final String OCT = "Oktober";
    public static final String NOV = "November";
    public static final String DEC = "Desember";
    public static final String JAN = "Januari";
    public static final String FEB = "Februari";
    public static final String MAR = "Maret";
    public static final String APR = "April";
    public static final String MAY = "Mei";
    public static final String JUN = "Juni";
}
