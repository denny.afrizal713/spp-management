package id.muhammadiyah.spp.util;

public class NumberUtil {

    private NumberUtil() {
        super();
    }

    public static String randomPassword() {
        String randomChars = "abcdefghijklmnopqrstuvwxyz1234567890";
        int len = 8;
        StringBuilder buildRandom = new StringBuilder(len);
        for (int x = 0; x < len; x++) {
            int idx = (int) (randomChars.length() * Math.random());
            buildRandom.append(randomChars.charAt(idx));
        }
        return buildRandom.toString();
    }

    public static String formatUang(Double amount) {
        return String.format("%,.2f", amount);
    }
}
