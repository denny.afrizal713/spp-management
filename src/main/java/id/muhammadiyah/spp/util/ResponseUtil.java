package id.muhammadiyah.spp.util;

import id.muhammadiyah.spp.data.model.response.Response;
import org.springframework.http.HttpStatus;

public class ResponseUtil {

    private ResponseUtil() {
        super();
    }

    public static <T> Response setResponse(int kode, String pesan, T data) {
        return new Response(kode, buildMessage(kode, pesan), data);
    }

    public static String buildMessage(int code, String msg) {
        return HttpStatus.valueOf(code).getReasonPhrase() + " : " + msg;
    }
}
