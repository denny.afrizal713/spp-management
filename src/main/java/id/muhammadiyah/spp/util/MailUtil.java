package id.muhammadiyah.spp.util;

import id.muhammadiyah.spp.data.model.response.DetailPengguna;

import java.util.Date;

public class MailUtil {

    public static String akunSubject(String nama) {
        return "Pembuatan Akun " + nama;
    }

    public static String akunBody(DetailPengguna vo) {
        return "Assalamu'alaikum Wr Wb, " + vo.getNama() + "\n\n"
                + "Terlampir rincian detail akun yang sudah dibuat sebagai berikut:\n\n"
                + "\tUsername    : " + vo.getUsername() + "\n"
                + "\tNama        : " + vo.getNama() + "\n"
                + "\tPassword    : " + vo.getPassword() + "\n"
                + "\tEmail       : " + vo.getEmail() + "\n"
                + "\tNo. Telepon : " + vo.getPhone() + "\n\n"
                + "Terima kasih\n\n"
                + "Best regards,\n"
                + "Muhammadiyah Team";
    }


    public static String changePass(String type, String nama) {
        return type + " Password " + nama;
    }

    public static String resetBody(DetailPengguna vo) {
        return "Assalamu'alaikum Wr Wb, " + vo.getNama() + "\n\n"
                + "Berikut password terbaru untuk keperluan masuk ke aplikasi :\n\n"
                + "\tUsername    : " + vo.getUsername() + "\n"
                + "\tPassword    : " + vo.getPassword() + "\n\n"
                + "Terima kasih\n\n"
                + "Best regards,\n"
                + "Muhammadiyah Team";
    }

    public static String recover(String nama) {
        return "Pemulihan Akun " + nama;
    }

    public static String recBody(DetailPengguna vo) {
        return "Assalamu'alaikum Wr Wb, " + vo.getNama() + "\n\n"
                + "Pemulihan Akun Telah Berhasil Dengan Rincian Berikut :\n\n"
                + "\tUsername    : " + vo.getUsername() + "\n"
                + "\tNama        : " + vo.getNama() + "\n\n"
                + "Terima kasih\n\n"
                + "Best regards,\n"
                + "Muhammadiyah Team";
    }

    public static String pembayaran(String type, String nama) {
        return "Pembayaran " + type + " " + nama;
    }

    public static String transBody(String nis, String nama, Date tanggal, Double nominal, String keterangan) {
        return "Assalamu'alaikum Wr Wb, " + nama + "\n\n"
                + "Terlampir rincian detail pembayaran sebagai berikut:\n\n"
                + "\tNIS        : " + nis + "\n"
                + "\tTanggal    : " + DateUtil.dateToStr(tanggal, "yyyy-MM-dd") + "\n"
                + "\tJumlah     : " + NumberUtil.formatUang(nominal) + "\n"
                + "\tKeterangan : " + keterangan + "\n\n"
                + "Terima kasih\n\n"
                + "Best regards,\n"
                + "Muhammadiyah Team";
    }
}
