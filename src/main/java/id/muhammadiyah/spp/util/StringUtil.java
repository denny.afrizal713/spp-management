package id.muhammadiyah.spp.util;

import id.muhammadiyah.spp.data.entity.core.Pengguna;

public class StringUtil {

    private StringUtil() {
        super();
    }

    public static String namaDanGelarPengguna(Pengguna user) {
        String gelarDepan = user.getGelarDepan().equals("-") ? "" : user.getGelarDepan();
        if (gelarDepan.contains("H.") && gelarDepan.contains("KH.")) {
            gelarDepan = gelarDepan.replace("KH. ", "");
            gelarDepan = gelarDepan.replace("H.", "");
            gelarDepan = gelarDepan + "KH.";
        }
        String gelarBelakang = user.getGelarBelakang().equals("-") ? "" : user.getGelarBelakang();
        return gelarDepan + " " + user.getNama() + " " + gelarBelakang;
    }
}
