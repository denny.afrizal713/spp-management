package id.muhammadiyah.spp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    private DateUtil() {
        super();
    }

    public static Date tanggalAwal(Date tanggal) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(tanggal);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date tanggalAkhir(Date tanggal) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(tanggal);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    public static Date strToDate(String tanggal, String format) throws ParseException {
        return new SimpleDateFormat(format).parse(tanggal);
    }

    public static String dateToStr(Date tanggal, String format) {
        return new SimpleDateFormat(format).format(tanggal);
    }

    public static String konversiTanggal(Date tanggal) {
        String tgl = dateToStr(tanggal, "yyyy-MM-dd");
        String bulan = "";
        switch (tgl.substring(5, 7)) {
            case "01":
                bulan = "Januari";
                break;
            case "02":
                bulan = "Februari";
                break;
            case "03":
                bulan = "Maret";
                break;
            case "04":
                bulan = "April";
                break;
            case "05":
                bulan = "Mei";
                break;
            case "06":
                bulan = "Juni";
                break;
            case "07":
                bulan = "Juli";
                break;
            case "08":
                bulan = "Agustus";
                break;
            case "09":
                bulan = "September";
                break;
            case "10":
                bulan = "Oktober";
                break;
            case "11":
                bulan = "November";
                break;
            case "12":
                bulan = "Desember";
                break;
            default:
                bulan = "-";
                break;
        }
        return tgl.substring(8, 10) + " " + bulan + " " + tgl.substring(0, 4);
    }
}
