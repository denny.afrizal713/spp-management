package id.muhammadiyah.spp.service.common;

import id.muhammadiyah.spp.data.entity.core.Menu;
import id.muhammadiyah.spp.data.entity.core.Pembayaran;
import id.muhammadiyah.spp.data.entity.core.Pengguna;
import id.muhammadiyah.spp.data.entity.core.Siswa;
import id.muhammadiyah.spp.data.model.response.DropdownItem;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import id.muhammadiyah.spp.data.repository.*;
import id.muhammadiyah.spp.variable.enumeration.StatusEnum;
import id.muhammadiyah.spp.variable.enumeration.TipeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class CommonService {

    @Value("${spring.mail.username}")
    protected String mailUser;

    @Autowired
    protected JavaMailSender mailSender;

    @Autowired
    protected PenggunaRepository penggunaRepository;

    @Autowired
    protected JabatanRepository jabatanRepository;

    @Autowired
    protected MenuRepository menuRepository;

    @Autowired
    protected SiswaRepository siswaRepository;

    @Autowired
    protected TransaksiRepository transaksiRepository;

    @Autowired
    protected PembayaranRepository pembayaranRepository;

    @Autowired
    protected DetailSppTbRepository detailSppTbRepository;

    @Autowired
    protected DetailUpRepository detailUpRepository;

    @Autowired
    protected DetailSppTlRepository detailSppTlRepository;

    @Autowired
    protected SiswaHistoryRepository siswaHistoryRepository;

    @Autowired
    protected DetailDuRepository detailDuRepository;

    public static final String STAT = "status";

    protected void sendMail(String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailUser);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }

    public List<DropdownItem> generateDropdown(String[] request) {
        return IntStream.range(0, request.length).mapToObj(x -> new DropdownItem(x + 1, request[x])).collect(Collectors.toList());
    }

    public String generateNip() {
        Pengguna user = penggunaRepository.findFirstByOrderByNipDesc();
        int id = 1;
        if (user != null) {
            id = Integer.parseInt(user.getNip());
            id++;
        }
        String nip = "0000000000" + id;
        return nip.substring(nip.length() - 10);
    }

    public String generateNis(String jenjang) {
        Siswa siswa = siswaRepository.findFirstByJenjangOrderByNisDesc(jenjang);
        int nis = 1;
        if (siswa != null) {
            nis = Integer.parseInt(siswa.getNis());
            nis++;
        }
        String cnis = "0000000000" + nis;
        return cnis.substring(cnis.length() - 10);
    }

    public String tentukanTipeBayar(TipeEnum tipe) {
        String type = "";
        switch (tipe) {
            case UP:
                type = "UP";
                break;
            case SPPTB:
                type = "TB";
                break;
            case SPPTL:
                type = "TL";
                break;
            case DU:
                type = "DU";
                break;
            default:
                type = "-";
                break;
        }
        return type;
    }

    public String generateNoBayar(String nis, TipeEnum tipe, String noSebelumnya) {
        int no = 0;
        if (noSebelumnya.equals("") || !tentukanTipeBayar(tipe).equals(noSebelumnya.substring(0, 2))) {
            Pembayaran bayar = cariPembayaranTerakhir(tipe, nis);
            no = bayar == null ? 0 : Integer.parseInt(bayar.getNoBayar().substring(bayar.getNoBayar().length() - 7));
        } else {
            no = Integer.parseInt(noSebelumnya.substring(noSebelumnya.length() - 7));
        }
        if (no == 0) {
            return tentukanTipeBayar(tipe) + nis + "0000001";
        }
        no++;
        String nomor = "0000000" + no;
        String noBayar = nomor.substring(nomor.length() - 7);
        return tentukanTipeBayar(tipe) + nis + noBayar;
    }

    public String[] daftarTahunAjaran() {
        int tahunBerjalan = Integer.parseInt(cariTahunAjaran().substring(0, 4));
        int tahunMulai = tahunBerjalan - 10;
        List<String> listTahun = new ArrayList<>();
        listTahun.add("Semua");
        for (int x = tahunMulai; x <= tahunBerjalan; x++) {
            int tahunSelanjutnya = x + 1;
            listTahun.add(x + "-" + String.valueOf(tahunSelanjutnya).substring(2, 4));
        }
        return listTahun.stream().toArray(String[]::new);
    }

    public String cariTahunAjaran() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        String period = sdf.format(new Date());
        String bulan = period.split("-")[0];
        String tahun = period.split("-")[1];
        String prev = String.valueOf(Integer.parseInt(tahun) - 1);
        String next = String.valueOf(Integer.parseInt(tahun) + 1);
        if (Integer.parseInt(bulan) > 6) {
            return tahun + "-" + next.substring(2, 4);
        }
        return prev + "-" + tahun.substring(2, 4);
    }

    public Long idTerakhir(TipeEnum parameter, String nis) {
        long id;
        switch (parameter) {
            case SISWA:
                id = siswaRepository.findFirstByOrderByIdSiswaDesc() == null ? 0L : siswaRepository.findFirstByOrderByIdSiswaDesc().getIdSiswa();
                break;
            case TRANS:
                id = transaksiRepository.findFirstByOrderByIdTransaksiDesc() == null ? 0L : transaksiRepository.findFirstByOrderByIdTransaksiDesc().getIdTransaksi();
                break;
            case UP:
                id = detailUpRepository.findFirstByOrderByIdUpDesc() == null ? 0L : detailUpRepository.findFirstByOrderByIdUpDesc().getIdUp();
                break;
            case SPPTB:
                id = detailSppTbRepository.findFirstByOrderByIdSppTbDesc() == null ? 0L : detailSppTbRepository.findFirstByOrderByIdSppTbDesc().getIdSppTb();
                break;
            case SPPTL:
                id = detailSppTlRepository.findFirstByIdNisOrderByIdIdSppTlDesc(nis) == null ? 0L : detailSppTlRepository.findFirstByIdNisOrderByIdIdSppTlDesc(nis).getId().getIdSppTl();
                break;
            case PAY:
                id = pembayaranRepository.findFirstByIdNisOrderByIdIdBayarDesc(nis) == null ? 0L : pembayaranRepository.findFirstByIdNisOrderByIdIdBayarDesc(nis).getId().getIdBayar();
                break;
            case DU:
                id = detailDuRepository.findFirstByIdNisOrderByIdIdDuDesc(nis) == null ? 0L : detailDuRepository.findFirstByIdNisOrderByIdIdDuDesc(nis).getId().getIdDu();
                break;
            default:
                id = siswaHistoryRepository.findFirstByIdNisOrderByIdIdHistoryDesc(nis) == null ? 0L : siswaHistoryRepository.findFirstByIdNisOrderByIdIdHistoryDesc(nis).getId().getIdHistory();
                break;
        }
        return id;
    }

    public Pembayaran cariPembayaranTerakhir(TipeEnum tipe, String nis) {
        Specification<Pembayaran> specs = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("jenisPembayaran"), tipe.getLabel()));
            predicates.add(criteriaBuilder.equal(root.get("id").get("nis"), nis));
            criteriaQuery.orderBy(criteriaBuilder.desc(root.get("noBayar")));
            Predicate[] preds = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(preds));
        };
        List<Pembayaran> listBayar = pembayaranRepository.findAll(specs);
        return listBayar.isEmpty() ? null : listBayar.get(0);
    }

    public List<Siswa> cariDaftarSiswa(List<String> nis, String jenjang) {
        Specification<Siswa> specs = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(root.get("nis").in(nis));
            predicates.add(criteriaBuilder.equal(root.get("jenjang"), jenjang));
            Predicate[] preds = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(preds));
        };
        return siswaRepository.findAll(specs);
    }

    public List<Siswa> cariAlumni(List<String> kelas) {
        Specification<Siswa> specs = (root, cq, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get(STAT), StatusEnum.NON.getLabel()));
            predicates.add(root.get("kelas").in(kelas));
            Predicate[] preds = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(preds));
        };
        return siswaRepository.findAll(specs);
    }

    public List<Siswa> cariSiswaAktif(List<String> kelas) {
        Specification<Siswa> specs = (root, cq, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(root.get("kelas").in(kelas));
            predicates.add(cb.equal(root.get(STAT), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(preds));
        };
        return siswaRepository.findAll(specs);
    }

    public List<Siswa> cariDariKelas(String kelas) {
        Specification<Siswa> specs = (root, cq, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("kelas"), kelas));
            predicates.add(cb.equal(root.get(STAT), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(preds));
        };
        return siswaRepository.findAll(specs);
    }

    public List<String> cariKelas(UserLogin user) {
        List<String> kelas = new ArrayList<>();
        if (user.getJabatan().getIdJabatan() == 2) {
            List<Menu> menus = menuRepository.findByParentId(2L);
            menus.forEach(x -> kelas.add(x.getNama()));
        } else {
            user.getAkses().forEach(x -> {
                if (x.getParent() == 2) {
                    kelas.add(x.getNama());
                }
            });
        }
        return kelas;
    }

    public String cariJenjang(String kelas) {
        Menu menu = menuRepository.findFirstByNama(kelas);
        return menu == null ? "-" : menu.getJenjang();
    }

    public Siswa cariBerdasarkanNisdanJenjang(String nis, String jenjang) {
        Specification<Siswa> specs = (root, cq, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("nis"), nis));
            predicates.add(cb.equal(root.get("jenjang"), jenjang));
            predicates.add(cb.equal(root.get(STAT), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(preds));
        };
        return siswaRepository.findOne(specs).orElse(null);
    }

    public String cariBulan() {
        String bln = new SimpleDateFormat("MM").format(new Date());
        switch (bln) {
            case "01":
                return "Januari";
            case "02":
                return "Februari";
            case "03":
                return "Maret";
            case "04":
                return "April";
            case "05":
                return "Mei";
            case "06":
                return "Juni";
            case "07":
                return "Juli";
            case "08":
                return "Agustus";
            case "09":
                return "September";
            case "10":
                return "Oktober";
            case "11":
                return "November";
            case "12":
                return "Desember";
            default:
                return "-";
        }
    }
}
