package id.muhammadiyah.spp.service.core;

import id.muhammadiyah.spp.data.model.response.HakAksesMenu;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface JabatanService {

    public List<HakAksesMenu> ambilHakAksesList(String jabatan, String kelas);
}
