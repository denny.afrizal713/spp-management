package id.muhammadiyah.spp.service.core;

import id.muhammadiyah.spp.data.entity.core.Siswa;
import id.muhammadiyah.spp.data.model.request.CariSiswa;
import id.muhammadiyah.spp.data.model.request.InputSiswa;
import id.muhammadiyah.spp.data.model.response.DetailDaftarSiswa;
import id.muhammadiyah.spp.data.model.response.DropdownItem;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserAkses;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface SiswaService {

    public List<DetailDaftarSiswa> daftarSiswa(CariSiswa cariSiswa, List<UserAkses> akses);

    public Siswa detailSiswa(String nis, String jenjang);

    public List<DropdownItem> cariDaftarTahunAjaran();

    public Response simpanDataSiswa(InputSiswa siswa);

    public Response ubahDataSiswa(InputSiswa siswa);

    public Response hapusDataSiswa(String nis, String jenjang, String user);
}
