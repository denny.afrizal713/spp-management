package id.muhammadiyah.spp.service.core;

import com.itextpdf.text.DocumentException;
import id.muhammadiyah.spp.data.model.request.CariPembayaran;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

@Transactional
public interface LaporanService {

    public Response cariTransaksi(CariPembayaran trans, UserLogin user) throws ParseException;

    public void cetakTransaksi(CariPembayaran trans, UserLogin user, HttpServletResponse response) throws DocumentException, IOException, ParseException;

    public Response cariTunggakan(String bulan, UserLogin user);

    public void cetakTunggakan(String bulan, UserLogin user, HttpServletResponse response) throws DocumentException, IOException;
}
