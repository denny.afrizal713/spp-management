package id.muhammadiyah.spp.service.core;

import id.muhammadiyah.spp.data.entity.core.Pengguna;
import id.muhammadiyah.spp.data.model.request.CariAkunData;
import id.muhammadiyah.spp.data.model.request.CariPengguna;
import id.muhammadiyah.spp.data.model.request.InputPengguna;
import id.muhammadiyah.spp.data.model.response.DaftarPengguna;
import id.muhammadiyah.spp.data.model.response.DropdownItem;
import id.muhammadiyah.spp.data.model.response.Response;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface PenggunaService {

    public Response simpanPengguna(InputPengguna pengguna);

    public Pengguna getByUsername(String username);

    public List<DaftarPengguna> cariPengguna(CariPengguna pengguna);

    public List<DropdownItem> getOption(String key);

    public Pengguna getByNipAndUsername(String nip, String username);

    public Response ubahPassword(Pengguna pengguna, String plain);

    public Response pulihkanAkun(Pengguna pengguna);

    public Pengguna cariDenganNip(String nip);

    public Response hapusPengguna(Pengguna user);

    public List<Pengguna> cariAkunContains(CariAkunData akun);

    public Pengguna cariWalas(String kelas);
}
