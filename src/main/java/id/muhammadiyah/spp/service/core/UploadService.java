package id.muhammadiyah.spp.service.core;

import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UploadData;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Transactional
public interface UploadService {

    public void unduhKonsep(HttpServletResponse response);

    public Response readXlsxFile(MultipartFile multipartFile);

    public Response readXlsFile(MultipartFile multipartFile);

    public Response simpanUploadData(List<UploadData> listData, String username,List<String> listNis);
}
