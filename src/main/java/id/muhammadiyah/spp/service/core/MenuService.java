package id.muhammadiyah.spp.service.core;

import id.muhammadiyah.spp.data.entity.core.Menu;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface MenuService {

    public Menu cariBerdasarkanNama(String nama);

    public Menu cariBerdasarkanId(Long id);
}
