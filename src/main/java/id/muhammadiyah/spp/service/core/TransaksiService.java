package id.muhammadiyah.spp.service.core;

import id.muhammadiyah.spp.data.model.request.Penyesuaian;
import id.muhammadiyah.spp.data.model.request.TransBayar;
import id.muhammadiyah.spp.data.model.response.Response;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TransaksiService {

    public Response bayarUangPangkal(TransBayar trans);

    public Response bayarSpp(TransBayar trans);

    public Response bayarSppTl(TransBayar trans);

    public Response bayarDaftarUlang(TransBayar trans);

    public Response penyesuaian(Penyesuaian bayar);
}
