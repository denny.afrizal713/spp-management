package id.muhammadiyah.spp.service.implement;

import id.muhammadiyah.spp.data.entity.core.Menu;
import id.muhammadiyah.spp.service.common.CommonService;
import id.muhammadiyah.spp.service.core.MenuService;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceImplement extends CommonService implements MenuService {

    @Override
    public Menu cariBerdasarkanNama(String nama) {
        return menuRepository.findFirstByNama(nama);
    }

    @Override
    public Menu cariBerdasarkanId(Long id) {
        return menuRepository.findByIdMenu(id);
    }
}
