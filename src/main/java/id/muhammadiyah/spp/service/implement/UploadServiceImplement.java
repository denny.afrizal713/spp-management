package id.muhammadiyah.spp.service.implement;

import id.muhammadiyah.spp.data.entity.core.*;
import id.muhammadiyah.spp.data.entity.embed.DetailDuId;
import id.muhammadiyah.spp.data.entity.embed.DetailSppTlId;
import id.muhammadiyah.spp.data.entity.embed.SiswaHistoryId;
import id.muhammadiyah.spp.data.model.response.GagalUploadData;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UploadData;
import id.muhammadiyah.spp.data.model.response.UploadDataResponse;
import id.muhammadiyah.spp.service.common.CommonService;
import id.muhammadiyah.spp.service.core.UploadService;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import id.muhammadiyah.spp.variable.enumeration.StatusEnum;
import id.muhammadiyah.spp.variable.enumeration.TipeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Slf4j
@Service
public class UploadServiceImplement extends CommonService implements UploadService {

    private static final String[] header = {
            "No",
            "NIS",
            "Nama",
            "Jenis Kelamin",
            "Kelas",
            "Email",
            "Uang Pangkal",
            "Daftar Ulang",
            "SPP TL",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember",
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Jumlah"
    };

    private StringBuilder valueKosong = new StringBuilder();
    private StringBuilder salahFormat = new StringBuilder();

    public Boolean cekHeader(List<String> headers) {
        boolean hasil = true;
        for (int x = 0; x < headers.size(); x++) {
            if (!headers.get(x).equals(header[x])) {
                hasil = false;
                break;
            }
        }
        return hasil;
    }

    public String cekHasil() {
        String hasilPesan = "";
        if (!valueKosong.toString().equals("")) {
            hasilPesan = "Data " + valueKosong.toString().substring(0, valueKosong.toString().length() - 2)
                    + " Tidak Diisi\r\n";
        } else if (!salahFormat.toString().equals("")) {
            hasilPesan = "Format Data " + salahFormat.toString().substring(0, salahFormat.toString().length() - 2)
                    + " Salah, Harap Memasukkan Nilai Angka";
        }
        return hasilPesan;
    }

    public UploadData setUploadData(Integer no, List<String> data) {
        UploadData upl = new UploadData();
        DecimalFormat dec = new DecimalFormat("###,###,###.##");
        for (int x = 0; x < data.size(); x++) {
            String val = data.get(x) == null ? "" : data.get(x);
            if (x >= 5 && Boolean.TRUE.equals(cekAngka(val))) {
                val = val.replace(".", "");
                val = val.replace("Rp. ", "");
                val = dec.format(Double.parseDouble(val));
            }
            switch (x) {
                case 1:
                    upl.setNis(val);
                    break;
                case 2:
                    upl.setNama(val);
                    break;
                case 3:
                    upl.setJenisKelamin(val);
                    break;
                case 4:
                    upl.setKelas(val);
                    break;
                case 5:
                    upl.setUangPangkal(val);
                    break;
                case 6:
                    upl.setDaftarUlang(val);
                    break;
                case 7:
                    upl.setSppTl(val);
                    break;
                case 8:
                    upl.setJuli(val);
                    break;
                case 9:
                    upl.setAgustus(val);
                    break;
                case 10:
                    upl.setSeptember(val);
                    break;
                case 11:
                    upl.setOktober(val);
                    break;
                case 12:
                    upl.setNovember(val);
                    break;
                case 13:
                    upl.setDesember(val);
                    break;
                case 14:
                    upl.setJanuari(val);
                    break;
                case 15:
                    upl.setFebruari(val);
                    break;
                case 16:
                    upl.setMaret(val);
                    break;
                case 17:
                    upl.setApril(val);
                    break;
                case 18:
                    upl.setMei(val);
                    break;
                case 19:
                    upl.setJuni(val);
                    break;
                case 20:
                    upl.setJumlah(val);
                    break;
                default:
                    upl.setNo(no);
                    break;
            }
        }
        return upl;
    }

    public void cekDetail(Integer no, String value) {
        if (value.equals("") || value.equals(" ")) {
            valueKosong.append(header[no]).append(", ");
        }
        if (no >= 5 && Boolean.FALSE.equals(cekAngka(value))) {
            salahFormat.append(header[no]).append(", ");
        }
    }

    public Boolean cekAngka(String str) {
        if (str == null) {
            return false;
        }
        str = str.replace(".", "");
        str = str.replace("Rp. ", "");
        Pattern ptrn = Pattern.compile("-?\\d+(\\.\\d+)?");
        return ptrn.matcher(str).matches();
    }

    public List<GagalUploadData> cekDataSiswa(List<Siswa> listSiswa, List<UploadData> listData, String tahunAjaran) {
        List<GagalUploadData> listGagal = new ArrayList<>();
        listData.forEach(x -> {
            String nis = x.getNis().replace(".0", "");
            int no = listGagal.size();
            Siswa siswa = listSiswa.stream().filter(c -> c.getNis().equals(nis)).findFirst().orElse(null);
            if (siswa != null && tahunAjaran.equals(siswa.getTahunAjaran())) {
                no++;
                listGagal.add(
                        new GagalUploadData(no, "Data NIS " + nis + " Sudah Terdaftar Atas Nama " + siswa.getNama()));
            }
        });
        return listGagal;
    }

    public Double hitungTotalSpp(UploadData data) {
        Double total = 0.0;
        try {
            total += Double.parseDouble(data.getJuli().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getAgustus().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getSeptember().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getOktober().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getNovember().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getDesember().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getJanuari().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getFebruari().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getMaret().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getApril().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getMei().replace(".", "").replace(",", ""));
            total += Double.parseDouble(data.getJuni().replace(".", "").replace(",", ""));
        } catch (Exception e) {
            total = 0.0;
        }
        return total;
    }

    @Override
    public void unduhKonsep(HttpServletResponse response) {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("SPP");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFont(headerFont);
            headerStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            headerStyle.setBorderTop(BorderStyle.THIN);
            headerStyle.setBorderBottom(BorderStyle.THIN);
            headerStyle.setBorderLeft(BorderStyle.THIN);
            headerStyle.setBorderRight(BorderStyle.THIN);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            Row headerRow = sheet.createRow(0);
            for (int x = 0; x < header.length; x++) {
                Cell headerCell = headerRow.createCell(x);
                headerCell.setCellValue(header[x]);
                headerCell.setCellStyle(headerStyle);
            }
            CellStyle itemCell = workbook.createCellStyle();
            itemCell.setBorderRight(BorderStyle.THIN);
            itemCell.setBorderLeft(BorderStyle.THIN);
            itemCell.setBorderTop(BorderStyle.THIN);
            itemCell.setBorderBottom(BorderStyle.THIN);
            for (int x = 0; x < 3; x++) {
                int index = x + 1;
                String tl = "0";
                String jkel = "Laki Laki";
                if (index == 3) {
                    tl = "3.915.000";
                    jkel = "Perempuan";
                }
                String kelas = "X MIPA 1";
                String email = "siswa@mail.com";
                String uangPangkal = "1.000.000";
                String biaya = "675.000";
                String jumlah = "9.100.000";
                Row xrow = sheet.createRow(x + 1);
                Cell no = xrow.createCell(0);
                no.setCellValue(index);
                no.setCellStyle(itemCell);
                Cell nis = xrow.createCell(1);
                nis.setCellValue(index);
                nis.setCellStyle(itemCell);
                Cell nama = xrow.createCell(2);
                nama.setCellValue("Siswa " + index);
                nama.setCellStyle(itemCell);
                Cell klm = xrow.createCell(3);
                klm.setCellValue(jkel);
                klm.setCellStyle(itemCell);
                Cell kls = xrow.createCell(4);
                kls.setCellValue(kelas);
                kls.setCellStyle(itemCell);
                Cell eml = xrow.createCell(5);
                eml.setCellValue(email);
                eml.setCellStyle(itemCell);
                Cell up = xrow.createCell(6);
                up.setCellValue(uangPangkal);
                up.setCellStyle(itemCell);
                Cell du = xrow.createCell(7);
                du.setCellValue(tl);
                du.setCellStyle(itemCell);
                Cell spptl = xrow.createCell(8);
                spptl.setCellValue(tl);
                spptl.setCellStyle(itemCell);
                Cell jul = xrow.createCell(9);
                jul.setCellValue(biaya);
                jul.setCellStyle(itemCell);
                Cell aug = xrow.createCell(10);
                aug.setCellValue(biaya);
                aug.setCellStyle(itemCell);
                Cell sep = xrow.createCell(11);
                sep.setCellValue(biaya);
                sep.setCellStyle(itemCell);
                Cell oct = xrow.createCell(12);
                oct.setCellValue(biaya);
                oct.setCellStyle(itemCell);
                Cell nov = xrow.createCell(13);
                nov.setCellValue(biaya);
                nov.setCellStyle(itemCell);
                Cell dec = xrow.createCell(14);
                dec.setCellValue(biaya);
                dec.setCellStyle(itemCell);
                Cell jan = xrow.createCell(15);
                jan.setCellValue(biaya);
                jan.setCellStyle(itemCell);
                Cell feb = xrow.createCell(16);
                feb.setCellValue(biaya);
                feb.setCellStyle(itemCell);
                Cell mar = xrow.createCell(17);
                mar.setCellValue(biaya);
                mar.setCellStyle(itemCell);
                Cell apr = xrow.createCell(18);
                apr.setCellValue(biaya);
                apr.setCellStyle(itemCell);
                Cell may = xrow.createCell(19);
                may.setCellValue(biaya);
                may.setCellStyle(itemCell);
                Cell jun = xrow.createCell(20);
                jun.setCellValue(biaya);
                jun.setCellStyle(itemCell);
                Cell jml = xrow.createCell(21);
                jml.setCellValue(jumlah);
                jml.setCellStyle(itemCell);
            }
            for (int x = 0; x < header.length; x++) {
                sheet.autoSizeColumn(x);
            }
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            workbook.write(byteStream);
            byte[] xbyte = byteStream.toByteArray();
            response.setContentType("application/ms-excel");
            response.setContentLength(xbyte.length);
            response.setHeader("Expires", "0");
            response.setHeader("Content-Disposition", "attachment; filename=SPP.xlsx");
            OutputStream outStream = response.getOutputStream();
            outStream.write(xbyte);
            outStream.flush();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    @Override
    public Response readXlsxFile(MultipartFile multipartFile) {
        List<UploadData> listXlsxData = new ArrayList<>();
        List<GagalUploadData> listGagal = new ArrayList<>();
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(multipartFile.getInputStream());
            XSSFSheet sheet = workbook.getSheetAt(0);
            List<String> headers = new ArrayList<>();
            XSSFRow rowHeader = sheet.getRow(0);
            for (int c = 0; c < rowHeader.getPhysicalNumberOfCells(); c++) {
                headers.add(rowHeader.getCell(c).toString());
            }
            if (Boolean.TRUE.equals(cekHeader(headers))) {
                for (int x = 1; x < sheet.getPhysicalNumberOfRows(); x++) {
                    XSSFRow rows = sheet.getRow(x);
                    valueKosong = new StringBuilder();
                    salahFormat = new StringBuilder();
                    List<String> xlsxDetail = new ArrayList<>();
                    for (int c = 0; c < rows.getPhysicalNumberOfCells(); c++) {
                        cekDetail(c, rows.getCell(c).toString());
                        xlsxDetail.add(rows.getCell(c).toString().replace(".0", ""));
                    }
                    if (!valueKosong.toString().equals("") || !salahFormat.toString().equals("")) {
                        listGagal.add(new GagalUploadData(x, cekHasil()));
                    }
                    listXlsxData.add(setUploadData(x, xlsxDetail));
                }
                return ResponseUtil.setResponse(
                        HttpStatus.OK.value(),
                        "",
                        new UploadDataResponse(listXlsxData, listGagal));
            }
            return ResponseUtil.setResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "Format Tidak Sesuai, " +
                            "Harap Mengikuti Format Yang Sesuai Atau Download Template Untuk Mengetahui Format Yang Sesuai",
                    null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage(), null);
        }
    }

    @Override
    public Response readXlsFile(MultipartFile multipartFile) {
        List<UploadData> listXlsData = new ArrayList<>();
        List<GagalUploadData> listGagal = new ArrayList<>();
        try {
            HSSFWorkbook workbook = new HSSFWorkbook(multipartFile.getInputStream());
            HSSFSheet sheet = workbook.getSheetAt(0);
            HSSFRow headerRow = sheet.getRow(0);
            List<String> headers = new ArrayList<>();
            for (int x = 0; x < headerRow.getPhysicalNumberOfCells(); x++) {
                headers.add(headerRow.getCell(x).toString());
            }
            if (Boolean.TRUE.equals(cekHeader(headers))) {
                for (int x = 1; x < sheet.getPhysicalNumberOfRows(); x++) {
                    List<String> xlsDetail = new ArrayList<>();
                    HSSFRow rows = sheet.getRow(x);
                    valueKosong = new StringBuilder();
                    salahFormat = new StringBuilder();
                    for (int c = 0; c < rows.getPhysicalNumberOfCells(); c++) {
                        xlsDetail.add(rows.getCell(c).toString());
                        cekDetail(c, rows.getCell(c).toString());
                    }
                    if (!valueKosong.toString().equals("") || !salahFormat.toString().equals("")) {
                        listGagal.add(new GagalUploadData(x, cekHasil()));
                    }
                    listXlsData.add(setUploadData(x, xlsDetail));
                }
                return ResponseUtil.setResponse(
                        HttpStatus.OK.value(),
                        "",
                        new UploadDataResponse(listXlsData, listGagal));
            }
            return ResponseUtil.setResponse(
                    HttpStatus.BAD_REQUEST.value(),
                    "Format Tidak Sesuai, " +
                            "Harap Mengikuti Format Yang Sesuai Atau Download Template Untuk Mengetahui Format Yang Sesuai",
                    null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        }
    }

    @Override
    public Response simpanUploadData(List<UploadData> listData, String username, List<String> listNis) {
        Date sekarang = new Date();
        try {
            String tahunAjaran = cariTahunAjaran();
            List<Siswa> listSiswa = cariDaftarSiswa(listNis, cariJenjang(listData.get(0).getKelas()));
            List<SiswaHistory> listHistory = new ArrayList<>();
            List<DetailUp> listUangPangkal = new ArrayList<>();
            List<DetailSppTb> listSppTb = new ArrayList<>();
            List<Transaksi> listTrans = new ArrayList<>();
            listSiswa.forEach(x -> {
                String nis = x.getNis().replace(".0", "");
                SiswaHistory history = new SiswaHistory();
                SiswaHistoryId idHistory = new SiswaHistoryId();
                idHistory.setIdHistory(0L);
                idHistory.setNis(nis);
                history.setId(idHistory);
                history.setKelas(x.getKelas());
                history.setTahunAjaran(x.getTahunAjaran());
                listHistory.add(history);
                listUangPangkal.add(x.getUangPangkal());
                listSppTb.add(x.getSppTahunBerjalan());
                listTrans.add(x.getTransaksi());
            });
            List<Siswa> siswaBaru = new ArrayList<>();
            List<SiswaHistory> historyBaru = new ArrayList<>();
            List<Transaksi> transBaru = new ArrayList<>();
            List<DetailUp> uangPangkalBaru = new ArrayList<>();
            List<DetailDu> daftarUlangBaru = new ArrayList<>();
            List<DetailSppTb> sppTbBaru = new ArrayList<>();
            List<DetailSppTl> sppTlBaru = new ArrayList<>();
            List<GagalUploadData> listGagal = cekDataSiswa(listSiswa, listData, tahunAjaran);
            if (listGagal.isEmpty()) {
                Long idSiswa = idTerakhir(TipeEnum.SISWA, null);
                Long idTransTerakhir = idTerakhir(TipeEnum.TRANS, null);
                Long idUpTerakhir = idTerakhir(TipeEnum.UP, null);
                Long idSppTbTerakhir = idTerakhir(TipeEnum.SPPTB, null);
                for (UploadData x : listData) {
                    String nis = x.getNis().replace(".0", "");
                    Long idSppTlTerakhir = idTerakhir(TipeEnum.SPPTL, nis);
                    Long idDuTerakhir = idTerakhir(TipeEnum.DU, nis);
                    Long idHistoryTerakhir = idTerakhir(TipeEnum.HISTORY, nis);
                    boolean du = false;
                    Siswa cariSiswa = listSiswa.stream().filter(c -> c.getNis().equals(nis)).findFirst().orElse(null);
                    String tahunMasuk = tahunAjaran;
                    double sppTahunLalu = 0.0;
                    Siswa siswa = new Siswa();
                    siswa.setDibuatOleh(username);
                    siswa.setTanggalBuat(sekarang);
                    if (cariSiswa != null) {
                        du = true;
                        tahunMasuk = cariSiswa.getTahunMasuk();
                        sppTahunLalu = Double.parseDouble(x.getSppTl().replace(",", ""));
                        idHistoryTerakhir++;
                        SiswaHistory history = new SiswaHistory();
                        SiswaHistoryId historyId = new SiswaHistoryId();
                        historyId.setIdHistory(idHistoryTerakhir);
                        historyId.setNis(nis);
                        history.setId(historyId);
                        history.setTahunAjaran(cariSiswa.getTahunAjaran());
                        history.setKelas(cariSiswa.getKelas());
                        history.setDibuatOleh(username);
                        history.setTanggalBuat(sekarang);
                        history.setDiubahOleh(username);
                        history.setTanggalUbah(sekarang);
                        historyBaru.add(history);
                        siswa.setDibuatOleh(cariSiswa.getDibuatOleh());
                        siswa.setTanggalBuat(cariSiswa.getTanggalBuat());
                    }
                    idSiswa++;
                    siswa.setIdSiswa(idSiswa);
                    siswa.setNis(nis);
                    siswa.setKelas(x.getKelas());
                    siswa.setNama(x.getNama());
                    siswa.setJenisKelamin(x.getJenisKelamin());
                    siswa.setAlamat("-");
                    siswa.setTempatLahir("-");
                    siswa.setTanggalLahir(null);
                    siswa.setTahunAjaran(tahunAjaran);
                    siswa.setTahunMasuk(tahunMasuk);
                    siswa.setJenjang(cariJenjang(x.getKelas()));
                    siswa.setStatus(StatusEnum.AKTIF.getLabel());
                    siswa.setDiubahOleh(username);
                    siswa.setTanggalUbah(sekarang);
                    Transaksi cariTrans = listTrans.stream().filter(c -> c.getNis().equals(nis)).findFirst()
                            .orElse(null);
                    Transaksi transaksi = new Transaksi();
                    transaksi.setNis(nis);
                    transaksi.setDiubahOleh(username);
                    transaksi.setTanggalUbah(sekarang);
                    Double sppTbTotal = hitungTotalSpp(x);
                    Double sppTlTotal = 0.0;
                    Double bayar = 0.0;
                    Double sppTlLebih = 0.0;
                    Double upLebih = 0.0;
                    Double upTotal = Double.parseDouble(x.getUangPangkal().replace(".", "").replace(",", ""));
                    Double duTotal = Double.parseDouble(x.getDaftarUlang().replace(".", "").replace(",", ""));
                    Double duLebih = 0.0;
                    if (cariTrans != null) {
                        transaksi.setIdTransaksi(cariTrans.getIdTransaksi());
                        sppTlTotal += cariTrans.getSppTbSisa();
                        upTotal = cariTrans.getUpSisa();
                        sppTlLebih += cariTrans.getSppTlLebih();
                        upLebih = cariTrans.getUpLebih();
                        duTotal += cariTrans.getDuSisa();
                        duLebih = cariTrans.getDuLebih();
                        transaksi.setDibuatOleh(cariTrans.getDibuatOleh());
                        transaksi.setTanggalBuat(cariTrans.getTanggalBuat());
                    } else {
                        idTransTerakhir++;
                        transaksi.setIdTransaksi(idTransTerakhir);
                        transaksi.setDibuatOleh(username);
                        transaksi.setTanggalBuat(sekarang);
                    }
                    transaksi.setSppTbTotal(sppTbTotal);
                    transaksi.setSppTbBayar(bayar);
                    transaksi.setSppTbSisa(sppTbTotal);
                    transaksi.setSppTbLebih(bayar);
                    transaksi.setSppTlTotal(sppTlTotal);
                    transaksi.setSppTlBayar(bayar);
                    transaksi.setSppTlSisa(sppTlTotal);
                    transaksi.setSppTlLebih(sppTlLebih);
                    transaksi.setUpTotal(upTotal);
                    transaksi.setUpBayar(bayar);
                    transaksi.setUpSisa(upTotal);
                    transaksi.setUpLebih(upLebih);
                    transaksi.setDuTotal(duTotal);
                    transaksi.setDuBayar(bayar);
                    transaksi.setDuSisa(duTotal);
                    transaksi.setDuLebih(duLebih);
                    transBaru.add(transaksi);
                    DetailUp cariUp = listUangPangkal.stream().filter(c -> c.getNis().equals(nis)).findFirst()
                            .orElse(null);
                    DetailUp uangPangkal = new DetailUp();
                    Double jumlahUp = Double.parseDouble(x.getUangPangkal().replace(".", "").replace(",", ""));
                    uangPangkal.setNis(nis);
                    if (cariUp != null) {
                        uangPangkal.setIdUp(cariUp.getIdUp());
                        uangPangkal.setJumlahUp(cariUp.getJumlahUp() + jumlahUp);
                        uangPangkal.setDibuatOleh(cariUp.getDibuatOleh());
                        uangPangkal.setTanggalBuat(cariUp.getTanggalBuat());
                        uangPangkal.setDiubahOleh(username);
                        uangPangkal.setTanggalUbah(sekarang);
                    } else {
                        idUpTerakhir++;
                        uangPangkal.setIdUp(idUpTerakhir);
                        uangPangkal.setJumlahUp(jumlahUp);
                        uangPangkal.setDibuatOleh(username);
                        uangPangkal.setTanggalBuat(sekarang);
                        uangPangkal.setDiubahOleh(username);
                        uangPangkal.setTanggalUbah(sekarang);
                    }
                    uangPangkalBaru.add(uangPangkal);
                    if (du) {
                        DetailDu daftarUlang = new DetailDu();
                        idDuTerakhir++;
                        DetailDuId duId = new DetailDuId();
                        duId.setIdDu(idDuTerakhir);
                        duId.setNis(nis);
                        daftarUlang.setId(duId);
                        daftarUlang
                                .setJumlahDu(Double.parseDouble(x.getDaftarUlang().replace(".", "").replace(",", "")));
                        daftarUlang.setTahunAjaran(tahunAjaran);
                        daftarUlang.setDibuatOleh(username);
                        daftarUlang.setTanggalBuat(sekarang);
                        daftarUlang.setDiubahOleh(username);
                        daftarUlang.setTanggalUbah(sekarang);
                        daftarUlangBaru.add(daftarUlang);
                    }
                    DetailSppTb cariSppTb = listSppTb.stream().filter(c -> c.getNis().equals(nis)).findFirst().orElse(null);
                    DetailSppTb sppTb = new DetailSppTb();
                    sppTb.setNis(nis);
                    sppTb.setTahunAjaran(tahunAjaran);
                    sppTb.setSppTl(sppTahunLalu);
                    sppTb.setSppJuli(Double.parseDouble(x.getJuli().replace(".", "").replace(",", "")));
                    sppTb.setSppAgustus(Double.parseDouble(x.getAgustus().replace(".", "").replace(",", "")));
                    sppTb.setSppSeptember(Double.parseDouble(x.getSeptember().replace(".", "").replace(",", "")));
                    sppTb.setSppOktober(Double.parseDouble(x.getOktober().replace(".", "").replace(",", "")));
                    sppTb.setSppNovember(Double.parseDouble(x.getNovember().replace(".", "").replace(",", "")));
                    sppTb.setSppDesember(Double.parseDouble(x.getDesember().replace(".", "").replace(",", "")));
                    sppTb.setSppJanuari(Double.parseDouble(x.getJanuari().replace(".", "").replace(",", "")));
                    sppTb.setSppFebruari(Double.parseDouble(x.getFebruari().replace(".", "").replace(",", "")));
                    sppTb.setSppMaret(Double.parseDouble(x.getMaret().replace(".", "").replace(",", "")));
                    sppTb.setSppApril(Double.parseDouble(x.getApril().replace(".", "").replace(",", "")));
                    sppTb.setSppMei(Double.parseDouble(x.getMei().replace(".", "").replace(",", "")));
                    sppTb.setSppJuni(Double.parseDouble(x.getJuni().replace(".", "").replace(",", "")));
                    sppTb.setDibuatOleh(username);
                    sppTb.setTanggalBuat(sekarang);
                    sppTb.setDiubahOleh(username);
                    sppTb.setTanggalUbah(sekarang);
                    if (cariSppTb != null) {
                        sppTb.setIdSppTb(cariSppTb.getIdSppTb());
                        idSppTlTerakhir++;
                        DetailSppTl sppTl = new DetailSppTl();
                        DetailSppTlId sppTlId = new DetailSppTlId();
                        sppTlId.setNis(nis);
                        sppTlId.setIdSppTl(idSppTlTerakhir);
                        sppTl.setId(sppTlId);
                        sppTl.setTahunAjaran(cariSppTb.getTahunAjaran());
                        sppTl.setSppJuli(cariSppTb.getSppJuli());
                        sppTl.setSppAgustus(cariSppTb.getSppAgustus());
                        sppTl.setSppSeptember(cariSppTb.getSppSeptember());
                        sppTl.setSppOktober(cariSppTb.getSppOktober());
                        sppTl.setSppNovember(cariSppTb.getSppNovember());
                        sppTl.setSppDesember(cariSppTb.getSppDesember());
                        sppTl.setSppJanuari(cariSppTb.getSppJanuari());
                        sppTl.setSppFebruari(cariSppTb.getSppFebruari());
                        sppTl.setSppMaret(cariSppTb.getSppMaret());
                        sppTl.setSppApril(cariSppTb.getSppApril());
                        sppTl.setSppMei(cariSppTb.getSppMei());
                        sppTl.setSppJuni(cariSppTb.getSppJuni());
                        sppTl.setDibuatOleh(username);
                        sppTl.setTanggalBuat(sekarang);
                        sppTl.setDiubahOleh(username);
                        sppTl.setTanggalUbah(sekarang);
                        sppTlBaru.add(sppTl);
                    } else {
                        idSppTbTerakhir++;
                        sppTb.setIdSppTb(idSppTbTerakhir);
                    }
                    sppTbBaru.add(sppTb);
                    siswa.setIdTrans(transaksi.getIdTransaksi());
                    siswa.setIdUp(uangPangkal.getIdUp());
                    siswa.setIdSpp(sppTb.getIdSppTb());
                    siswaBaru.add(siswa);
                }
                siswaRepository.saveAllAndFlush(siswaBaru);
                transaksiRepository.saveAllAndFlush(transBaru);
                detailUpRepository.saveAllAndFlush(uangPangkalBaru);
                detailSppTbRepository.saveAllAndFlush(sppTbBaru);
                if (!daftarUlangBaru.isEmpty()) {
                    detailDuRepository.saveAllAndFlush(daftarUlangBaru);
                }
                if (!historyBaru.isEmpty()) {
                    siswaHistoryRepository.saveAllAndFlush(historyBaru);
                }
                if (!sppTlBaru.isEmpty()) {
                    detailSppTlRepository.saveAllAndFlush(sppTlBaru);
                }
                return ResponseUtil.setResponse(HttpStatus.OK.value(), PesanConstant.BERHASIL_SIMPAN, null);
            }
            return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(),
                    PesanConstant.GAGAL_SIMPAN + ". Lihat Tabel Dibawah Untuk Detailnya", listGagal);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(),
                    PesanConstant.GAGAL_SIMPAN + " : " + e.getMessage(), null);
        }
    }
}
