package id.muhammadiyah.spp.service.implement;

import id.muhammadiyah.spp.data.entity.core.Jabatan;
import id.muhammadiyah.spp.data.entity.core.Menu;
import id.muhammadiyah.spp.data.model.response.HakAksesMenu;
import id.muhammadiyah.spp.service.common.CommonService;
import id.muhammadiyah.spp.service.core.JabatanService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JabatanServiceImplement extends CommonService implements JabatanService {

    public static final String Y = "Ya";
    public static final String N = "Tidak";

    @Override
    public List<HakAksesMenu> ambilHakAksesList(String jabatan, String kelas) {
        List<HakAksesMenu> hakAkses = new ArrayList<>();
        Jabatan role = jabatanRepository.findFirstByNamaContainsOrderByIdJabatanDesc(jabatan);
        if (role != null) {
            if (kelas.equals("") || kelas.equals("-")) {
                role.getAkses().forEach(x -> {
                    if (x.getMenu().getParentId() != 0) {
                        Integer no = hakAkses.size() + 1;
                        String baca = x.getBaca() == 1 ? Y : N;
                        String tulis = x.getTulis() == 1 ? Y : N;
                        hakAkses.add(new HakAksesMenu(no, x.getMenu().getNama(), baca, tulis, tulis, tulis));
                    }
                });
            } else {
                Menu menuAkses = menuRepository.findFirstByNama(kelas);
                if (menuAkses != null && menuAkses.getJenjang().equals(jabatan.replace("Wali Kelas ", ""))) {
                    hakAkses.add(new HakAksesMenu(1, kelas, Y, N, N, N));
                }
            }
        }
        return hakAkses;
    }
}
