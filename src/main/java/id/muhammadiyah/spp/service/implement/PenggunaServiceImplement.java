package id.muhammadiyah.spp.service.implement;

import id.muhammadiyah.spp.data.entity.core.Jabatan;
import id.muhammadiyah.spp.data.entity.core.Menu;
import id.muhammadiyah.spp.data.entity.core.Pengguna;
import id.muhammadiyah.spp.data.model.request.CariAkunData;
import id.muhammadiyah.spp.data.model.request.CariPengguna;
import id.muhammadiyah.spp.data.model.request.InputPengguna;
import id.muhammadiyah.spp.data.model.response.DaftarPengguna;
import id.muhammadiyah.spp.data.model.response.DetailPengguna;
import id.muhammadiyah.spp.data.model.response.DropdownItem;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.service.common.CommonService;
import id.muhammadiyah.spp.service.core.PenggunaService;
import id.muhammadiyah.spp.util.*;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import id.muhammadiyah.spp.variable.constant.StringConstant;
import id.muhammadiyah.spp.variable.enumeration.StatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PenggunaServiceImplement extends CommonService implements PenggunaService {

    @Override
    public Response simpanPengguna(InputPengguna pengguna) {
        try {
            String pass = "";
            String plainPass = "-";
            Pengguna user = new Pengguna();
            if (pengguna.getNip() != null && !pengguna.getNip().equals("")) {
                user = getByNip(pengguna.getNip());
            }
            if (user.getPassword() != null) {
                pass = user.getPassword();
            } else {
                plainPass = NumberUtil.randomPassword();
                pass = EncryptUtil.encrypt(plainPass);
            }
            user.setNip(pengguna.getNip());
            if (pengguna.getNip().equals("-")) {
                user.setNip(generateNip());
            }
            user.setUsername(pengguna.getUsername());
            user.setNama(pengguna.getNama());
            user.setPassword(pass);
            user.setJenisKelamin(pengguna.getJenisKelamin());
            user.setEmail(pengguna.getEmail());
            user.setTotalGagal(pengguna.getTotalGagal());
            user.setGelarDepan(pengguna.getGelarDepan());
            if (pengguna.getGelarDepan() == null || pengguna.getGelarDepan().equals("")) {
                user.setGelarDepan(StringConstant.STRIP);
            }
            user.setGelarBelakang(pengguna.getGelarBelakang());
            if (pengguna.getGelarBelakang() == null || pengguna.getGelarBelakang().equals("")) {
                user.setGelarBelakang(StringConstant.STRIP);
            }
            user.setPendidikanTerakhir(pengguna.getPendidikanTerakhir());
            if (pengguna.getPendidikanTerakhir() == null || pengguna.getPendidikanTerakhir().equals("")) {
                user.setPendidikanTerakhir(StringConstant.STRIP);
            }
            user.setWaliKelas(pengguna.getWaliKelas());
            if (pengguna.getWaliKelas() == null || pengguna.getWaliKelas().equals("")) {
                user.setWaliKelas(StringConstant.STRIP);
            }
            user.setIdJabatan(pengguna.getIdJabatan());
            user.setStatus(StatusEnum.AKTIF.getLabel());
            user.setDibuatOleh(pengguna.getUserCreate());
            user.setTanggalBuat(pengguna.getDateCreate());
            user.setDiubahOleh(pengguna.getUserUpdate());
            user.setTanggalUbah(pengguna.getDateUpdate());
            String message = PesanConstant.BERHASIL_SIMPAN;
            if (!plainPass.equals("-")) {
                StringBuilder sb = new StringBuilder();
                sb.append(message).append("\r\n")
                        .append("Username")
                        .append(" : ").append(user.getUsername()).append("\r\n")
                        .append(" Untuk Detail Silahkan Periksa Email ")
                        .append(user.getEmail());
                message = sb.toString();
                sendMail(user.getEmail(), MailUtil.akunSubject(pengguna.getNama()), MailUtil.akunBody(new DetailPengguna(
                        1L,
                        user.getUsername(),
                        user.getNama(),
                        plainPass,
                        user.getEmail(),
                        "-",
                        pengguna.getJenisKelamin()
                )));
            }
            return ResponseUtil.setResponse(HttpStatus.OK.value(), message, penggunaRepository.save(user));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), PesanConstant.GAGAL_SIMPAN + " : " + e.getMessage(), null);
        }
    }

    @Override
    public Pengguna getByUsername(String username) {
        Specification<Pengguna> specs = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.or(criteriaBuilder.equal(root.get("username"), username), criteriaBuilder.equal(root.get("nip"), username)));
            predicates.add(criteriaBuilder.equal(root.get("status"), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(preds));
        };
        Optional<Pengguna> hasil = penggunaRepository.findOne(specs);
        return hasil.orElse(null);
    }

    public Pengguna getByNip(String nip) {
        Specification<Pengguna> specs = (root, cq, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("nip"), nip));
            predicates.add(cb.equal(root.get("status"), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(preds));
        };
        Optional<Pengguna> hasil = penggunaRepository.findOne(specs);
        return hasil.orElse(new Pengguna());
    }

    public Specification<Pengguna> buildSpecs(CariPengguna pengguna) {
        Specification<Pengguna> specs;
        specs = (root, cq, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            Join<Object, Object> jabatan = root.join("jabatan", JoinType.LEFT);
            if (pengguna.getNip() != null) {
                predicates.add(cb.like(root.get("nip"), "%" + pengguna.getNip() + "%"));
            }
            if (pengguna.getUsername() != null) {
                predicates.add(cb.like(root.get("username"), "%" + pengguna.getUsername() + "%"));
            }
            if (pengguna.getNama() != null) {
                predicates.add(cb.like(root.get("nama"), "%" + pengguna.getNama() + "%"));
            }
            if (pengguna.getGelarDepan() != null) {
                predicates.add(cb.like(root.get("gelarDepan"), "%" + pengguna.getGelarDepan() + "%"));
            }
            if (pengguna.getGelarBelakang() != null) {
                predicates.add(cb.like(root.get("gelarBelakang"), "%" + pengguna.getGelarBelakang() + "%"));
            }
            if (pengguna.getJenisKelamin() != null && !pengguna.getJenisKelamin().equals("")) {
                predicates.add(cb.equal(root.get("jenisKelamin"), pengguna.getJenisKelamin()));
            }
            if (pengguna.getWaliKelas() != null && !pengguna.getWaliKelas().equals("")) {
                predicates.add(cb.equal(root.get("waliKelas"), pengguna.getWaliKelas()));
            }
            if (pengguna.getJabatan() != null) {
                predicates.add(cb.like(jabatan.get("nama"), "%" + pengguna.getJabatan() + "%"));
            }
            if (pengguna.getAksesJenjang() != null && !pengguna.getAksesJenjang().equals("")) {
                predicates.add(cb.equal(jabatan.get("aksesJenjang"), pengguna.getAksesJenjang()));
            }
            predicates.add(cb.equal(root.get("status"), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(preds));
        };
        return specs;
    }

    @Override
    public List<DaftarPengguna> cariPengguna(CariPengguna pengguna) {
        List<Pengguna> users = penggunaRepository.findAll(buildSpecs(pengguna));
        List<DaftarPengguna> hasilPengguna = new ArrayList<>();
        users.forEach(x -> {
            hasilPengguna.add(new DaftarPengguna(
                    x.getNip(),
                    x.getUsername(),
                    StringUtil.namaDanGelarPengguna(x),
                    x.getJenisKelamin(),
                    x.getPendidikanTerakhir(),
                    x.getJabatan().getNama(),
                    x.getWaliKelas(),
                    x.getJabatan().getAksesJenjang()
            ));
        });
        return hasilPengguna;
    }

    @Override
    public List<DropdownItem> getOption(String key) {
        List<DropdownItem> items = new ArrayList<>();
        items.add(new DropdownItem(0, "Semua"));
        switch (key) {
            case "jkel":
                items.addAll(generateDropdown(new String[]{"Laki laki", "Perempuan"}));
                break;
            case "pend":
                items.addAll(generateDropdown(new String[]{"SD", "SMP", "SMA / SMK", "D1", "D2", "D3", "S1", "S2", "S3"}));
                break;
            case "walas":
                List<Menu> menus = menuRepository.findByParentId(2L);
                menus.forEach(x -> items.add(new DropdownItem(items.size(), x.getNama())));
                break;
            case "walassd":
                List<Menu> menusd = menuRepository.findByParentIdAndJenjang(2L, "SD");
                menusd.forEach(x -> items.add(new DropdownItem(items.size(), x.getNama())));
                break;
            case "walassmp":
                List<Menu> menusmp = menuRepository.findByParentIdAndJenjang(2L, "SMP");
                menusmp.forEach(x -> items.add(new DropdownItem(items.size(), x.getNama())));
                break;
            case "walassma":
                List<Menu> menusma = menuRepository.findByParentIdAndJenjang(2L, "SMA");
                menusma.forEach(x -> items.add(new DropdownItem(items.size(), x.getNama())));
                break;
            case "walassmk":
                List<Menu> menusmk = menuRepository.findByParentIdAndJenjang(2L, "SMK");
                menusmk.forEach(x -> items.add(new DropdownItem(items.size(), x.getNama())));
                break;
            case "gdpn":
                items.addAll(generateDropdown(new String[]{"Drs", "Dra", "KH", "H", "Hj"}));
                break;
            case "gblkg":
                items.addAll(generateDropdown(new String[]{"S.A.B", "S.A.P", "S.Adm", "S.Ag", "S.Agr", "S.Ant", "S.Ars", "S.Ds",
                        "S.E", "S.A", "S.E.I", "S.Farm", "S.Fil", "S.H", "S.H.I", "S.H.Int", "S.Hum", "S.Gz", "S.Kel", "S.I.K", "S.I.Kom",
                        "S.I.P", "S.I.Pol", "S.Ptk", "S.In", "S.Ked", "S.K.G", "S.K.H", "S.Hut", "S.Keb", "S.Kep", "S.K.M", "S.Kom",
                        "S.K.P.M", "S.M", "S.Mn", "S.M.B", "S.Mat", "S.Par", "S.Pd", "S.Pd.I", "S.Pd.SD", "S.Pi", "S.Han", "S.Pt",
                        "S.Psi", "S.Si", "S.Si.Teol", "S.S.T.P", "S.S", "S.Sn", "S.SI", "S.Sos", "S.Sy", "S.T", "S.TI", "S.T.P", "S.Th.I",
                        "S.Th", "S.Tr.K", "S.T.Han", "S.S.T.Han", "S.Tr.Sos", "MA", "M.Cs", "M.P.H", "M.Ag", "M.Hut", "M.M", "M.Si", "M.Kom", "M.TI",
                        "MMSI", "M.Pd", "M.Ak", "M.A.R.S", "M.Sn", "M.Farm", "M.Psi", "M.Kn", "M.MPd", "M.T", "M.Hum", "M.Stat",
                        "M.H", "M.AB", "M.AP", "MSIE", "MSEE", "MSA", "M.Econ"}));
                break;
            case "gdpnlk":
                items.addAll(generateDropdown(new String[]{"Drs", "KH", "H"}));
                break;
            case "gdpnpr":
                items.addAll(generateDropdown(new String[]{"Dra", "Hj"}));
                break;
            case "gblkgd1":
                items.addAll(generateDropdown(new String[]{"A.P"}));
                break;
            case "gblkgd2":
                items.addAll(generateDropdown(new String[]{"A.Ma"}));
                break;
            case "gblkgd3":
                items.addAll(generateDropdown(new String[]{"A.Md"}));
                break;
            case "gblkgs1":
                items.addAll(generateDropdown(new String[]{"S.A.B", "S.A.P", "S.Adm", "S.Ag", "S.Agr", "S.Ant", "S.Ars", "S.Ds",
                        "S.E", "S.A", "S.E.I", "S.Farm", "S.Fil", "S.H", "S.H.I", "S.H.Int", "S.Hum", "S.Gz", "S.Kel", "S.I.K", "S.I.Kom",
                        "S.I.P", "S.I.Pol", "S.Ptk", "S.In", "S.Ked", "S.K.G", "S.K.H", "S.Hut", "S.Keb", "S.Kep", "S.K.M", "S.Kom",
                        "S.K.P.M", "S.M", "S.Mn", "S.M.B", "S.Mat", "S.Par", "S.Pd", "S.Pd.I", "S.Pd.SD", "S.Pi", "S.Han", "S.Pt",
                        "S.Psi", "S.Si", "S.Si.Teol", "S.S.T.P", "S.S", "S.Sn", "S.SI", "S.Sos", "S.Sy", "S.T", "S.TI", "S.T.P", "S.Th.I",
                        "S.Th", "S.Tr.K", "S.T.Han", "S.S.T.Han", "S.Tr.Sos"}));
                break;
            case "gblkgs2":
                items.addAll(generateDropdown(new String[]{"MA", "M.Cs", "M.P.H", "M.Ag", "M.Hut", "M.M", "M.Si", "M.Kom", "M.TI",
                        "MMSI", "M.Pd", "M.Ak", "M.A.R.S", "M.Sn", "M.Farm", "M.Psi", "M.Kn", "M.MPd", "M.T", "M.Hum", "M.Stat",
                        "M.H", "M.AB", "M.AP", "MSIE", "MSEE", "MSA", "M.Econ"}));
                break;
            case "jbtn":
                List<Jabatan> jabatanList = jabatanRepository.findAll();
                jabatanList.forEach(x -> items.add(new DropdownItem(x.getIdJabatan().intValue(), x.getNama())));
                break;
            case "akses":
                items.addAll(generateDropdown(new String[]{"SD", "SMP", "SMA", "SMK"}));
                break;
        }
        return items;
    }

    @Override
    public Pengguna getByNipAndUsername(String nip, String username) {
        Specification<Pengguna> specs = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("username"), username));
            predicates.add(criteriaBuilder.equal(root.get("nip"), nip));
            predicates.add(criteriaBuilder.equal(root.get("status"), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(preds));
        };
        Optional<Pengguna> hasil = penggunaRepository.findOne(specs);
        return hasil.orElse(null);
    }

    @Override
    public Response ubahPassword(Pengguna pengguna, String plain) {
        try {
            String encryptPass = EncryptUtil.encrypt(plain);
            pengguna.setPassword(encryptPass);
            Pengguna user = penggunaRepository.save(pengguna);
            String message = PesanConstant.BERHASIL_UBAH;
            StringBuilder sb = new StringBuilder();
            sb.append(message).append("\r\n")
                    .append("Username")
                    .append(" : ").append(user.getUsername())
                    .append(" Untuk Detail Silahkan Periksa Email ")
                    .append(user.getEmail());
            message = sb.toString();
            sendMail(user.getEmail(), MailUtil.changePass("Ubah", user.getNama()), MailUtil.resetBody(new DetailPengguna(
                    1L,
                    user.getUsername(),
                    user.getNama(),
                    plain,
                    user.getEmail(),
                    "-",
                    pengguna.getJenisKelamin()
            )));
            return ResponseUtil.setResponse(HttpStatus.OK.value(), message, user);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), PesanConstant.GAGAL_UBAH + " : " + e.getMessage(), null);
        }
    }

    @Override
    public Response pulihkanAkun(Pengguna pengguna) {
        try {
            pengguna.setTotalGagal(0);
            Pengguna user = penggunaRepository.save(pengguna);
            sendMail(user.getEmail(), MailUtil.recover(user.getNama()), MailUtil.recBody(new DetailPengguna(
                    1L,
                    user.getUsername(),
                    user.getNama(),
                    user.getPassword(),
                    user.getEmail(),
                    "-",
                    pengguna.getJenisKelamin()
            )));
            return ResponseUtil.setResponse(HttpStatus.OK.value(), "Akun Berhasil Dipulihkan", user);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(), PesanConstant.GAGAL_UBAH + " : " + e.getMessage(), null);
        }
    }

    @Override
    public Pengguna cariDenganNip(String nip) {
        return getByNip(nip);
    }

    @Override
    public Response hapusPengguna(Pengguna user) {
        try {
            user.setStatus(StatusEnum.NON.getLabel());
            penggunaRepository.save(user);
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PesanConstant.BERHASIL_HAPUS, null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.BAD_REQUEST.value(), PesanConstant.GAGAL_HAPUS + " : " + e.getMessage(), null);
        }
    }

    @Override
    public List<Pengguna> cariAkunContains(CariAkunData akun) {
        Specification<Pengguna> specs = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.like(root.get("username"), "%" + akun.getUsername() + "%"));
            predicates.add(criteriaBuilder.like(root.get("nip"), "%" + akun.getNip() + "%"));
            predicates.add(criteriaBuilder.like(root.get("nama"), "%" + akun.getNama() + "%"));
            predicates.add(criteriaBuilder.equal(root.get("status"), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(preds));
        };
        return penggunaRepository.findAll(specs);
    }

    @Override
    public Pengguna cariWalas(String kelas) {
        Specification<Pengguna> specs = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("waliKelas"), kelas));
            predicates.add(criteriaBuilder.equal(root.get("status"), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return criteriaBuilder.and(predicates.toArray(preds));
        };
        Optional<Pengguna> hasil = penggunaRepository.findOne(specs);
        return hasil.orElse(null);
    }
}
