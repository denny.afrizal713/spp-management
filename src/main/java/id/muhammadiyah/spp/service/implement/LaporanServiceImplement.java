package id.muhammadiyah.spp.service.implement;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import id.muhammadiyah.spp.data.entity.core.*;
import id.muhammadiyah.spp.data.entity.core.Menu;
import id.muhammadiyah.spp.data.model.request.CariPembayaran;
import id.muhammadiyah.spp.data.model.response.DaftarTransaksi;
import id.muhammadiyah.spp.data.model.response.DaftarTunggakan;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserLogin;
import id.muhammadiyah.spp.service.common.CommonService;
import id.muhammadiyah.spp.service.core.LaporanService;
import id.muhammadiyah.spp.util.DateUtil;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.util.StringUtil;
import id.muhammadiyah.spp.variable.constant.JenisConstant;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LaporanServiceImplement extends CommonService implements LaporanService {

    public static final String TRS = "Transaksi";

    public DaftarTunggakan dataTunggakanAktif(int no, String kelas, String bulan) {
        double tunggakanSpp = 0.0;
        double tunggakanUp = 0.0;
        double totalTunggakan;
        for (Siswa sws : cariDariKelas(kelas)) {
            Transaksi trans = sws.getTransaksi();
            DetailSppTb tb = sws.getSppTahunBerjalan();
            tunggakanSpp += tb == null ? 0.0 : hitungTunggakanSpp(bulan, tb);
            tunggakanUp += trans == null ? 0.0 : trans.getUpSisa();
        }
        totalTunggakan = tunggakanSpp + tunggakanUp;
        Pengguna user = penggunaRepository.findFirstByWaliKelasOrderByTanggalBuatDesc(kelas);
        return new DaftarTunggakan(no, "Tunggakan Kelas " + kelas, tunggakanSpp, tunggakanUp, totalTunggakan, user == null ? "-" : StringUtil.namaDanGelarPengguna(user));
    }

    public DaftarTunggakan dataTunggakanAlumni(int no, List<String> kelas) {
        double totalSppAlumni = 0.0;
        double totalUpAlumni = 0.0;
        double totalTunggakanAlumni;
        for (Siswa sws : cariAlumni(kelas)) {
            Transaksi trans = sws.getTransaksi();
            totalSppAlumni += trans == null ? 0.0 : trans.getSppTbSisa();
            totalSppAlumni += trans == null ? 0.0 : trans.getSppTlSisa();
            totalUpAlumni += trans == null ? 0.0 : trans.getUpSisa();
        }
        totalTunggakanAlumni = totalSppAlumni + totalUpAlumni;
        return new DaftarTunggakan(no, "Tunggakan Siswa Sudah Lulus", totalSppAlumni, totalUpAlumni, totalTunggakanAlumni, "-");
    }

    public DaftarTunggakan dataTunggakanTahunLalu(int no, List<String> kelas) {
        double totalSppTl = 0.0;
        double totalUpTl = 0.0;
        double totalTunggakanTl;
        for (Siswa sws : cariSiswaAktif(kelas)) {
            Transaksi trans = sws.getTransaksi();
            totalSppTl += trans == null ? 0.0 : trans.getSppTlSisa();
            totalUpTl += trans == null ? 0.0 : trans.getUpSisa();
        }
        totalTunggakanTl = totalSppTl + totalUpTl;
        return new DaftarTunggakan(no, "Tunggakan Tahun Sebelumnya", totalSppTl, totalUpTl, totalTunggakanTl, "-");
    }

    public Double hitungTunggakanSpp(String bln, DetailSppTb tb) {
        double tunggakan;
        switch (bln) {
            case "Juli":
                tunggakan = tb.getSppJuli();
                break;
            case "Agustus":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus();
                break;
            case "September":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember();
                break;
            case "Oktober":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober();
                break;
            case "November":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember();
                break;
            case "Desember":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember();
                break;
            case "Januari":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari();
                break;
            case "Februari":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari();
                break;
            case "Maret":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret();
                break;
            case "April":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret() + tb.getSppApril();
                break;
            case "Mei":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret() + tb.getSppApril()
                        + tb.getSppMei();
                break;
            case "Juni":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret() + tb.getSppApril()
                        + tb.getSppMei() + tb.getSppJuni();
                break;
            default:
                tunggakan = 0.0;
                break;
        }
        return tunggakan;
    }

    public List<DaftarTunggakan> listTunggakan(String bulan, List<String> listKelas) {
        List<DaftarTunggakan> tunggakanList = new ArrayList<>();
        int no = 0;
        no++;
        tunggakanList.add(dataTunggakanAlumni(no, listKelas));
        no++;
        tunggakanList.add(dataTunggakanTahunLalu(no, listKelas));
        for (String kls : listKelas) {
            no++;
            tunggakanList.add(dataTunggakanAktif(no, kls, bulan));
        }
        return tunggakanList;
    }

    public List<DaftarTransaksi> listTrans(CariPembayaran trans, List<String> listKelas) throws ParseException {
        if (trans.getTanggal() == null) {
            trans.setTanggal(new Date());
        }
        Date tgl1 = DateUtil.tanggalAwal(trans.getTanggal());
        Date tgl2 = DateUtil.tanggalAkhir(trans.getTanggal());
        String jns = null;
        if (trans.getJenis() != null) {
            switch (trans.getJenis()) {
                case "up":
                    jns = JenisConstant.UP;
                    break;
                case "du":
                    jns = JenisConstant.DU;
                    break;
                case "tb":
                    jns = JenisConstant.TB;
                    break;
                case "tl":
                    jns = JenisConstant.TL;
                    break;
                default:
                    jns = null;
                    break;
            }
        }
        List<Object[]> hasil;
        if (jns != null && !listKelas.isEmpty()) {
            hasil = pembayaranRepository.filterByKelasAndTanggalAndJenis(listKelas, tgl1, tgl2, jns);
        } else if (jns == null && !listKelas.isEmpty()) {
            hasil = pembayaranRepository.filterByKelasAndTanggal(listKelas, tgl1, tgl2);
        } else if (jns != null && listKelas.isEmpty()) {
            hasil = pembayaranRepository.filterByTanggalAndJenis(tgl2, tgl2, jns);
        } else {
            hasil = pembayaranRepository.filterByTanggal(tgl1, tgl2);
        }
        List<DaftarTransaksi> listDaftar = new ArrayList<>();
        int no = 0;
        for (Object[] obj : hasil) {
            no++;
            String ket = obj[3].toString();
            if (ket.equals(JenisConstant.TB)) {
                ket = "SPP Bulan " + obj[4].toString();
            }
            String jenis = obj[3].toString();
            switch (jenis) {
                case JenisConstant.UP:
                    jenis = "Uang PSB";
                    break;
                case JenisConstant.DU:
                    jenis = "Uang Daftar Ulang";
                    break;
                case JenisConstant.TB:
                    jenis = "Uang SPP";
                    break;
                case JenisConstant.TL:
                    jenis = "Uang SPP Tahun Lalu";
                    break;
                default:
                    jenis = "-";
                    break;
            }
            DaftarTransaksi daftar = new DaftarTransaksi(
                    no,
                    obj[0].toString(),
                    obj[1].toString(),
                    obj[2].toString(),
                    jenis,
                    Double.parseDouble(obj[5].toString()),
                    ket,
                    DateUtil.strToDate(obj[6].toString(), "yyyy-MM-dd hh:mm:ss")
            );
            listDaftar.add(daftar);
        }
        return listDaftar;
    }

    public void setPdfAttribute(String judul, HttpServletResponse response) {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + judul + ".pdf");
    }

    public void cetakJudulTabel(PdfPTable table, List<String> judul) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.RED);
        cell.setHorizontalAlignment(1);
        Font font = FontFactory.getFont(FontFactory.TIMES_BOLD);
        font.setSize(12);
        font.setColor(BaseColor.BLACK);
        for (String str : judul) {
            cell.setPhrase(new Phrase(str, font));
            table.addCell(cell);
        }
    }

    public String formatUang(Double nominal) {
        DecimalFormat dec = new DecimalFormat("###,###,###.##");
        return dec.format(nominal);
    }

    public void cetakDetailTransaksi(PdfPTable tabel, List<DaftarTransaksi> listTrans) {
        PdfPCell cell = new PdfPCell();
        cell.setHorizontalAlignment(1);
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        font.setSize(12);
        font.setColor(BaseColor.BLACK);
        Font totalFont = FontFactory.getFont(FontFactory.TIMES_BOLD);
        totalFont.setSize(12);
        totalFont.setColor(BaseColor.BLACK);
        double total = 0.0;
        for (DaftarTransaksi trans : listTrans) {
            cell.setPhrase(new Phrase(String.valueOf(trans.getNo()), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase(trans.getNis(), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase(trans.getNama(), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase(trans.getKelas(), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase(trans.getJenis(), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase("Rp. " + formatUang(trans.getNominal()), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase(trans.getKeterangan(), font));
            tabel.addCell(cell);
            total += trans.getNominal();
        }
        cell.setPhrase(new Phrase("Total Transaksi Pembayaran " + DateUtil.konversiTanggal(listTrans.get(0).getTanggal()), totalFont));
        cell.setColspan(5);
        tabel.addCell(cell);
        cell.setPhrase(new Phrase("Rp. " + formatUang(total), totalFont));
        tabel.addCell(cell);
        cell.setPhrase(new Phrase("-", totalFont));
        tabel.addCell(cell);
    }

    public void cetakDetailTunggakan(PdfPTable tabel, List<DaftarTunggakan> listTunggakan) {
        PdfPCell cell = new PdfPCell();
        cell.setHorizontalAlignment(1);
        Font totalFont = FontFactory.getFont(FontFactory.TIMES_BOLD);
        totalFont.setSize(12);
        totalFont.setColor(BaseColor.BLACK);
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        font.setSize(12);
        font.setColor(BaseColor.BLACK);
        double totalSpp = 0.0;
        double totalPsb = 0.0;
        double subTotal = 0.0;
        for (DaftarTunggakan tunggak : listTunggakan) {
            cell.setPhrase(new Phrase(String.valueOf(tunggak.getNo()), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase(tunggak.getKeterangan(), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase("Rp. " + formatUang(tunggak.getSpp()), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase("Rp. " + formatUang(tunggak.getUangPangkal()), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase("Rp. " + formatUang(tunggak.getTotal()), font));
            tabel.addCell(cell);
            cell.setPhrase(new Phrase(tunggak.getWalas(), font));
            tabel.addCell(cell);
            totalSpp += tunggak.getSpp();
            totalPsb += tunggak.getUangPangkal();
            subTotal += tunggak.getTotal();
        }
        cell.setPhrase(new Phrase("#", totalFont));
        tabel.addCell(cell);
        cell.setPhrase(new Phrase("Total", totalFont));
        tabel.addCell(cell);
        cell.setPhrase(new Phrase("Rp. " + formatUang(totalSpp), totalFont));
        tabel.addCell(cell);
        cell.setPhrase(new Phrase("Rp. " + formatUang(totalPsb), totalFont));
        tabel.addCell(cell);
        cell.setPhrase(new Phrase("Rp. " + formatUang(subTotal), totalFont));
        tabel.addCell(cell);
        cell.setPhrase(new Phrase("-", totalFont));
        tabel.addCell(cell);
    }

    public void cetakPdf(HttpServletResponse response, UserLogin user, String jenis, List<DaftarTransaksi> trans, List<DaftarTunggakan> tunggak, String bulan) throws IOException, DocumentException {
        Document doc = new Document(PageSize.A4);
        PdfWriter.getInstance(doc, response.getOutputStream());
        doc.open();
        Font headerFont = FontFactory.getFont(FontFactory.TIMES_BOLD);
        headerFont.setSize(18);
        headerFont.setColor(BaseColor.BLACK);
        Font subHeaderFont = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        subHeaderFont.setSize(18);
        subHeaderFont.setColor(BaseColor.BLACK);
        Font subFont = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        subFont.setSize(14);
        subFont.setColor(BaseColor.BLACK);
        PdfPTable tabel;
        String jenjang = user.getJabatan().getAksesJenjang().equals("Semua") ? "Yayasan" : user.getJabatan().getAksesJenjang();
        if (jenis.equals(TRS)) {
            Paragraph judul1 = new Paragraph("Laporan Transaksi Pembayaran", headerFont);
            judul1.setAlignment(1);
            doc.add(judul1);
            Paragraph judul2 = new Paragraph("Unit " + jenjang + " Muhammadiyah 18 Jakarta", headerFont);
            judul2.setAlignment(1);
            doc.add(judul2);
            doc.add(Chunk.NEWLINE);
            Paragraph judul3 = new Paragraph("Tanggal : " + DateUtil.konversiTanggal(trans.get(0).getTanggal()), subFont);
            judul3.setAlignment(3);
            judul3.setSpacingAfter(15f);
            doc.add(judul3);
            float[] lebarKolom = {1.5f, 2f, 6f, 3f, 7f, 5f, 6f};
            tabel = new PdfPTable(lebarKolom);
            cetakJudulTabel(tabel, judulTabel(TRS));
            cetakDetailTransaksi(tabel, trans);
        } else {
            Paragraph judul1 = new Paragraph("Daftar Tunggakan", headerFont);
            judul1.setAlignment(1);
            doc.add(judul1);
            Paragraph judul2 = new Paragraph(jenjang + " Muhammadiyah 18 Jakarta", headerFont);
            judul2.setAlignment(1);
            doc.add(judul2);
            Paragraph judul3 = new Paragraph("Tahun Ajaran " + cariTahunAjaran(), subHeaderFont);
            judul3.setAlignment(1);
            doc.add(judul3);
            Paragraph judul4 = new Paragraph("(" + bulan + ")", subHeaderFont);
            judul4.setAlignment(1);
            doc.add(judul4);
            doc.add(Chunk.NEWLINE);
            float[] lebarKolom = {1f, 5f, 5f, 5f, 5f, 5f};
            tabel = new PdfPTable(lebarKolom);
            cetakJudulTabel(tabel, judulTabel("Tunggakan"));
            cetakDetailTunggakan(tabel, tunggak);
        }
        tabel.setHorizontalAlignment(3);
        tabel.setWidthPercentage(100f);
        doc.add(tabel);
        doc.addCreationDate();
        doc.addAuthor(user.getNama());
        doc.addTitle("Laporan " + jenis);
        doc.addCreator(user.getNama());
        doc.close();
    }

    public List<String> judulTabel(String jenis) {
        List<String> judul = new ArrayList<>();
        judul.add("No");
        if (jenis.equals(TRS)) {
            judul.add("NIS");
            judul.add("Nama");
            judul.add("Kelas");
            judul.add("Jenis Pembayaran");
            judul.add("Nominal");
            judul.add("Keterangan");
        } else {
            judul.add("Keterangan");
            judul.add("Tunggakan Uang SPP");
            judul.add("Tunggakan Uang Pangkal");
            judul.add("Total Tunggakan");
            judul.add("Wali Kelas");
        }
        return judul;
    }

    @Override
    public Response cariTransaksi(CariPembayaran trans, UserLogin user) throws ParseException {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", listTrans(trans, cariKelas(user)));
    }

    @Override
    public void cetakTransaksi(CariPembayaran trans, UserLogin user, HttpServletResponse response) throws DocumentException, IOException, ParseException {
        setPdfAttribute("Daftar Transaksi " + new SimpleDateFormat("yyyy-MM-dd").format(trans.getTanggal()), response);
        cetakPdf(response, user, TRS, listTrans(trans, cariKelas(user)), null, "-");
    }

    @Override
    public Response cariTunggakan(String bulan, UserLogin user) {
        return ResponseUtil.setResponse(HttpStatus.OK.value(), "", listTunggakan(bulan, cariKelas(user)));
    }

    @Override
    public void cetakTunggakan(String bulan, UserLogin user, HttpServletResponse response) throws DocumentException, IOException {
        setPdfAttribute("Daftar Tunggakan Bulan " + bulan, response);
        cetakPdf(response, user, "Tunggakan", null, listTunggakan(bulan, cariKelas(user)), bulan);
    }
}
