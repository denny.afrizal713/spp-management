package id.muhammadiyah.spp.service.implement;

import id.muhammadiyah.spp.data.entity.core.*;
import id.muhammadiyah.spp.data.entity.embed.DetailDuId;
import id.muhammadiyah.spp.data.model.request.CariSiswa;
import id.muhammadiyah.spp.data.model.request.InputSiswa;
import id.muhammadiyah.spp.data.model.response.DetailDaftarSiswa;
import id.muhammadiyah.spp.data.model.response.DropdownItem;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.data.model.response.UserAkses;
import id.muhammadiyah.spp.service.common.CommonService;
import id.muhammadiyah.spp.service.core.SiswaService;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.variable.constant.BulanConstant;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import id.muhammadiyah.spp.variable.enumeration.StatusEnum;
import id.muhammadiyah.spp.variable.enumeration.TipeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class SiswaServiceImplement extends CommonService implements SiswaService {

    private static final String MSG = " Data Siswa Dengan NIS ";

    public Double hitungJumlah(Siswa siswa) {
        Double total = 0.0;
        try {
            total += siswa.getTransaksi().getUpSisa();
            total += siswa.getTransaksi().getSppTlSisa();
            total += siswa.getTransaksi().getDuSisa();
            total += siswa.getSppTahunBerjalan().getSppJuli();
            total += siswa.getSppTahunBerjalan().getSppAgustus();
            total += siswa.getSppTahunBerjalan().getSppSeptember();
            total += siswa.getSppTahunBerjalan().getSppOktober();
            total += siswa.getSppTahunBerjalan().getSppNovember();
            total += siswa.getSppTahunBerjalan().getSppDesember();
            total += siswa.getSppTahunBerjalan().getSppJanuari();
            total += siswa.getSppTahunBerjalan().getSppFebruari();
            total += siswa.getSppTahunBerjalan().getSppMaret();
            total += siswa.getSppTahunBerjalan().getSppApril();
            total += siswa.getSppTahunBerjalan().getSppMei();
            total += siswa.getSppTahunBerjalan().getSppJuni();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            total = 0.0;
        }
        return total;
    }

    public DetailSppTb ubahDataSppTb(DetailSppTb tb) {
        String bulan = cariBulan();
        switch (bulan) {
            case BulanConstant.JUL:
                tb.setSppJuli(0.0);
                tb.setSppAgustus(0.0);
                tb.setSppSeptember(0.0);
                tb.setSppOktober(0.0);
                tb.setSppNovember(0.0);
                tb.setSppDesember(0.0);
                tb.setSppJanuari(0.0);
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.AUG:
                tb.setSppAgustus(0.0);
                tb.setSppSeptember(0.0);
                tb.setSppOktober(0.0);
                tb.setSppNovember(0.0);
                tb.setSppDesember(0.0);
                tb.setSppJanuari(0.0);
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.SEP:
                tb.setSppSeptember(0.0);
                tb.setSppOktober(0.0);
                tb.setSppNovember(0.0);
                tb.setSppDesember(0.0);
                tb.setSppJanuari(0.0);
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.OCT:
                tb.setSppOktober(0.0);
                tb.setSppNovember(0.0);
                tb.setSppDesember(0.0);
                tb.setSppJanuari(0.0);
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.NOV:
                tb.setSppNovember(0.0);
                tb.setSppDesember(0.0);
                tb.setSppJanuari(0.0);
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.DEC:
                tb.setSppDesember(0.0);
                tb.setSppJanuari(0.0);
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.JAN:
                tb.setSppJanuari(0.0);
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.FEB:
                tb.setSppFebruari(0.0);
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.MAR:
                tb.setSppMaret(0.0);
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.APR:
                tb.setSppApril(0.0);
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.MAY:
                tb.setSppMei(0.0);
                tb.setSppJuni(0.0);
                break;
            case BulanConstant.JUN:
                tb.setSppJuni(0.0);
                break;
        }
        return tb;
    }

    @Override
    public List<DetailDaftarSiswa> daftarSiswa(CariSiswa cariSiswa, List<UserAkses> akses) {
        Specification<Siswa> specs;
        specs = (root, cq, cb) -> {
            List<String> kelas = new ArrayList<>();
            if (cariSiswa.getKelas().equals("") || cariSiswa.getKelas() == null) {
                for (UserAkses aks : akses) {
                    if (aks.getParent() == 2) {
                        kelas.add(aks.getNama());
                    }
                }
            } else {
                kelas.add(cariSiswa.getKelas());
            }
            List<String> tahun = new ArrayList<>();
            if (cariSiswa.getTahunAjaran().equals("") || cariSiswa.getTahunAjaran() == null) {
                tahun = Arrays.asList(daftarTahunAjaran());
            }
            List<Predicate> predicates = new ArrayList<>();
            if (cariSiswa.getNis() != null) {
                predicates.add(cb.like(root.get("nis"), "%" + cariSiswa.getNis() + "%"));
            }
            if (cariSiswa.getNama() != null) {
                predicates.add(cb.like(root.get("nama"), "%" + cariSiswa.getNama() + "%"));
            }
            predicates.add(root.get("kelas").in(kelas));
            predicates.add(root.get("tahunAjaran").in(tahun));
            predicates.add(cb.equal(root.get("status"), StatusEnum.AKTIF.getLabel()));
            Predicate[] preds = new Predicate[predicates.size()];
            return cb.and(predicates.toArray(preds));
        };
        List<Siswa> hasil = siswaRepository.findAll(specs);
        List<DetailDaftarSiswa> hasilSiswa = new ArrayList<>();
        hasil.forEach(x -> {
            int no = hasilSiswa.size();
            no++;
            Double jumlah = hitungJumlah(x);
            hasilSiswa.add(new DetailDaftarSiswa(
                    no,
                    x.getNis(),
                    x.getNama(),
                    x.getKelas(),
                    x.getTahunAjaran(),
                    x.getTransaksi().getUpSisa(),
                    x.getTransaksi().getSppTlSisa(),
                    x.getTransaksi().getDuSisa(),
                    x.getSppTahunBerjalan().getSppJuli(),
                    x.getSppTahunBerjalan().getSppAgustus(),
                    x.getSppTahunBerjalan().getSppSeptember(),
                    x.getSppTahunBerjalan().getSppOktober(),
                    x.getSppTahunBerjalan().getSppNovember(),
                    x.getSppTahunBerjalan().getSppDesember(),
                    x.getSppTahunBerjalan().getSppJanuari(),
                    x.getSppTahunBerjalan().getSppFebruari(),
                    x.getSppTahunBerjalan().getSppMaret(),
                    x.getSppTahunBerjalan().getSppApril(),
                    x.getSppTahunBerjalan().getSppMei(),
                    x.getSppTahunBerjalan().getSppJuni(),
                    jumlah));
        });
        return hasilSiswa;
    }

    @Override
    public Siswa detailSiswa(String nis, String jenjang) {
        return cariBerdasarkanNisdanJenjang(nis, jenjang);
    }

    @Override
    public List<DropdownItem> cariDaftarTahunAjaran() {
        return generateDropdown(daftarTahunAjaran());
    }

    @Override
    public Response simpanDataSiswa(InputSiswa siswa) {
        try {
            String nis = siswa.getNis();
            String jenjang = cariJenjang(siswa.getKelas());
            Siswa exist = cariBerdasarkanNisdanJenjang(nis, jenjang);
            if (exist != null) {
                return ResponseUtil.setResponse(
                        HttpStatus.FORBIDDEN.value(),
                        "Nis " + nis + " Sudah Terdaftar Atas Nama " + exist.getNama(),
                        null);
            }
            if (nis.equals("-")) {
                nis = generateNis(jenjang);
            }
            Siswa murid = new Siswa();
            Long idSiswa = idTerakhir(TipeEnum.SISWA, nis);
            idSiswa++;
            murid.setIdSiswa(idSiswa);
            murid.setNis(siswa.getNis());
            murid.setKelas(siswa.getKelas());
            murid.setEmail(siswa.getEmail());
            murid.setNama(siswa.getNama());
            murid.setJenisKelamin(siswa.getJenisKelamin());
            murid.setAlamat("-");
            murid.setTempatLahir("-");
            murid.setTanggalLahir(null);
            murid.setTahunAjaran(siswa.getTahunAjaran());
            murid.setTahunMasuk(siswa.getTahunAjaran());
            murid.setJenjang(jenjang);
            murid.setStatus(StatusEnum.AKTIF.getLabel());
            murid.setDibuatOleh(siswa.getUserCreate());
            murid.setTanggalBuat(siswa.getDateCreate());
            murid.setDiubahOleh(siswa.getUserUpdate());
            murid.setTanggalUbah(siswa.getDateUpdate());
            Long idTrans = idTerakhir(TipeEnum.TRANS, nis);
            Transaksi trans = new Transaksi();
            idTrans++;
            murid.setIdTrans(idTrans);
            trans.setIdTransaksi(idTrans);
            trans.setNis(nis);
            trans.setSppTbTotal(siswa.getTotal());
            trans.setSppTbBayar(0.0);
            trans.setSppTbSisa(siswa.getTotal());
            trans.setSppTbLebih(0.0);
            trans.setSppTlTotal(0.0);
            trans.setSppTlBayar(0.0);
            trans.setSppTlSisa(0.0);
            trans.setSppTlLebih(0.0);
            trans.setUpTotal(siswa.getUangPangkal());
            trans.setUpBayar(0.0);
            trans.setUpSisa(siswa.getUangPangkal());
            trans.setUpLebih(0.0);
            trans.setDuTotal(0.0);
            trans.setDuBayar(0.0);
            trans.setDuSisa(0.0);
            trans.setDuLebih(0.0);
            trans.setDibuatOleh(siswa.getUserCreate());
            trans.setTanggalBuat(siswa.getDateCreate());
            trans.setDiubahOleh(siswa.getUserUpdate());
            trans.setTanggalUbah(siswa.getDateUpdate());
            Long idUp = idTerakhir(TipeEnum.UP, nis);
            DetailUp psb = new DetailUp();
            idUp++;
            murid.setIdUp(idUp);
            psb.setIdUp(idUp);
            psb.setNis(nis);
            psb.setJumlahUp(siswa.getUangPangkal());
            psb.setDibuatOleh(siswa.getUserCreate());
            psb.setTanggalBuat(siswa.getDateCreate());
            psb.setDiubahOleh(siswa.getUserUpdate());
            psb.setTanggalUbah(siswa.getDateUpdate());
            Long idDu = idTerakhir(TipeEnum.DU, nis);
            idDu++;
            DetailDuId duId = new DetailDuId();
            duId.setNis(nis);
            duId.setIdDu(idDu);
            DetailDu du = new DetailDu();
            du.setId(duId);
            du.setJumlahDu(0.0);
            du.setTahunAjaran(siswa.getTahunAjaran());
            du.setDibuatOleh(siswa.getUserCreate());
            du.setTanggalBuat(siswa.getDateCreate());
            du.setDiubahOleh(siswa.getUserUpdate());
            du.setTanggalUbah(siswa.getDateUpdate());
            Long idSpp = idTerakhir(TipeEnum.SPPTB, nis);
            DetailSppTb spp = new DetailSppTb();
            idSpp++;
            murid.setIdSpp(idSpp);
            spp.setIdSppTb(idSpp);
            spp.setNis(nis);
            spp.setTahunAjaran(siswa.getTahunAjaran());
            spp.setSppTl(0.0);
            spp.setSppJuli(siswa.getJuli());
            spp.setSppAgustus(siswa.getAgustus());
            spp.setSppSeptember(siswa.getSeptember());
            spp.setSppOktober(siswa.getOktober());
            spp.setSppNovember(siswa.getNovember());
            spp.setSppDesember(siswa.getDesember());
            spp.setSppJanuari(siswa.getJanuari());
            spp.setSppFebruari(siswa.getFebruari());
            spp.setSppMaret(siswa.getMaret());
            spp.setSppApril(siswa.getApril());
            spp.setSppMei(siswa.getMei());
            spp.setSppJuni(siswa.getJuni());
            spp.setDibuatOleh(siswa.getUserCreate());
            spp.setTanggalBuat(siswa.getDateCreate());
            spp.setDiubahOleh(siswa.getUserUpdate());
            spp.setTanggalUbah(siswa.getDateUpdate());
            siswaRepository.save(murid);
            transaksiRepository.save(trans);
            detailUpRepository.save(psb);
            detailDuRepository.save(du);
            detailSppTbRepository.save(spp);
            List<DetailDu> duList = new ArrayList<>();
            duList.add(du);
            murid.setTransaksi(trans);
            murid.setUangPangkal(psb);
            murid.setSppTahunBerjalan(spp);
            murid.setDaftarUlang(duList);
            murid.setHistorySiswa(new ArrayList<>());
            murid.setPembayaran(new ArrayList<>());
            murid.setSppTahunLalu(new ArrayList<>());
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PesanConstant.BERHASIL_SIMPAN + MSG + nis, murid);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    PesanConstant.GAGAL_SIMPAN + " : " + e.getMessage(), null);
        }
    }

    @Override
    public Response ubahDataSiswa(InputSiswa siswa) {
        try {
            String nis = siswa.getNis();
            Siswa murid = cariBerdasarkanNisdanJenjang(nis, cariJenjang(siswa.getKelas()));
            if (murid == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            DetailSppTb spp = murid.getSppTahunBerjalan();
            spp.setSppJuli(siswa.getJuli());
            spp.setSppAgustus(siswa.getAgustus());
            spp.setSppSeptember(siswa.getSeptember());
            spp.setSppOktober(siswa.getOktober());
            spp.setSppNovember(siswa.getNovember());
            spp.setSppDesember(siswa.getDesember());
            spp.setSppJanuari(siswa.getJanuari());
            spp.setSppFebruari(siswa.getFebruari());
            spp.setSppMaret(siswa.getMaret());
            spp.setSppApril(siswa.getApril());
            spp.setSppMei(siswa.getMei());
            spp.setSppJuni(siswa.getJuni());
            spp.setDiubahOleh(siswa.getUserUpdate());
            spp.setTanggalUbah(siswa.getDateUpdate());
            DetailUp up = murid.getUangPangkal();
            up.setJumlahUp(siswa.getUangPangkal());
            up.setDiubahOleh(siswa.getUserUpdate());
            up.setTanggalUbah(siswa.getDateUpdate());
            DetailDu du = new DetailDu();
            try {
                du = murid.getDaftarUlang().get(0);
            } catch (Exception e) {
                du = detailDuRepository.findFirstByIdNisOrderByIdIdDuDesc(siswa.getNis());
            }
            du.setJumlahDu(siswa.getDaftarUlang());
            du.setDiubahOleh(siswa.getUserUpdate());
            du.setTanggalUbah(siswa.getDateUpdate());
            Transaksi trans = murid.getTransaksi();
            trans.setSppTbTotal(siswa.getTotal());
            trans.setSppTbSisa(siswa.getTotal());
            trans.setUpTotal(siswa.getUangPangkal());
            trans.setUpSisa(siswa.getUangPangkal());
            trans.setSppTlTotal(siswa.getSppTl());
            trans.setSppTlSisa(siswa.getSppTl());
            trans.setDiubahOleh(siswa.getUserUpdate());
            trans.setTanggalUbah(siswa.getDateUpdate());
            detailSppTbRepository.save(spp);
            transaksiRepository.save(trans);
            detailUpRepository.save(up);
            detailDuRepository.save(du);
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PesanConstant.BERHASIL_UBAH + MSG + nis, murid);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    PesanConstant.GAGAL_UBAH + " : " + e.getMessage(), null);
        }
    }

    @Override
    public Response hapusDataSiswa(String nis, String jenjang, String user) {
        try {
            Siswa siswa = cariBerdasarkanNisdanJenjang(nis, jenjang);
            if (siswa == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            siswa.setStatus(StatusEnum.DEL.getLabel());
            DetailSppTb tb = ubahDataSppTb(siswa.getSppTahunBerjalan());
            siswaRepository.save(siswa);
            detailSppTbRepository.save(tb);
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PesanConstant.BERHASIL_HAPUS + MSG + nis, siswa);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    PesanConstant.GAGAL_HAPUS + " : " + e.getMessage(), null);
        }
    }
}
