package id.muhammadiyah.spp.service.implement;

import id.muhammadiyah.spp.data.entity.core.*;
import id.muhammadiyah.spp.data.entity.embed.PembayaranId;
import id.muhammadiyah.spp.data.model.request.Penyesuaian;
import id.muhammadiyah.spp.data.model.request.TransBayar;
import id.muhammadiyah.spp.data.model.response.DetailPengguna;
import id.muhammadiyah.spp.data.model.response.Response;
import id.muhammadiyah.spp.service.common.CommonService;
import id.muhammadiyah.spp.service.core.TransaksiService;
import id.muhammadiyah.spp.util.DateUtil;
import id.muhammadiyah.spp.util.MailUtil;
import id.muhammadiyah.spp.util.ResponseUtil;
import id.muhammadiyah.spp.variable.constant.BulanConstant;
import id.muhammadiyah.spp.variable.constant.JenisConstant;
import id.muhammadiyah.spp.variable.constant.PesanConstant;
import id.muhammadiyah.spp.variable.constant.StringConstant;
import id.muhammadiyah.spp.variable.enumeration.BulanEnum;
import id.muhammadiyah.spp.variable.enumeration.JenisPembayaranEnum;
import id.muhammadiyah.spp.variable.enumeration.TipeEnum;
import id.muhammadiyah.spp.variable.enumeration.TipePembayaranEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class TransaksiServiceImplement extends CommonService implements TransaksiService {

    private static final String PSN = "Pembayaran Berhasil Disimpan";
    private static final String ERR = "Tujuan Penyesuaian Tidak Valid";

    public Double hitungTotalSpp(DetailSppTl tl) {
        try {
            Double total = 0.0;
            total += tl.getSppJuli();
            total += tl.getSppAgustus();
            total += tl.getSppSeptember();
            total += tl.getSppOktober();
            total += tl.getSppNovember();
            total += tl.getSppDesember();
            total += tl.getSppJanuari();
            total += tl.getSppFebruari();
            total += tl.getSppMaret();
            total += tl.getSppApril();
            total += tl.getSppMei();
            total += tl.getSppJuni();
            return total;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return 0.0;
        }
    }

    public Double cariNominalSpp(DetailSppTb spp, String bulan) {
        double biaya;
        switch (bulan) {
            case BulanConstant.JUL:
                biaya = spp.getSppJuli();
                break;
            case BulanConstant.AUG:
                biaya = spp.getSppAgustus();
                break;
            case BulanConstant.SEP:
                biaya = spp.getSppSeptember();
                break;
            case BulanConstant.OCT:
                biaya = spp.getSppOktober();
                break;
            case BulanConstant.NOV:
                biaya = spp.getSppNovember();
                break;
            case BulanConstant.DEC:
                biaya = spp.getSppDesember();
                break;
            case BulanConstant.JAN:
                biaya = spp.getSppJanuari();
                break;
            case BulanConstant.FEB:
                biaya = spp.getSppFebruari();
                break;
            case BulanConstant.MAR:
                biaya = spp.getSppMaret();
                break;
            case BulanConstant.APR:
                biaya = spp.getSppApril();
                break;
            case BulanConstant.MAY:
                biaya = spp.getSppMei();
                break;
            case BulanConstant.JUN:
                biaya = spp.getSppJuni();
                break;
            default:
                biaya = 0.0;
                break;
        }
        return biaya;
    }

    public DetailSppTb setNominal(DetailSppTb tb, String bulan, Double sppBayar) {
        switch (bulan) {
            case BulanConstant.JUL:
                tb.setSppJuli(sppBayar);
                break;
            case BulanConstant.AUG:
                tb.setSppAgustus(sppBayar);
                break;
            case BulanConstant.SEP:
                tb.setSppSeptember(sppBayar);
                break;
            case BulanConstant.OCT:
                tb.setSppOktober(sppBayar);
                break;
            case BulanConstant.NOV:
                tb.setSppNovember(sppBayar);
                break;
            case BulanConstant.DEC:
                tb.setSppDesember(sppBayar);
                break;
            case BulanConstant.JAN:
                tb.setSppJanuari(sppBayar);
                break;
            case BulanConstant.FEB:
                tb.setSppFebruari(sppBayar);
                break;
            case BulanConstant.MAR:
                tb.setSppMaret(sppBayar);
                break;
            case BulanConstant.APR:
                tb.setSppApril(sppBayar);
                break;
            case BulanConstant.MAY:
                tb.setSppMei(sppBayar);
                break;
            case BulanConstant.JUN:
                tb.setSppJuni(sppBayar);
                break;
        }
        return tb;
    }

    public Pembayaran setSppBayar(TransBayar trans, Double jumlahBayar, String noSebelumnya, String bulan, long id) {
        Pembayaran balanceTb = new Pembayaran();
        PembayaranId balanceTbId = new PembayaranId();
        if (id == 0) {
            id = idTerakhir(TipeEnum.PAY, trans.getNis());
        }
        id++;
        balanceTbId.setIdBayar(id);
        balanceTbId.setNis(trans.getNis());
        balanceTb.setId(balanceTbId);
        balanceTb.setNoBayar(generateNoBayar(trans.getNis(), TipeEnum.SPPTB, noSebelumnya));
        balanceTb.setJenisPembayaran(JenisPembayaranEnum.TB.getLabel());
        balanceTb.setBulan(bulan);
        if (bulan == null || bulan.equals("")) {
            balanceTb.setBulan(trans.getBulan());
        }
        balanceTb.setTipeBayar(TipePembayaranEnum.NORMAL.getLabel());
        balanceTb.setJumlahBayar(jumlahBayar);
        balanceTb.setDibuatOleh(trans.getUserCreate());
        balanceTb.setTanggalBuat(trans.getDateCreate());
        balanceTb.setDiubahOleh(trans.getUserUpdate());
        balanceTb.setTanggalUbah(trans.getDateUpdate());
        return balanceTb;
    }

    public Double hitungTunggakahSppTb(DetailSppTb tb) {
        double tunggakan;
        String bln = new SimpleDateFormat("MM").format(new Date());
        switch (bln) {
            case "07":
                tunggakan = tb.getSppJuli();
                break;
            case "08":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus();
                break;
            case "09":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember();
                break;
            case "10":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober();
                break;
            case "11":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember();
                break;
            case "12":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember();
                break;
            case "01":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari();
                break;
            case "02":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari();
                break;
            case "03":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret();
                break;
            case "04":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret() + tb.getSppApril();
                break;
            case "05":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret() + tb.getSppApril()
                        + tb.getSppMei();
                break;
            case "06":
                tunggakan = tb.getSppJuli() + tb.getSppAgustus() + tb.getSppSeptember() + tb.getSppOktober() + tb.getSppNovember()
                        + tb.getSppDesember() + tb.getSppJanuari() + tb.getSppFebruari() + tb.getSppMaret() + tb.getSppApril()
                        + tb.getSppMei() + tb.getSppJuni();
                break;
            default:
                tunggakan = 0.0;
                break;
        }
        return tunggakan;
    }

    public Pembayaran setPembayaranAdjustment(Penyesuaian adjust, Double bayar, String prevNo, Long id, TipeEnum jenis, String bulan) {
        Pembayaran adj = new Pembayaran();
        PembayaranId adjId = new PembayaranId();
        if (id == 0) {
            id = idTerakhir(TipeEnum.PAY, adjust.getNis());
        }
        id++;
        adjId.setNis(adjust.getNis());
        adjId.setIdBayar(id);
        adj.setId(adjId);
        adj.setJenisPembayaran(jenis.getLabel());
        adj.setNoBayar(generateNoBayar(adjust.getNis(), jenis, prevNo));
        adj.setJumlahBayar(bayar);
        adj.setTipeBayar(TipePembayaranEnum.ADJUST.getLabel());
        adj.setBulan(bulan);
        if (bulan == null || bulan.equals("")) {
            adj.setBulan(cariBulan());
        }
        adj.setDibuatOleh(adjust.getUserCreate());
        adj.setTanggalBuat(adjust.getDateCreate());
        adj.setDiubahOleh(adjust.getUserUpdate());
        adj.setTanggalUbah(adjust.getDateUpdate());
        return adj;
    }

    public Siswa upAdjust(Siswa siswa, Penyesuaian adjust, String from) {
        List<Pembayaran> upBayar = new ArrayList<>();
        Transaksi trans = siswa.getTransaksi();
        DetailUp up = siswa.getUangPangkal();
        trans.setUpBayar(trans.getUpBayar() + adjust.getBayar());
        if (adjust.getBayar() < trans.getUpSisa()) {
            trans.setUpSisa(trans.getUpSisa() - adjust.getBayar());
            up.setJumlahUp(trans.getUpSisa());
            switch (from) {
                case JenisConstant.DU:
                    trans.setDuLebih(0.0);
                    break;
                case JenisConstant.TB:
                    trans.setSppTbLebih(0.0);
                    break;
                case JenisConstant.TL:
                    trans.setSppTlLebih(0.0);
                    break;
            }
            upBayar.add(setPembayaranAdjustment(adjust, adjust.getBayar(), "", 0L, TipeEnum.UP, ""));
        } else {
            switch (from) {
                case JenisConstant.DU:
                    trans.setDuLebih(adjust.getBayar() - trans.getUpSisa());
                    break;
                case JenisConstant.TB:
                    trans.setSppTbLebih(adjust.getBayar() - trans.getUpSisa());
                    break;
                case JenisConstant.TL:
                    trans.setSppTlLebih(adjust.getBayar() - trans.getUpSisa());
                    break;
            }
            upBayar.add(setPembayaranAdjustment(adjust, trans.getUpSisa(), "", 0L, TipeEnum.UP, ""));
            trans.setUpSisa(0.0);
            up.setJumlahUp(0.0);
        }
        siswa.setPembayaran(upBayar);
        return siswa;
    }

    public Siswa duAdjust(Siswa siswa, Penyesuaian adjust, String from) {
        Long id = 0L;
        String noTrans = "";
        Transaksi trans = siswa.getTransaksi();
        List<DetailDu> du = siswa.getDaftarUlang();
        List<Pembayaran> duBayar = new ArrayList<>();
        Double bayarDu = adjust.getBayar();
        trans.setDuBayar(trans.getDuBayar() + bayarDu);
        if (adjust.getBayar() < trans.getDuSisa()) {
            trans.setDuSisa(trans.getDuSisa() - adjust.getBayar());
            switch (from) {
                case JenisConstant.UP:
                    trans.setUpLebih(0.0);
                    break;
                case JenisConstant.TB:
                    trans.setSppTbLebih(0.0);
                    break;
                case JenisConstant.TL:
                    trans.setSppTlLebih(0.0);
                    break;
            }
            boolean habis = false;
            int idx = 0;
            do {
                if (bayarDu < du.get(idx).getJumlahDu()) {
                    Pembayaran pay = setPembayaranAdjustment(adjust, bayarDu, noTrans, id, TipeEnum.DU, "");
                    du.get(idx).setJumlahDu(du.get(idx).getJumlahDu() - bayarDu);
                    duBayar.add(pay);
                    habis = true;
                } else {
                    Pembayaran pay = setPembayaranAdjustment(adjust, du.get(idx).getJumlahDu(), noTrans, id, TipeEnum.DU, "");
                    bayarDu = bayarDu - du.get(idx).getJumlahDu();
                    id = pay.getId().getIdBayar();
                    noTrans = pay.getNoBayar();
                    du.get(idx).setJumlahDu(0.0);
                    duBayar.add(pay);
                }
                idx++;
            } while (!habis);
        } else {
            switch (from) {
                case JenisConstant.UP:
                    trans.setUpLebih(adjust.getBayar() - trans.getDuSisa());
                    break;
                case JenisConstant.TB:
                    trans.setSppTbLebih(adjust.getBayar() - trans.getDuSisa());
                    break;
                case JenisConstant.TL:
                    trans.setSppTlLebih(adjust.getBayar() - trans.getDuSisa());
                    break;
            }
            trans.setDuSisa(0.0);
            for (DetailDu x : du) {
                if (x.getJumlahDu() > 0.0) {
                    Pembayaran pay = setPembayaranAdjustment(adjust, x.getJumlahDu(), noTrans, id, TipeEnum.DU, "");
                    x.setJumlahDu(0.0);
                    id = pay.getId().getIdBayar();
                    noTrans = pay.getNoBayar();
                    duBayar.add(pay);
                }
            }
        }
        siswa.setPembayaran(duBayar);
        return siswa;
    }

    public Siswa tbAdjust(Siswa siswa, Penyesuaian adjust, String from) {
        List<Pembayaran> tbBayar = new ArrayList<>();
        Transaksi trans = siswa.getTransaksi();
        DetailSppTb tb = siswa.getSppTahunBerjalan();
        Double tunggakan = hitungTunggakahSppTb(tb);
        trans.setSppTbSisa(trans.getSppTbSisa() - tunggakan);
        Double bayarTb = adjust.getBayar();
        if (adjust.getBayar() < tunggakan) {
            Double kurang = tunggakan - adjust.getBayar();
            kurang += trans.getSppTbSisa();
            trans.setSppTbSisa(kurang);
            switch (from) {
                case JenisConstant.UP:
                    trans.setUpLebih(0.0);
                    break;
                case JenisConstant.DU:
                    trans.setDuLebih(0.0);
                    break;
                case JenisConstant.TL:
                    trans.setSppTlLebih(0.0);
                    break;
            }
        } else {
            bayarTb = tunggakan;
            switch (from) {
                case JenisConstant.UP:
                    trans.setUpLebih(adjust.getBayar() - tunggakan);
                    break;
                case JenisConstant.DU:
                    trans.setDuLebih(adjust.getBayar() - tunggakan);
                    break;
                case JenisConstant.TL:
                    trans.setSppTlLebih(adjust.getBayar() - tunggakan);
                    break;
            }
        }
        trans.setSppTbBayar(trans.getSppTbBayar() + bayarTb);
        int idx = 0;
        boolean habis = false;
        Double bayar;
        String noTrans = "";
        Long id = 0L;
        double byr;
        do {
            idx++;
            switch (idx) {
                case 1:
                    bayar = tb.getSppJuli();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppJuli(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppJuli(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.JUL);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 2:
                    bayar = tb.getSppAgustus();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppAgustus(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppAgustus(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.AUG);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 3:
                    bayar = tb.getSppSeptember();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppSeptember(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppSeptember(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.SEP);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 4:
                    bayar = tb.getSppOktober();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppOktober(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppOktober(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.OCT);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 5:
                    bayar = tb.getSppNovember();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppNovember(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppNovember(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.NOV);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 6:
                    bayar = tb.getSppDesember();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppDesember(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppDesember(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.DEC);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 7:
                    bayar = tb.getSppJanuari();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppJanuari(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppJanuari(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.JAN);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 8:
                    bayar = tb.getSppFebruari();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppFebruari(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppFebruari(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.FEB);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 9:
                    bayar = tb.getSppMaret();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppMaret(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppMaret(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.MAR);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 10:
                    bayar = tb.getSppApril();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppApril(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppApril(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.APR);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 11:
                    bayar = tb.getSppMei();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppMei(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppMei(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.MAY);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                case 12:
                    bayar = tb.getSppJuni();
                    if (bayar > 0.0) {
                        if (bayar > bayarTb) {
                            tb.setSppJuni(bayar - bayarTb);
                            byr = bayarTb;
                            habis = true;
                        } else {
                            tb.setSppJuni(0.0);
                            byr = bayar;
                        }
                        Pembayaran tbByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTB, BulanConstant.JUN);
                        id = tbByr.getId().getIdBayar();
                        noTrans = tbByr.getNoBayar();
                        tbBayar.add(tbByr);
                    }
                    break;
                default:
                    bayar = 0.0;
                    break;
            }
            bayarTb = bayarTb - bayar;
            if (bayarTb <= 0.0) {
                habis = true;
            }
        } while (!habis);
        siswa.setPembayaran(tbBayar);
        return siswa;
    }

    public Siswa tlAdjust(Siswa siswa, Penyesuaian adjust, String from) {
        Transaksi trans = siswa.getTransaksi();
        List<Pembayaran> tlBayar = new ArrayList<>();
        Double bayarTl = adjust.getBayar();
        if (adjust.getBayar() < trans.getSppTlSisa()) {
            trans.setSppTlSisa(trans.getSppTlSisa() - adjust.getBayar());
            switch (from) {
                case JenisConstant.UP:
                    trans.setUpLebih(0.0);
                    break;
                case JenisConstant.DU:
                    trans.setDuLebih(0.0);
                    break;
                case JenisConstant.TB:
                    trans.setSppTbLebih(0.0);
                    break;
            }
            trans.setSppTlBayar(trans.getSppTlBayar() + bayarTl);
        } else {
            switch (from) {
                case JenisConstant.UP:
                    trans.setUpLebih(adjust.getBayar() - trans.getSppTlSisa());
                    break;
                case JenisConstant.DU:
                    trans.setDuLebih(adjust.getBayar() - trans.getSppTlSisa());
                    break;
                case JenisConstant.TB:
                    trans.setSppTbLebih(adjust.getBayar() - trans.getSppTlSisa());
                    break;
            }
            trans.setSppTlSisa(0.0);
            trans.setSppTlBayar(trans.getSppTlBayar() + trans.getSppTlSisa());
        }
        List<DetailSppTl> tls = siswa.getSppTahunLalu();
        int idx;
        long id = 0L;
        String noTrans = "";
        boolean habis = false;
        boolean komplit;
        double bayar;
        double byr;
        for (DetailSppTl tl : tls) {
            komplit = false;
            if (!habis) {
                idx = 0;
                do {
                    idx++;
                    switch (idx) {
                        case 1:
                            bayar = tl.getSppJuli();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppJuli(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppJuli(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.JUL);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 2:
                            bayar = tl.getSppAgustus();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppAgustus(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppAgustus(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.AUG);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 3:
                            bayar = tl.getSppSeptember();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppSeptember(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppSeptember(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.SEP);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 4:
                            bayar = tl.getSppOktober();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppOktober(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppOktober(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.OCT);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 5:
                            bayar = tl.getSppNovember();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppNovember(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppNovember(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.NOV);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 6:
                            bayar = tl.getSppDesember();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppDesember(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppDesember(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.DEC);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 7:
                            bayar = tl.getSppJanuari();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppJanuari(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppJanuari(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.JAN);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 8:
                            bayar = tl.getSppFebruari();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppFebruari(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppFebruari(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.FEB);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 9:
                            bayar = tl.getSppMaret();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppMaret(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppMaret(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.MAR);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 10:
                            bayar = tl.getSppApril();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppApril(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppApril(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.APR);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 11:
                            bayar = tl.getSppMei();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppMei(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppMei(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.MAY);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            break;
                        case 12:
                            bayar = tl.getSppJuni();
                            if (bayar > 0.0) {
                                if (bayar > bayarTl) {
                                    tl.setSppJuni(bayar - bayarTl);
                                    byr = bayarTl;
                                    habis = true;
                                } else {
                                    tl.setSppJuni(0.0);
                                    byr = bayar;
                                }
                                Pembayaran tlByr = setPembayaranAdjustment(adjust, byr, noTrans, id, TipeEnum.SPPTL, BulanConstant.JUN);
                                id = tlByr.getId().getIdBayar();
                                noTrans = tlByr.getNoBayar();
                                tlBayar.add(tlByr);
                            }
                            komplit = true;
                            break;
                        default:
                            bayar = 0.0;
                            break;
                    }
                    bayarTl = bayarTl - bayar;
                    if (bayarTl <= 0.0) {
                        habis = true;
                    }
                } while (!habis && !komplit);
            }
        }
        siswa.setPembayaran(tlBayar);
        return siswa;
    }

    @Override
    public Response bayarUangPangkal(TransBayar trans) {
        try {
            String nis = trans.getNis();
            Siswa siswa = cariBerdasarkanNisdanJenjang(nis, trans.getJenjang());
            if (siswa == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            Transaksi upTrans = siswa.getTransaksi();
            Double upSisa = upTrans.getUpSisa() - trans.getJumlahBayar();
            Double upBayar = upTrans.getUpBayar() + trans.getJumlahBayar();
            upTrans.setUpBayar(upBayar);
            upTrans.setUpSisa(upSisa);
            if (upSisa < 0) {
                String lebih = String.valueOf(upSisa).replace(StringConstant.STRIP, "");
                upTrans.setUpLebih(Double.parseDouble(lebih));
                upTrans.setUpSisa(0.0);
                upSisa = 0.0;
            }
            upTrans.setDiubahOleh(trans.getUserUpdate());
            upTrans.setTanggalUbah(trans.getDateUpdate());
            DetailUp up = siswa.getUangPangkal();
            up.setJumlahUp(upSisa);
            up.setDiubahOleh(trans.getUserUpdate());
            up.setTanggalUbah(trans.getDateUpdate());
            Pembayaran bayarUp = new Pembayaran();
            PembayaranId bayarId = new PembayaranId();
            Long idBayar = idTerakhir(TipeEnum.PAY, nis);
            idBayar++;
            bayarId.setIdBayar(idBayar);
            bayarId.setNis(nis);
            bayarUp.setId(bayarId);
            bayarUp.setNoBayar(generateNoBayar(nis, TipeEnum.UP, ""));
            bayarUp.setJenisPembayaran(JenisPembayaranEnum.UP.getLabel());
            bayarUp.setBulan(StringConstant.STRIP);
            bayarUp.setTipeBayar(TipePembayaranEnum.NORMAL.getLabel());
            bayarUp.setJumlahBayar(trans.getJumlahBayar());
            bayarUp.setDibuatOleh(trans.getUserCreate());
            bayarUp.setDiubahOleh(trans.getUserUpdate());
            bayarUp.setTanggalBuat(trans.getDateCreate());
            bayarUp.setTanggalUbah(trans.getDateUpdate());
            transaksiRepository.save(upTrans);
            detailUpRepository.save(up);
            pembayaranRepository.save(bayarUp);
            sendMail(
                    siswa.getEmail(),
                    MailUtil.pembayaran("Uang Pangkal", siswa.getNama()),
                    MailUtil.transBody(
                            siswa.getNis(),
                            siswa.getNama(),
                            new Date(),
                            trans.getJumlahBayar(),
                            "Pembayaran Uang Pangkal"
                    )
            );
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PSN, siswa);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        }
    }

    @Override
    public Response bayarSpp(TransBayar trans) {
        try {
            Siswa siswa = cariBerdasarkanNisdanJenjang(trans.getNis(), trans.getJenjang());
            if (siswa == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            List<Pembayaran> listBayar = new ArrayList<>();
            Transaksi transTb = siswa.getTransaksi();
            transTb.setDiubahOleh(trans.getUserUpdate());
            transTb.setTanggalUbah(trans.getDateUpdate());
            DetailSppTb tb = siswa.getSppTahunBerjalan();
            Double biaya = cariNominalSpp(tb, trans.getBulan());
            transTb.setSppTbBayar(transTb.getSppTbBayar() + trans.getJumlahBayar());
            transTb.setSppTbSisa(transTb.getSppTbSisa() - trans.getJumlahBayar());
            if (biaya > trans.getJumlahBayar()) {
                double kurangBayar = biaya - trans.getJumlahBayar();
                double lebihBayar = transTb.getSppTbLebih();
                double sppBayar;
                if (lebihBayar < kurangBayar) {
                    transTb.setSppTbLebih(0.0);
                    sppBayar = trans.getJumlahBayar() + lebihBayar;
                } else {
                    transTb.setSppTbLebih(lebihBayar - kurangBayar);
                    sppBayar = trans.getJumlahBayar() + kurangBayar;
                }
                double bayar = biaya - sppBayar;
                tb = setNominal(tb, trans.getBulan(), bayar <= 0 ? 0.0 : bayar);
                listBayar.add(setSppBayar(trans, sppBayar, "", "", 0L));
            } else if (biaya < trans.getJumlahBayar()) {
                Double lebih = trans.getJumlahBayar() - biaya;
                lebih = transTb.getSppTbLebih() + lebih;
                String noTrans = generateNoBayar(trans.getNis(), TipeEnum.PAY, "");
                boolean lanjut;
                int idx;
                long id = idTerakhir(TipeEnum.PAY, trans.getNis());
                switch (trans.getBulan()) {
                    case BulanConstant.JUL:
                        transTb.setSppTbLebih(lebih);
                        if (tb.getSppJuli() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), "", BulanConstant.JUL, id));
                        }
                        break;
                    case BulanConstant.AUG:
                        if (tb.getSppAgustus() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), "", BulanConstant.AUG, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        if (tb.getSppJuli() > 0.0) {
                            if (lebih < tb.getSppJuli()) {
                                listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                lebih = 0.0;
                            } else {
                                lebih = lebih - tb.getSppJuli();
                                listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                tb = setNominal(tb, BulanConstant.JUL, 0.0);
                            }
                        }
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.SEP:
                        if (tb.getSppSeptember() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), "", BulanConstant.SEP, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            if (idx == 1) {
                                if (tb.getSppJuli() > 0.0) {
                                    if (lebih < tb.getSppJuli()) {
                                        listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                        tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                        lebih = 0.0;
                                        lanjut = false;
                                    } else {
                                        lebih = lebih - tb.getSppJuli();
                                        listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                        tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                        noTrans = listBayar.get(idx).getNoBayar();
                                        id++;
                                    }
                                }
                            } else {
                                if (tb.getSppAgustus() > 0.0) {
                                    if (lebih < tb.getSppAgustus()) {
                                        listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                        tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                        lebih = 0.0;
                                    } else {
                                        lebih = lebih - tb.getSppAgustus();
                                        listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                        tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                    }
                                }
                                lanjut = false;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.OCT:
                        if (tb.getSppOktober() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), "", BulanConstant.OCT, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.NOV:
                        if (tb.getSppNovember() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), "", BulanConstant.NOV, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.DEC:
                        if (tb.getSppDesember() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppDesember(), "", BulanConstant.DEC, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 5:
                                    if (tb.getSppNovember() > 0.0) {
                                        if (lebih < tb.getSppNovember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, tb.getSppNovember() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppNovember();
                                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.JAN:
                        if (tb.getSppJanuari() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppJanuari(), "", BulanConstant.JAN, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 5:
                                    if (tb.getSppNovember() > 0.0) {
                                        if (lebih < tb.getSppNovember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, tb.getSppNovember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppNovember();
                                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 6:
                                    if (tb.getSppDesember() > 0.0) {
                                        if (lebih < tb.getSppDesember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, tb.getSppDesember() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppDesember();
                                            listBayar.add(setSppBayar(trans, tb.getSppDesember(), noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.FEB:
                        if (tb.getSppFebruari() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppFebruari(), "", BulanConstant.FEB, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 5:
                                    if (tb.getSppNovember() > 0.0) {
                                        if (lebih < tb.getSppNovember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, tb.getSppNovember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppNovember();
                                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 6:
                                    if (tb.getSppDesember() > 0.0) {
                                        if (lebih < tb.getSppDesember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, tb.getSppDesember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppDesember();
                                            listBayar.add(setSppBayar(trans, tb.getSppDesember(), noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 7:
                                    if (tb.getSppJanuari() > 0.0) {
                                        if (lebih < tb.getSppJanuari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, tb.getSppJanuari() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppJanuari();
                                            listBayar.add(setSppBayar(trans, tb.getSppJanuari(), noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.MAR:
                        if (tb.getSppMaret() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppMaret(), "", BulanConstant.MAR, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 5:
                                    if (tb.getSppNovember() > 0.0) {
                                        if (lebih < tb.getSppNovember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, tb.getSppNovember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppNovember();
                                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 6:
                                    if (tb.getSppDesember() > 0.0) {
                                        if (lebih < tb.getSppDesember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, tb.getSppDesember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppDesember();
                                            listBayar.add(setSppBayar(trans, tb.getSppDesember(), noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 7:
                                    if (tb.getSppJanuari() > 0.0) {
                                        if (lebih < tb.getSppJanuari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, tb.getSppJanuari() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJanuari();
                                            listBayar.add(setSppBayar(trans, tb.getSppJanuari(), noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 8:
                                    if (tb.getSppFebruari() > 0.0) {
                                        if (lebih < tb.getSppFebruari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, tb.getSppFebruari() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppFebruari();
                                            listBayar.add(setSppBayar(trans, tb.getSppFebruari(), noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.APR:
                        if (tb.getSppApril() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppApril(), "", BulanConstant.APR, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 5:
                                    if (tb.getSppNovember() > 0.0) {
                                        if (lebih < tb.getSppNovember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, tb.getSppNovember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppNovember();
                                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 6:
                                    if (tb.getSppDesember() > 0.0) {
                                        if (lebih < tb.getSppDesember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, tb.getSppDesember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppDesember();
                                            listBayar.add(setSppBayar(trans, tb.getSppDesember(), noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 7:
                                    if (tb.getSppJanuari() > 0.0) {
                                        if (lebih < tb.getSppJanuari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, tb.getSppJanuari() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJanuari();
                                            listBayar.add(setSppBayar(trans, tb.getSppJanuari(), noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 8:
                                    if (tb.getSppFebruari() > 0.0) {
                                        if (lebih < tb.getSppFebruari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, tb.getSppFebruari() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppFebruari();
                                            listBayar.add(setSppBayar(trans, tb.getSppFebruari(), noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 9:
                                    if (tb.getSppMaret() > 0.0) {
                                        if (lebih < tb.getSppMaret()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.MAR, id));
                                            tb = setNominal(tb, BulanConstant.MAR, tb.getSppMaret() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppMaret();
                                            listBayar.add(setSppBayar(trans, tb.getSppMaret(), noTrans, BulanConstant.MAR, id));
                                            tb = setNominal(tb, BulanConstant.MAR, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.MAY:
                        if (tb.getSppMei() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppMei(), "", BulanConstant.MAY, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 5:
                                    if (tb.getSppNovember() > 0.0) {
                                        if (lebih < tb.getSppNovember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, tb.getSppNovember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppNovember();
                                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 6:
                                    if (tb.getSppDesember() > 0.0) {
                                        if (lebih < tb.getSppDesember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, tb.getSppDesember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppDesember();
                                            listBayar.add(setSppBayar(trans, tb.getSppDesember(), noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 7:
                                    if (tb.getSppJanuari() > 0.0) {
                                        if (lebih < tb.getSppJanuari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, tb.getSppJanuari() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJanuari();
                                            listBayar.add(setSppBayar(trans, tb.getSppJanuari(), noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 8:
                                    if (tb.getSppFebruari() > 0.0) {
                                        if (lebih < tb.getSppFebruari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, tb.getSppFebruari() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppFebruari();
                                            listBayar.add(setSppBayar(trans, tb.getSppFebruari(), noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 9:
                                    if (tb.getSppMaret() > 0.0) {
                                        if (lebih < tb.getSppMaret()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.MAR, id));
                                            tb = setNominal(tb, BulanConstant.MAR, tb.getSppMaret() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppMaret();
                                            listBayar.add(setSppBayar(trans, tb.getSppMaret(), noTrans, BulanConstant.MAR, id));
                                            tb = setNominal(tb, BulanConstant.MAR, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 10:
                                    if (tb.getSppApril() > 0.0) {
                                        if (lebih < tb.getSppApril()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.APR, id));
                                            tb = setNominal(tb, BulanConstant.APR, tb.getSppApril() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppApril();
                                            listBayar.add(setSppBayar(trans, tb.getSppApril(), noTrans, BulanConstant.APR, id));
                                            tb = setNominal(tb, BulanConstant.APR, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    case BulanConstant.JUN:
                        if (tb.getSppJuni() > 0.0) {
                            listBayar.add(setSppBayar(trans, tb.getSppJuni(), "", BulanConstant.JUN, id));
                            noTrans = listBayar.get(0).getNoBayar();
                            id++;
                        }
                        lanjut = true;
                        idx = 0;
                        do {
                            idx++;
                            switch (idx) {
                                case 1:
                                    if (tb.getSppJuli() > 0.0) {
                                        if (lebih < tb.getSppJuli()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, tb.getSppJuli() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJuli();
                                            listBayar.add(setSppBayar(trans, tb.getSppJuli(), noTrans, BulanConstant.JUL, id));
                                            tb = setNominal(tb, BulanConstant.JUL, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 2:
                                    if (tb.getSppAgustus() > 0.0) {
                                        if (lebih < tb.getSppAgustus()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, tb.getSppAgustus() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppAgustus();
                                            listBayar.add(setSppBayar(trans, tb.getSppAgustus(), noTrans, BulanConstant.AUG, id));
                                            tb = setNominal(tb, BulanConstant.AUG, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 3:
                                    if (tb.getSppSeptember() > 0.0) {
                                        if (lebih < tb.getSppSeptember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, tb.getSppSeptember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppSeptember();
                                            listBayar.add(setSppBayar(trans, tb.getSppSeptember(), noTrans, BulanConstant.SEP, id));
                                            tb = setNominal(tb, BulanConstant.SEP, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (tb.getSppOktober() > 0.0) {
                                        if (lebih < tb.getSppOktober()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, tb.getSppOktober() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppOktober();
                                            listBayar.add(setSppBayar(trans, tb.getSppOktober(), noTrans, BulanConstant.OCT, id));
                                            tb = setNominal(tb, BulanConstant.OCT, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 5:
                                    if (tb.getSppNovember() > 0.0) {
                                        if (lebih < tb.getSppNovember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, tb.getSppNovember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppNovember();
                                            listBayar.add(setSppBayar(trans, tb.getSppNovember(), noTrans, BulanConstant.NOV, id));
                                            tb = setNominal(tb, BulanConstant.NOV, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 6:
                                    if (tb.getSppDesember() > 0.0) {
                                        if (lebih < tb.getSppDesember()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, tb.getSppDesember() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppDesember();
                                            listBayar.add(setSppBayar(trans, tb.getSppDesember(), noTrans, BulanConstant.DEC, id));
                                            tb = setNominal(tb, BulanConstant.DEC, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 7:
                                    if (tb.getSppJanuari() > 0.0) {
                                        if (lebih < tb.getSppJanuari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, tb.getSppJanuari() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppJanuari();
                                            listBayar.add(setSppBayar(trans, tb.getSppJanuari(), noTrans, BulanConstant.JAN, id));
                                            tb = setNominal(tb, BulanConstant.JAN, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 8:
                                    if (tb.getSppFebruari() > 0.0) {
                                        if (lebih < tb.getSppFebruari()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, tb.getSppFebruari() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppFebruari();
                                            listBayar.add(setSppBayar(trans, tb.getSppFebruari(), noTrans, BulanConstant.FEB, id));
                                            tb = setNominal(tb, BulanConstant.FEB, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 9:
                                    if (tb.getSppMaret() > 0.0) {
                                        if (lebih < tb.getSppMaret()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.MAR, id));
                                            tb = setNominal(tb, BulanConstant.MAR, tb.getSppMaret() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppMaret();
                                            listBayar.add(setSppBayar(trans, tb.getSppMaret(), noTrans, BulanConstant.MAR, id));
                                            tb = setNominal(tb, BulanConstant.MAR, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 10:
                                    if (tb.getSppApril() > 0.0) {
                                        if (lebih < tb.getSppApril()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.APR, id));
                                            tb = setNominal(tb, BulanConstant.APR, tb.getSppApril() - lebih);
                                            lebih = 0.0;
                                            lanjut = false;
                                        } else {
                                            lebih = lebih - tb.getSppApril();
                                            listBayar.add(setSppBayar(trans, tb.getSppApril(), noTrans, BulanConstant.APR, id));
                                            tb = setNominal(tb, BulanConstant.APR, 0.0);
                                            noTrans = listBayar.get(idx).getNoBayar();
                                            id++;
                                        }
                                    }
                                    break;
                                case 11:
                                    if (tb.getSppMei() > 0.0) {
                                        if (lebih < tb.getSppMei()) {
                                            listBayar.add(setSppBayar(trans, lebih, noTrans, BulanConstant.MAY, id));
                                            tb = setNominal(tb, BulanConstant.MAY, tb.getSppMei() - lebih);
                                            lebih = 0.0;
                                        } else {
                                            lebih = lebih - tb.getSppMei();
                                            listBayar.add(setSppBayar(trans, tb.getSppMei(), noTrans, BulanConstant.MAY, id));
                                            tb = setNominal(tb, BulanConstant.MAY, 0.0);
                                        }
                                    }
                                    lanjut = false;
                                    break;
                                default:
                                    lanjut = false;
                                    break;
                            }
                        } while (lanjut);
                        transTb.setSppTbLebih(lebih);
                        break;
                    default:
                        return ResponseUtil.setResponse(
                                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                                "Bulan Pembayaran Tidak Ditemukan",
                                null
                        );
                }
                tb = setNominal(tb, trans.getBulan(), 0.0);
            } else {
                tb = setNominal(tb, trans.getBulan(), 0.0);
                listBayar.add(setSppBayar(trans, biaya, "", "", 0L));
            }
            transaksiRepository.save(transTb);
            detailSppTbRepository.save(tb);
            pembayaranRepository.saveAll(listBayar);
            sendMail(
                    siswa.getEmail(),
                    MailUtil.pembayaran("SPP Bulan " + trans.getBulan(), siswa.getNama()),
                    MailUtil.transBody(
                            siswa.getNis(),
                            siswa.getNama(),
                            new Date(),
                            trans.getJumlahBayar(),
                            "Pembayaran SPP Bulan " + trans.getBulan()
                    )
            );
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PSN, siswa);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        }
    }

    @Override
    public Response bayarSppTl(TransBayar trans) {
        try {
            Siswa siswa = cariBerdasarkanNisdanJenjang(trans.getNis(), trans.getJenjang());
            if (siswa == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            Transaksi transTl = siswa.getTransaksi();
            transTl.setSppTlBayar(transTl.getSppTlBayar() + trans.getJumlahBayar());
            if (transTl.getSppTlSisa() < trans.getJumlahBayar()) {
                transTl.setSppTlLebih(trans.getJumlahBayar() - transTl.getSppTlSisa());
                transTl.setSppTlSisa(0.0);
            } else {
                transTl.setSppTlSisa(transTl.getSppTlSisa() - trans.getJumlahBayar());
            }
            List<DetailSppTl> tls = siswa.getSppTahunLalu();
            List<DetailSppTl> stl = new ArrayList<>();
            List<Pembayaran> listTl = new ArrayList<>();
            Long idTl = idTerakhir(TipeEnum.PAY, trans.getNis());
            for (DetailSppTl tl : tls) {
                String noTrans = "";
                Double total = hitungTotalSpp(tl);
                tl.setDiubahOleh(trans.getUserUpdate());
                tl.setTanggalUbah(trans.getDateUpdate());
                if (trans.getJumlahBayar() >= total) {
                    for (int x = 1; x <= 12; x++) {
                        Double jumlah;
                        String bulan = "";
                        switch (x) {
                            case 1:
                                bulan = BulanEnum.JUL.getLabel();
                                jumlah = tl.getSppJuli();
                                break;
                            case 2:
                                bulan = BulanEnum.AUG.getLabel();
                                jumlah = tl.getSppAgustus();
                                break;
                            case 3:
                                bulan = BulanEnum.SEP.getLabel();
                                jumlah = tl.getSppSeptember();
                                break;
                            case 4:
                                bulan = BulanEnum.OCT.getLabel();
                                jumlah = tl.getSppOktober();
                                break;
                            case 5:
                                bulan = BulanEnum.NOV.getLabel();
                                jumlah = tl.getSppNovember();
                                break;
                            case 6:
                                bulan = BulanEnum.DEC.getLabel();
                                jumlah = tl.getSppDesember();
                                break;
                            case 7:
                                bulan = BulanEnum.JAN.getLabel();
                                jumlah = tl.getSppJanuari();
                                break;
                            case 8:
                                bulan = BulanEnum.FEB.getLabel();
                                jumlah = tl.getSppFebruari();
                                break;
                            case 9:
                                bulan = BulanEnum.MAR.getLabel();
                                jumlah = tl.getSppMaret();
                                break;
                            case 10:
                                bulan = BulanEnum.APR.getLabel();
                                jumlah = tl.getSppApril();
                                break;
                            case 11:
                                bulan = BulanEnum.MAY.getLabel();
                                jumlah = tl.getSppMei();
                                break;
                            case 12:
                                bulan = BulanEnum.JUN.getLabel();
                                jumlah = tl.getSppJuni();
                                break;
                            default:
                                bulan = StringConstant.STRIP;
                                jumlah = 0.0;
                                break;
                        }
                        if (jumlah > 0.0) {
                            idTl++;
                            PembayaranId byrId = new PembayaranId();
                            byrId.setNis(trans.getNis());
                            byrId.setIdBayar(idTl);
                            Pembayaran byr = new Pembayaran();
                            byr.setId(byrId);
                            byr.setNoBayar(generateNoBayar(trans.getNis(), TipeEnum.SPPTL, noTrans));
                            byr.setDibuatOleh(trans.getUserCreate());
                            byr.setTanggalBuat(trans.getDateCreate());
                            byr.setBulan(bulan);
                            byr.setTipeBayar(TipePembayaranEnum.NORMAL.getLabel());
                            byr.setJenisPembayaran(JenisPembayaranEnum.TL.getLabel());
                            byr.setJumlahBayar(jumlah);
                            byr.setDiubahOleh(trans.getUserUpdate());
                            byr.setTanggalUbah(trans.getDateUpdate());
                            listTl.add(byr);
                            noTrans = byr.getNoBayar();
                        }
                    }
                    tl.setSppJuli(0.0);
                    tl.setSppAgustus(0.0);
                    tl.setSppSeptember(0.0);
                    tl.setSppOktober(0.0);
                    tl.setSppNovember(0.0);
                    tl.setSppDesember(0.0);
                    tl.setSppJanuari(0.0);
                    tl.setSppFebruari(0.0);
                    tl.setSppMaret(0.0);
                    tl.setSppApril(0.0);
                    tl.setSppMei(0.0);
                    tl.setSppJuni(0.0);
                } else {
                    boolean habis = false;
                    Double sppBayar = trans.getJumlahBayar();
                    int bln = 0;
                    String bulan = "";
                    Double jumlah = 0.0;
                    do {
                        bln++;
                        switch (bln) {
                            case 1:
                                bulan = BulanEnum.JUL.getLabel();
                                jumlah = tl.getSppJuli();
                                if (sppBayar <= tl.getSppJuli()) {
                                    jumlah = sppBayar;
                                    tl.setSppJuli(tl.getSppJuli() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppJuli();
                                    tl.setSppJuli(0.0);
                                }
                                break;
                            case 2:
                                bulan = BulanEnum.AUG.getLabel();
                                jumlah = tl.getSppAgustus();
                                if (sppBayar <= tl.getSppAgustus()) {
                                    jumlah = sppBayar;
                                    tl.setSppAgustus(tl.getSppAgustus() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppAgustus();
                                    tl.setSppAgustus(0.0);
                                }
                                break;
                            case 3:
                                bulan = BulanEnum.SEP.getLabel();
                                jumlah = tl.getSppSeptember();
                                if (sppBayar <= tl.getSppSeptember()) {
                                    jumlah = sppBayar;
                                    tl.setSppSeptember(tl.getSppSeptember() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppSeptember();
                                    tl.setSppSeptember(0.0);
                                }
                                break;
                            case 4:
                                bulan = BulanEnum.OCT.getLabel();
                                jumlah = tl.getSppOktober();
                                if (sppBayar <= tl.getSppOktober()) {
                                    jumlah = sppBayar;
                                    tl.setSppOktober(tl.getSppOktober() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppOktober();
                                    tl.setSppOktober(0.0);
                                }
                                break;
                            case 5:
                                bulan = BulanEnum.NOV.getLabel();
                                jumlah = tl.getSppNovember();
                                if (sppBayar <= tl.getSppNovember()) {
                                    jumlah = sppBayar;
                                    tl.setSppNovember(tl.getSppNovember() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppNovember();
                                    tl.setSppNovember(0.0);
                                }
                                break;
                            case 6:
                                bulan = BulanEnum.DEC.getLabel();
                                jumlah = tl.getSppDesember();
                                if (sppBayar <= tl.getSppDesember()) {
                                    jumlah = sppBayar;
                                    tl.setSppDesember(tl.getSppDesember() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppDesember();
                                    tl.setSppDesember(0.0);
                                }
                                break;
                            case 7:
                                bulan = BulanEnum.JAN.getLabel();
                                jumlah = tl.getSppJanuari();
                                if (sppBayar <= tl.getSppJanuari()) {
                                    jumlah = sppBayar;
                                    tl.setSppJanuari(tl.getSppJanuari() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppJanuari();
                                    tl.setSppJanuari(0.0);
                                }
                                break;
                            case 8:
                                bulan = BulanEnum.FEB.getLabel();
                                jumlah = tl.getSppFebruari();
                                if (sppBayar <= tl.getSppFebruari()) {
                                    jumlah = sppBayar;
                                    tl.setSppFebruari(tl.getSppFebruari() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppFebruari();
                                    tl.setSppFebruari(0.0);
                                }
                                break;
                            case 9:
                                bulan = BulanEnum.MAR.getLabel();
                                jumlah = tl.getSppMaret();
                                if (sppBayar <= tl.getSppMaret()) {
                                    jumlah = sppBayar;
                                    tl.setSppMaret(tl.getSppMaret() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppMaret();
                                    tl.setSppMaret(0.0);
                                }
                                break;
                            case 10:
                                bulan = BulanEnum.APR.getLabel();
                                jumlah = tl.getSppApril();
                                if (sppBayar <= tl.getSppApril()) {
                                    jumlah = sppBayar;
                                    tl.setSppApril(tl.getSppApril() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppApril();
                                    tl.setSppApril(0.0);
                                }
                                break;
                            case 11:
                                bulan = BulanEnum.MAY.getLabel();
                                jumlah = tl.getSppMei();
                                if (sppBayar <= tl.getSppMei()) {
                                    jumlah = sppBayar;
                                    tl.setSppMei(tl.getSppMei() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppMei();
                                    tl.setSppMei(0.0);
                                }
                                break;
                            case 12:
                                bulan = BulanEnum.JUN.getLabel();
                                jumlah = tl.getSppJuni();
                                if (sppBayar <= tl.getSppJuni()) {
                                    jumlah = sppBayar;
                                    tl.setSppJuni(tl.getSppJuni() - sppBayar);
                                    habis = true;
                                } else {
                                    sppBayar = sppBayar - tl.getSppJuni();
                                    tl.setSppJuni(0.0);
                                }
                                break;
                            default:
                                habis = false;
                                break;
                        }
                        stl.add(tl);
                        idTl++;
                        PembayaranId tlBayarId = new PembayaranId();
                        tlBayarId.setIdBayar(idTl);
                        tlBayarId.setNis(trans.getNis());
                        Pembayaran tlBayar = new Pembayaran();
                        tlBayar.setId(tlBayarId);
                        tlBayar.setNoBayar(generateNoBayar(trans.getNis(), TipeEnum.SPPTL, noTrans));
                        tlBayar.setTipeBayar(TipePembayaranEnum.NORMAL.getLabel());
                        tlBayar.setJenisPembayaran(JenisPembayaranEnum.TL.getLabel());
                        tlBayar.setDibuatOleh(trans.getUserCreate());
                        tlBayar.setTanggalBuat(trans.getDateCreate());
                        tlBayar.setBulan(bulan);
                        tlBayar.setJumlahBayar(jumlah);
                        tlBayar.setDiubahOleh(trans.getUserUpdate());
                        tlBayar.setTanggalUbah(trans.getDateUpdate());
                        listTl.add(tlBayar);
                        noTrans = tlBayar.getNoBayar();
                    } while (!habis);
                }
            }
            pembayaranRepository.saveAllAndFlush(listTl);
            detailSppTlRepository.saveAllAndFlush(stl);
            transaksiRepository.save(transTl);
            sendMail(
                    siswa.getEmail(),
                    MailUtil.pembayaran("SPP Tahun Lalu", siswa.getNama()),
                    MailUtil.transBody(
                            siswa.getNis(),
                            siswa.getNama(),
                            new Date(),
                            trans.getJumlahBayar(),
                            "Pembayaran SPP Tahun Lalu"
                    )
            );
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PSN, siswa);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        }
    }

    @Override
    public Response bayarDaftarUlang(TransBayar trans) {
        try {
            Siswa siswa = cariBerdasarkanNisdanJenjang(trans.getNis(), trans.getJenjang());
            if (siswa == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            Transaksi duTrans = siswa.getTransaksi();
            Double duSisa = duTrans.getDuSisa() - trans.getJumlahBayar();
            Double duBayar = duTrans.getDuBayar() + trans.getJumlahBayar();
            duTrans.setDuSisa(duSisa);
            duTrans.setDuBayar(duBayar);
            if (duSisa < 0) {
                String duLebih = String.valueOf(duSisa).replace(StringConstant.STRIP, "");
                duTrans.setDuLebih(Double.parseDouble(duLebih));
                duTrans.setDuSisa(0.0);
                duSisa = 0.0;
            }
            duTrans.setDiubahOleh(trans.getUserUpdate());
            duTrans.setTanggalUbah(trans.getDateUpdate());
            DetailDu du = siswa.getDaftarUlang().get(0);
            du.setJumlahDu(duSisa);
            du.setDiubahOleh(trans.getUserUpdate());
            du.setTanggalUbah(trans.getDateUpdate());
            Pembayaran bayarDu = new Pembayaran();
            Long idDu = idTerakhir(TipeEnum.PAY, trans.getNis());
            idDu++;
            PembayaranId duId = new PembayaranId();
            duId.setNis(trans.getNis());
            duId.setIdBayar(idDu);
            bayarDu.setId(duId);
            bayarDu.setNoBayar(generateNoBayar(trans.getNis(), TipeEnum.DU, ""));
            bayarDu.setDibuatOleh(trans.getUserCreate());
            bayarDu.setTanggalBuat(trans.getDateCreate());
            bayarDu.setBulan(StringConstant.STRIP);
            bayarDu.setTipeBayar(TipePembayaranEnum.NORMAL.getLabel());
            bayarDu.setJenisPembayaran(JenisPembayaranEnum.DU.getLabel());
            bayarDu.setJumlahBayar(trans.getJumlahBayar());
            bayarDu.setDiubahOleh(trans.getUserUpdate());
            bayarDu.setTanggalUbah(trans.getDateUpdate());
            transaksiRepository.save(duTrans);
            detailDuRepository.save(du);
            pembayaranRepository.save(bayarDu);
            sendMail(
                    siswa.getEmail(),
                    MailUtil.pembayaran("Daftar Ulang", siswa.getNama()),
                    MailUtil.transBody(
                            siswa.getNis(),
                            siswa.getNama(),
                            new Date(),
                            trans.getJumlahBayar(),
                            "Pembayaran Daftar Ulang"
                    )
            );
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PSN, siswa);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        }
    }

    @Override
    public Response penyesuaian(Penyesuaian bayar) {
        try {
            Siswa siswa = cariBerdasarkanNisdanJenjang(bayar.getNis(), bayar.getJenjang());
            if (siswa == null) {
                return ResponseUtil.setResponse(HttpStatus.NOT_FOUND.value(), PesanConstant.TIDAK_DITEMUKAN, null);
            }
            switch (bayar.getDari()) {
                case JenisConstant.UP:
                    switch (bayar.getKe()) {
                        case JenisConstant.DU:
                            siswa = duAdjust(siswa, bayar, JenisConstant.UP);
                            break;
                        case JenisConstant.TB:
                            siswa = tbAdjust(siswa, bayar, JenisConstant.UP);
                            break;
                        case JenisConstant.TL:
                            siswa = tlAdjust(siswa, bayar, JenisConstant.UP);
                            break;
                        default:
                            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ERR, null);
                    }
                    break;
                case JenisConstant.DU:
                    switch (bayar.getKe()) {
                        case JenisConstant.UP:
                            siswa = upAdjust(siswa, bayar, JenisConstant.DU);
                            break;
                        case JenisConstant.TB:
                            siswa = tbAdjust(siswa, bayar, JenisConstant.DU);
                            break;
                        case JenisConstant.TL:
                            siswa = tlAdjust(siswa, bayar, JenisConstant.DU);
                            break;
                        default:
                            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ERR, null);
                    }
                    break;
                case JenisConstant.TB:
                    switch (bayar.getKe()) {
                        case JenisConstant.UP:
                            siswa = upAdjust(siswa, bayar, JenisConstant.TB);
                            break;
                        case JenisConstant.DU:
                            siswa = duAdjust(siswa, bayar, JenisConstant.TB);
                            break;
                        case JenisConstant.TL:
                            siswa = tlAdjust(siswa, bayar, JenisConstant.TB);
                            break;
                        default:
                            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ERR, null);
                    }
                    break;
                case JenisConstant.TL:
                    switch (bayar.getKe()) {
                        case JenisConstant.UP:
                            siswa = upAdjust(siswa, bayar, JenisConstant.TL);
                            break;
                        case JenisConstant.DU:
                            siswa = duAdjust(siswa, bayar, JenisConstant.TL);
                            break;
                        case JenisConstant.TB:
                            siswa = tbAdjust(siswa, bayar, JenisConstant.TL);
                            break;
                        default:
                            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ERR, null);
                    }
                    break;
                default:
                    return ResponseUtil.setResponse(
                            HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            "Sumber Penyesuaian Tidak Valid",
                            null
                    );
            }
            transaksiRepository.save(siswa.getTransaksi());
            detailUpRepository.save(siswa.getUangPangkal());
            detailDuRepository.saveAll(siswa.getDaftarUlang());
            detailSppTlRepository.saveAll(siswa.getSppTahunLalu());
            detailSppTbRepository.save(siswa.getSppTahunBerjalan());
            pembayaranRepository.saveAll(siswa.getPembayaran());
            return ResponseUtil.setResponse(HttpStatus.OK.value(), PSN, siswa);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseUtil.setResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), null);
        }
    }
}
